//
//  NotificationName+Extension.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 8/12/20.
//

import Foundation

extension Notification.Name {

    static let appTimeout = Notification.Name("appTimeout")

}
