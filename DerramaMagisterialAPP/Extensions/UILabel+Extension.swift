//
//  UILabel+Extension.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import Foundation

extension UILabel {
    
    func personalizeLabelStyle(fontSize : CGFloat, isBold : Bool) {
        let nameFont = isBold ? Constant.dmFontBold : Constant.dmFont
        self.font = UIFont(name: nameFont, size: fontSize)
    }
    
    func personalizeRoundStyle() {
        personalizeLabelStyle(fontSize: 15, isBold: true)
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 0.5
        layer.masksToBounds = true
        layer.cornerRadius = 15.0
        backgroundColor = .white
        textColor = Colors.primaryColor()
    }
    
    func personalizeAmountRoundStyle() {
        personalizeLabelStyle(fontSize: 15, isBold: true)
        layer.masksToBounds = true
        layer.cornerRadius = 15.0
        backgroundColor = Colors.amountEvaluationColor()
        textColor = .white
    }
    
    func personlizeAmountTitleResult(){
        numberOfLines = 0
        textAlignment = .center
        textColor = Colors.productCellTextColor()
        personalizeLabelStyle(fontSize: 12, isBold: false)
    }
    
    func personlizeAmountDescResult(){
        numberOfLines = 0
        textAlignment = .center
        textColor = Colors.productCellTextColor()
        personalizeLabelStyle(fontSize: 14, isBold: true)
    }
}
