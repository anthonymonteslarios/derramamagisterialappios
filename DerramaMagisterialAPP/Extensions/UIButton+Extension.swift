//
//  UIButton+Extension.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/14/20.
//

import Foundation

extension UIButton {
    
    func personalizeButton(title : String, sizeFont : CGFloat) {
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont(name: Constant.dmFont, size: sizeFont)
    }
    
    func personalizeBoldButton(title : String, sizeFont : CGFloat) {
        setTitle(title, for: .normal)
        titleLabel?.font = UIFont(name: Constant.dmFontBold, size: sizeFont)
    }
    
    func personalizeButtonBasicStyle(with title : String) {
        personalizeButton(title: title, sizeFont: 12.0)
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 15.0
        backgroundColor = Colors.primaryColor()
        setTitleColor(UIColor.white, for: .normal)
    }
    
    func personalizeButtonRoundBasicStyle(with title : String) {
        personalizeBoldButton(title: title, sizeFont: 12.0)
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 5.0
        backgroundColor = Colors.primaryColor()
        setTitleColor(UIColor.white, for: .normal)
    }
    
    func personalizeGrayButtonRoundBasicStyle(with title : String) {
        personalizeBoldButton(title: title, sizeFont: 12.0)
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 5.0
        backgroundColor = .white
        setTitleColor(Colors.primaryColor(), for: .normal)
    }
    
    func personalizeButtonTransparenttyle(with title : String) {
        personalizeBoldButton(title: title, sizeFont: 13.0)
        backgroundColor = .clear
        setTitleColor(Colors.textInformationColor(), for: .normal)
    }
}
