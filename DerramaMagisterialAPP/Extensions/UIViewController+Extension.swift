//
//  UIViewController+Extension.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import Foundation

extension UIViewController {
    class func getController(storyBoard: String, indentifier: String) -> UIViewController{
        let storyBoardProduct = UIStoryboard(name: storyBoard, bundle: nil)
        return storyBoardProduct.instantiateViewController(withIdentifier: indentifier)
    }
    
    func alertasNativas(title: String, message: String, acceptButton: String, cancelButton: String? = nil, handlerAccept: (() -> Void)? = nil, handlerCancel: (() -> Void)? = nil){
        let alertController = UIAlertController(title: title, message: message, preferredStyle:UIAlertController.Style.alert)

        alertController.addAction(UIAlertAction(title: acceptButton , style: UIAlertAction.Style.default)
         { action -> Void in
            handlerAccept?()
         })
        
        if let cancelButton = cancelButton{
            alertController.addAction(UIAlertAction(title: cancelButton , style: UIAlertAction.Style.default)
             { action -> Void in
                handlerCancel?()
             })
        }
        
        self.present(alertController, animated: true, completion: nil)        
    }
    

    open override func awakeFromNib() {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

