//
//  View.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 22/11/20.
//

import Foundation

class CornerView: UIView{
    
    
    override func awakeFromNib() {

        layer.cornerRadius = 5
        layer.borderColor = UIColor.clear.cgColor
        layer.borderWidth = 1.0
        layer.masksToBounds = true

    }
        

}
