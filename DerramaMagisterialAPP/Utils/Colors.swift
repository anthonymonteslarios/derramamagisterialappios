//
//  Colors.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import Foundation

struct Colors {
    
    static func primaryColor() -> UIColor {
        return UIColor.colorFromHexString("#0060AF", withAlpha: 1)
    }
    
    static func textMenuColor() -> UIColor{
        return UIColor.colorFromHexString("#434343", withAlpha: 1)
    }
    
    static func textSelectMenuColor() -> UIColor{
        return UIColor.colorFromHexString("#053760", withAlpha: 1)
    }
    
    static func viewSelectMenuColor() -> UIColor{
        return UIColor.colorFromHexString("#0060AF", withAlpha: 0.2)
    }
    
    static func separatorMenuCellColor() -> UIColor{
        return UIColor.colorFromHexString("#D9D9D9", withAlpha: 1)
    }
    
    static func screenColor() -> UIColor{
        return UIColor.colorFromHexString("#EFEFEF", withAlpha: 1)
    }

    static func cardColor() -> UIColor{
        return UIColor.colorFromHexString("#D9D9D9", withAlpha: 1)
    }
    
    static func productTextColor() -> UIColor{
        return UIColor.colorFromHexString("#053760", withAlpha: 1)
    }
    
    static func productCellTextColor() -> UIColor{
        return UIColor.colorFromHexString("#006585", withAlpha: 1)
    }
    
    static func tabDeselectColor() -> UIColor{
        return UIColor.colorFromHexString("#ffffff", withAlpha: 0.5)
    }
    
    static func amountEvaluationColor() -> UIColor{
        return UIColor.colorFromHexString("#58c4e7", withAlpha: 1)
    }
    
    static func textInformationColor() -> UIColor{
        return UIColor.colorFromHexString("#5181d5", withAlpha: 1)
    }
    
    static func bnContainerColor() -> UIColor{
        return UIColor.colorFromHexString("#FFEBEE", withAlpha: 1)
    }
    
    static func textBnColor() -> UIColor{
        return UIColor.colorFromHexString("#E57373", withAlpha: 1)
    }
    
    static func textAmountBnColor() -> UIColor{
        return UIColor.colorFromHexString("#B71C1C", withAlpha: 1)
    }
    
    
}
