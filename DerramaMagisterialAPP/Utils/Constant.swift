//
//  Constant.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 12/11/20.
//

import Foundation

class Constant {
    //DESARROLLO
    //static let URL_BASE = "http://chatbot.derrama.org.pe/MDCanalMovil/motor/"
    //PRODUCCIÓN
    static let URL_BASE = "http://web.derrama.org.pe/MDCanalMovil/motor/"
    
    static let URL_EXTERNAL = "https://web.derrama.org.pe/CuentaAsociados/cambiarclave.html"
    
    //MARK: - Fonts
    static let dmFont = "Helvetica"
    static let dmFontBold = "Helvetica-Bold"
    static let textEmpty = ""
    
    
    //MARK: - Datos - GET
    static let FirstRecalcular = "N"
    static let Recalcular = "S"
    static let CreditoConsumo = "N"
    static let CreditoTeApoyo = "S"
    static let coinSymbolPEN = "S/"
    
    //MARK: - Datos - ListarTipoDesembolso
    static let desembolso = "A"
    static let canal = "4"
    
    //MARK: - Mensajes
    static let validateLogin = "Debe ingresar con su DNI y su clave digital."
    static let textNextVersion = "Disponible Próximamente..."
    
    //MARK: - Acciones
    static let acceptButton = "Aceptar"
    
    //MARK: - Error Labels
    static let errorPhoneNumberEmpty = "Por favor ingresa un número."
    static let errorPhoneNumberInvalid = "Por favor ingresa un número válido."
    static let errorEmailEmpty = "Por favor ingresa un e-mail."
    static let errorEmailInvalid = "Por favor ingresa un e-mail válido."
    
    static let messageAlertCredit = "ESTIMADO DOCENTE VERIFIQUE SI LLENÓ CORRECTAMENTE EL PAGARÉ, A LA FECHA EXISTEN CRÉDITOS RECHAZADOS SÓLO POR EL MAL LLENADO DE ESTE DOCUMENTO.¿SEGURO QUE LLENÓ CORRECTAMENTE EL DOCUMENTO?"
    

}
