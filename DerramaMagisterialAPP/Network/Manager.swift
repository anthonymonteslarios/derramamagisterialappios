//
//  Manager.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 12/11/20.
//

import Foundation
import Alamofire
import SwiftyJSON

struct Manager {
    
    // MARK: - Singleton
    static var shared = Manager()
    var alamoFireManager : SessionManager? // this line

    
    mutating func requesTimeOut() -> SessionManager{
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        
        return alamoFireManager!
    }
    
    mutating func requestLoginDonor(Dni : String, Password : String, completion: @escaping (ResponseLogin?, ErrorResponse) -> ()){
          
        let url_login = Constant.URL_BASE + "" + RestApi.Post_Login

          let parameters: Parameters = [
            "DNI": Dni, "CLAVE": Password]
    
        
        requesTimeOut().request(url_login, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseLogin { response in
            
            do {
                let responseLogin = try JSONDecoder().decode(ErrorResponse.self, from: response.data!)
                if ((responseLogin.first?.success) == nil){
                    if let introduccion = response.result.value {
                        print("Success \(introduccion)")
                        completion(introduccion, ErrorResponse.init())
                        return
                    }
                }else{
                    completion(ResponseLogin.init(), responseLogin)
                }
            } catch {
                if let introduccion = response.result.value {
                    print("Success \(introduccion)")
                    completion(introduccion, ErrorResponse.init())
                    return
                }
            }
            
            
            

          }
      }
    
    mutating func requestPhotoDonor(Dni : String, completion: @escaping (ResponsePhoto?, ErrorResponse) -> ()){
          
        let url_login = Constant.URL_BASE + "" + RestApi.POST_Photo

          let parameters: Parameters = [
            "DNI": Dni]
    
        
        requesTimeOut().request(url_login, method:.post, parameters:parameters, encoding: JSONEncoding.default).responsePhoto { response in
                                      
            do {
                let responseLogin = try JSONDecoder().decode(ErrorResponse.self, from: response.data!)
                if ((responseLogin.first?.success) == nil){
                    if let introduccion = response.result.value {
                        print("Success \(introduccion)")
                        completion(introduccion, ErrorResponse.init())
                        return
                    }
                }else{
                    completion(response.result.value!, responseLogin)
                }
            } catch {
                if let photo = response.result.value {
                    print("Success \(photo)")
                    completion(photo, ErrorResponse.init())
                    return
                }else{
                    completion(nil, ErrorResponse.init())
                }
            }

          }
      }
    
    
        
    mutating func requestSendDocumentFacial(asodni : String,capturaFacial: String, aplicacion: String, usureg: String, cortar:String,firma: String, asonomdni: String, asoid: String, credid: String,codSol: String, asoTipId:String, regPenId: String, completion: @escaping (ResponseMessage) -> ()){
          
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_EnvioFacial

          let parameters: Parameters = [
            "asodni": asodni,
            "capturaFacial": capturaFacial,
            "aplicacion":aplicacion,
            "usureg":usureg,
            "cortar":cortar,
            "firma":firma,
            "asonomdni":asonomdni,
            "asoid":asoid,
            "credid":credid,
            "codSol":codSol,
            "asoTipId":asoTipId,
            "regPenId":regPenId
          ]
    
       
        requesTimeOut().request(url_envioFacial, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseResponseMessage { response in
                                              
            let responseSaveData = response.result.value
            
            if let responseSaveData = responseSaveData{
                completion(responseSaveData)
            }else{
                completion(ResponseMessage())
            }
            
          }
      }
    
    
    mutating func requestGetProducts(ASODNI : String, RECALCULAR : String, MONTO_OTORGAR : String, NROCUOTAS : String, TEM : String, MONTO_OTORGAR_X: String, NROCUOTAS_X: String, PARALELO: String, completion: @escaping (ResponseProductsAll?, ErrorResponse) -> ()){
          
        let url_GET = Constant.URL_BASE + "" + RestApi.GET_Data

        var parametersGet: Parameters = [:]
        
        parametersGet = ["ASODNI": ASODNI, "RECALCULAR": RECALCULAR, "MONTO_OTORGAR": MONTO_OTORGAR, "NROCUOTAS": NROCUOTAS, "TEM": TEM, "MONTO_OTORGAR_X": MONTO_OTORGAR_X, "NROCUOTAS_X" : NROCUOTAS_X, "PARALELO" : PARALELO]
    
        
        requesTimeOut().request(url_GET, method:.post, parameters:parametersGet, encoding: JSONEncoding.default).responseGetData { response in
                                      
            do {
                let responseLogin = try JSONDecoder().decode(ErrorResponse.self, from: response.data!)
                if ((responseLogin.first?.success) == nil){
                    if let introduccion = response.result.value {
                        print("Success \(introduccion)")
                        completion(introduccion, ErrorResponse.init())
                        return
                    }
                }else{
                    completion(response.result.value, responseLogin)
                }
            } catch {
                if let introduccion = response.result.value {
                    print("Success \(introduccion)")
                    completion(introduccion, ErrorResponse.init())
                    return
                }
            }

          }
      }
    
    mutating func requestSaveRequest(ASOID : String, ASONCTA : String, TIPOCANAL : String, ASOAPENOMDNI : String, CUENTA_VALIDADA : String, OFERTA_TIPOCREID: String, OFERTA_MONMIN: String, OFERTA_MONMAX: String, OFERTA_CUOMIN: String, OFERTA_CUOMAX: String, OFERTA_MONTO: String, OFERTA_PLAZO: String, OFERTA_CUOTA: String, OFERTA_PORINT: String, OFERTA_PORFLT: String, OFERTA_PORDES: String, OFERTA_CAPPAGO: String, FECEMI: String, FECCAP: String, NOMPADREMADRE: String, CORREO: String, CELULAR: String, OFERTA_NOTAABONO: String, OFERTA_DIAGRACIA: String, OFERTA_DIAPROCESO: String, OFERTA_DIAPROCESOGRACIA: String, OFERTA_MONINTPRO: String, OFERTA_TEA: String, OFERTA_TCEA: String, OFERTA_CODPAD: String, OFERTA_HABER: String, OFERTA_DESCUENTO: String, OFERTA_LIQUIDO: String, FIRMA: String, PARALELO: String, DISPOSITIVO: String, LATITUDE: String, LONGITUDE: String, UA: String, MOBILE: String, PHONE: String, TABLET: String, USERAGENT: String, OS: String, IP_PUBLICA: String, completion: @escaping (ErrorElement?, ErrorResponse) -> ()){
          
        let url_GET = Constant.URL_BASE + "" + RestApi.POST_SaveRequest

        var parametersGet: Parameters = [:]
        
        parametersGet = ["ASOID": ASOID, "ASONCTA": ASONCTA, "TIPOCANAL": TIPOCANAL, "ASOAPENOMDNI": ASOAPENOMDNI, "CUENTA_VALIDADA": CUENTA_VALIDADA, "OFERTA_TIPOCREID": OFERTA_TIPOCREID, "OFERTA_MONMIN" : OFERTA_MONMIN, "OFERTA_MONMAX" : OFERTA_MONMAX, "OFERTA_CUOMIN": OFERTA_CUOMIN, "OFERTA_CUOMAX": OFERTA_CUOMAX, "OFERTA_MONTO": OFERTA_MONTO, "OFERTA_PLAZO": OFERTA_PLAZO, "OFERTA_CUOTA": OFERTA_CUOTA, "OFERTA_PORINT": OFERTA_PORINT, "OFERTA_PORFLT": OFERTA_PORFLT, "OFERTA_PORDES": OFERTA_PORDES, "OFERTA_CAPPAGO": OFERTA_CAPPAGO, "FECEMI": FECEMI, "FECCAP": FECCAP, "NOMPADREMADRE": NOMPADREMADRE, "CORREO": CORREO, "CELULAR": CELULAR, "OFERTA_NOTAABONO": OFERTA_NOTAABONO, "OFERTA_DIAGRACIA": OFERTA_DIAGRACIA, "OFERTA_DIAPROCESO": OFERTA_DIAPROCESO, "OFERTA_DIAPROCESOGRACIA": OFERTA_DIAPROCESOGRACIA, "OFERTA_MONINTPRO": OFERTA_MONINTPRO, "OFERTA_TEA": OFERTA_TEA, "OFERTA_TCEA": OFERTA_TCEA, "OFERTA_CODPAD": OFERTA_CODPAD, "OFERTA_HABER": OFERTA_HABER, "OFERTA_DESCUENTO": OFERTA_DESCUENTO, "OFERTA_LIQUIDO": OFERTA_LIQUIDO, "FIRMA": FIRMA, "PARALELO": PARALELO, "DISPOSITIVO": DISPOSITIVO, "LATITUDE": LATITUDE, "LONGITUDE": LONGITUDE, "UA": UA, "MOBILE": MOBILE, "PHONE": PHONE, "TABLET": TABLET, "USERAGENT": USERAGENT, "OS": OS, "IP_PUBLICA": IP_PUBLICA]
    
        
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: parametersGet,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                       encoding: .ascii)
            print("JSON string = \(theJSONText!)")
        }
        
        
        requesTimeOut().request(url_GET, method:.post, parameters:parametersGet, encoding: JSONEncoding.default).requestSaveRequest { response in
                            
            let responseSaveData = response.result.value
            
            if let responseSaveData = responseSaveData{
                completion(responseSaveData, ErrorResponse.init())
            }else{
                completion(ErrorElement(), ErrorResponse.init())
            }
            
          }
      }
  
    
    mutating func requestGetRandomNames(Dni : String, completion: @escaping (ResponseRandomNameArray?, ErrorResponse) -> ()){
          
        let url_randomNames = Constant.URL_BASE + "" + RestApi.Post_RandomNAMES

          let parameters: Parameters = [
            "asodni": Dni]
    
        
        requesTimeOut().request(url_randomNames, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseResponseRandomName { response in
                                      
            do {
                let responseLogin = try JSONDecoder().decode(ResponseRandomNameArray.self, from: response.data!)
     
                completion(responseLogin,[])
                
            } catch {
     
            }

          }
      }
    
    
 
    mutating func requestControlIdentidad(asodni : String,fechaEmision: String, fechaCaducidad: String, padreMadre: String, dniNoCaduca:String, completion: @escaping (ResponseIdentidad?, ErrorResponse) -> ()){
          
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_ControlIdentidad

          let parameters: Parameters = [
            "ASODNI": asodni,
            "FECHAEMISION": fechaEmision,
            "FECHACADUCIDAD":fechaCaducidad,
            "PADREMADRE":padreMadre,
            "DNINOCADUCA":dniNoCaduca
          ]
        
        requesTimeOut().request(url_envioFacial, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseResponseControlIdentidad { response in
                                                                  
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
            
            
            let responseSaveData = response.result.value
            
            if let responseSaveData = responseSaveData{
                completion(responseSaveData, ErrorResponse.init())
            }else{
                completion(ResponseIdentidad(), ErrorResponse.init() )
            }
            
            
          }
      }
    
    mutating func requestListDesembolso(completion: @escaping (ResponseDesembolso?, ErrorResponse) -> ()){
             
        let url_randomNames = Constant.URL_BASE + "" + RestApi.Post_ListTypeDesembolso

        let parameters: Parameters = [:]
    
        
        requesTimeOut().request(url_randomNames, method:.post, parameters:parameters, encoding: JSONEncoding.default).requestListDesembolso { response in
                                      
            do {
                let responseDesembolsos = try JSONDecoder().decode(ResponseDesembolso.self, from: response.data!)
     
                completion(responseDesembolsos,[])
                
            } catch {
     
            }
          }
         }
    

    mutating func requestRefuses(completion: @escaping (ResponseRefuses?, ErrorResponse) -> ()){
          
     let url_randomNames = Constant.URL_BASE + "" + RestApi.Post_Refuses

     let parameters: Parameters = [:]
 
        requesTimeOut().request(url_randomNames, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            
            if let json = response.result.value, let data = JSON(json).array{
                var arrayRefuses = [Refuse]()
                for refuse in data {
                    arrayRefuses.append(Refuse.parse(refuse))
                }
                completion(arrayRefuses,[])
            }else{
                completion([],[])
            }
        })
      }
    
    mutating func requestRefuseSol(ACEPTAR_CODSOL : String,MOTIVORECHAZO: String, completion: @escaping (ErrorElement?, ErrorResponse) -> ()){
          
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_RefusesSol

          let parameters: Parameters = [
            "ACEPTAR_CODSOL": ACEPTAR_CODSOL,
            "MOTIVORECHAZO": MOTIVORECHAZO
          ]
        
        requesTimeOut().request(url_envioFacial, method:.post, parameters:parameters, encoding: JSONEncoding.default).requestSaveRequest { response in
                            
            let responseSaveData = response.result.value
            
            if let responseSaveData = responseSaveData{
                completion(responseSaveData, ErrorResponse.init())
            }else{
                completion(ErrorElement(), ErrorResponse.init())
            }
            
          }
      }
    
    mutating func requestSendResult(ACEPTAR_CODSOL : String, ACEPTAR_MONTO: String, ACEPTAR_PLAZO: String, ACEPTAR_INTERES: String, ACEPTAR_CUOTA: String, ACEPTAR_MONTO_INTERES_PROCESO: String, ACEPTAR_NOTA_ABONO: String, ACEPTAR_TEA: String, ACEPTAR_TCEA: String, ACEPTAR_DIASGRACIA: String, ACEPTAR_DIASENPROCESO:String, ACEPTAR_DIASPROCESOGRACIA: String, TIPODESEMBOLSO: String, USUARIO: String, CORREO: String, CELULAR: String, ASODNI: String, ASOAPENOMDNI: String, TIPOCANAL: String, completion: @escaping (ResponseIdentidad?, ResponseIdentidad) -> ()){
          
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_SendResult

          let parameters: Parameters = [
            "ACEPTAR_CODSOL": ACEPTAR_CODSOL,
            "ACEPTAR_MONTO": ACEPTAR_MONTO,
            "ACEPTAR_PLAZO": ACEPTAR_PLAZO,
            "ACEPTAR_INTERES": ACEPTAR_INTERES,
            "ACEPTAR_CUOTA": ACEPTAR_CUOTA,
            "ACEPTAR_MONTO_INTERES_PROCESO": ACEPTAR_MONTO_INTERES_PROCESO,
            "ACEPTAR_NOTA_ABONO": ACEPTAR_NOTA_ABONO,
            "ACEPTAR_TEA": ACEPTAR_TEA,
            "ACEPTAR_TCEA": ACEPTAR_TCEA,
            "ACEPTAR_DIASGRACIA": ACEPTAR_DIASGRACIA,
            "ACEPTAR_DIASENPROCESO": ACEPTAR_DIASENPROCESO,
            "ACEPTAR_DIASPROCESOGRACIA": ACEPTAR_DIASPROCESOGRACIA,
            "TIPODESEMBOLSO": TIPODESEMBOLSO,
            "USUARIO": USUARIO,
            "CORREO": CORREO,
            "CELULAR": CELULAR,
            "ASODNI": ASODNI,
            "ASOAPENOMDNI": ASOAPENOMDNI,
            "TIPOCANAL": TIPOCANAL
          ]
        
        requesTimeOut().request(url_envioFacial, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON(completionHandler: { (response) in
            if let json = response.result.value, let data = JSON(json).array?.first{
                
                completion(ResponseIdentidad.parse(data), ResponseIdentidad())
            }else{
                completion(ResponseIdentidad(), ResponseIdentidad())
            }
        })
      }
    
    mutating func requestDocumentos(asodni : String,correo: String, CREDID: String, AUTORIZACIONONP: String, completion: @escaping (ErrorResponse?, ErrorResponse) -> ()){
          
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_Documentacion

          let parameters: Parameters = [
            "ASOID": asodni,
            "CORREO": correo,
            "CREDID":CREDID,
            "AUTORIZACIONONP":AUTORIZACIONONP
          ]
        
        requesTimeOut().request(url_envioFacial, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseDocumentacion { response in
                                                                  
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
            
            let responseSaveData = response.result.value
            
            if let responseSaveData = responseSaveData{
                completion(responseSaveData, ErrorResponse.init())
            }else{
                completion(ErrorResponse(), ErrorResponse.init() )
            }
            
            
          }
      }
    
    
    
    mutating func requestUpdLoadDocument(CODSOL: String, RECOGER_DOCUMENTO: String, ASODNI: String, file_dni: UIImage, file_dni2: UIImage, file_contrato_credito: UIImage, file_contrato_credito2: UIImage, file_contrato_credito3: UIImage, file_carta_autorizacion: UIImage, file_hoja_resumen: UIImage, file_pagare: UIImage ,completion: @escaping (ResponseMessage) -> ()){
        let url_envioFacial = Constant.URL_BASE + "" + RestApi.Post_UploadArchivo
        
        let dataCodSol = CODSOL.data(using: .utf8)!
        let dataRecoger = RECOGER_DOCUMENTO.data(using: .utf8)!
        let dataASODNI = ASODNI.data(using: .utf8)!
        
        requesTimeOut().upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(dataCodSol, withName: "CODSOL", mimeType: "text/plain")
            multipartFormData.append(dataRecoger, withName: "RECOGER_DOCUMENTO", mimeType: "text/plain")
            multipartFormData.append(dataASODNI, withName: "ASODNI", mimeType: "text/plain")
            
            if RECOGER_DOCUMENTO == "V"{
                                
                if (Utils.imageIsNullOrNot(imageName: file_dni))  {
                    multipartFormData.append(file_dni.pngData()!, withName: "file_dni", fileName: "dni1.png", mimeType: "multipart/form-data")
                }
                if (Utils.imageIsNullOrNot(imageName: file_dni2))  {
                    multipartFormData.append(file_dni2.pngData()!, withName: "file_dni2", fileName: "dni2.png", mimeType: "multipart/form-data")
                }
                if (Utils.imageIsNullOrNot(imageName: file_contrato_credito))  {
                    multipartFormData.append(file_contrato_credito.pngData()!, withName: "file_contrato_credito", fileName: "file_contrato_credito.png", mimeType: "multipart/form-data")
                }
                if (Utils.imageIsNullOrNot(imageName: file_contrato_credito2))  {
                    multipartFormData.append(file_contrato_credito2.pngData()!, withName: "file_contrato_credito2", fileName: "file_contrato_credito2.png", mimeType: "multipart/form-data")
                }
                if (Utils.imageIsNullOrNot(imageName: file_contrato_credito3))  {
                    multipartFormData.append(file_contrato_credito3.pngData()!, withName: "file_contrato_credito3", fileName: "file_contrato_credito3.png", mimeType: "multipart/form-data")
                }

                if (Utils.imageIsNullOrNot(imageName: file_carta_autorizacion))  {
                    multipartFormData.append(file_carta_autorizacion.pngData()!, withName: (DMProduct.shared.credit?.autorizacionDescuentoOnp == "N") ? "file_carta_autorizacion_onp" : "file_carta_autorizacion" , fileName: "file_carta_autorizacion.png", mimeType: "multipart/form-data")
                }

                if (Utils.imageIsNullOrNot(imageName: file_hoja_resumen))  {
                    multipartFormData.append(file_hoja_resumen.pngData()!, withName: "file_hoja_resumen", fileName: "file_hoja_resumen.png", mimeType: "multipart/form-data")
                }
                
                if (Utils.imageIsNullOrNot(imageName: file_pagare))  {
                    multipartFormData.append(file_pagare.pngData()!, withName: "file_pagare", fileName: "file_pagare.png", mimeType: "multipart/form-data")
                }

            }
        }, to: url_envioFacial) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in

                    if !response.result.isSuccess {
                        print("# ERROR")
                    } else {
                        print("# SUCCESS")
                        print(response)
                        
                        do {
                            let responseFacial = try JSONDecoder().decode(ResponseMessage.self, from: response.data!)
                            completion(responseFacial)
                            
                        } catch {
                            if let introduccion = response.result.value {
                                completion(introduccion as! ResponseMessage)
                                return
                            }else{
                                completion(response.result.value! as! ResponseMessage)
                                return
                            }
                        }

                    }
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        }
        
    }
    
    
    mutating func requestGetListRequest(DNI : String, completion: @escaping (ResponseListRequest?) -> ()){
          
        let url_GET = Constant.URL_BASE + "" + RestApi.POST_ListRequest

        var parametersGet: Parameters = [:]
        
        parametersGet = ["DNI": DNI]
    
        
        requesTimeOut().request(url_GET, method:.post, parameters:parametersGet, encoding: JSONEncoding.default).responseGetListRequest { response in
                                      
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
            }
            
            let responseSaveData = response.result.value        
            
            do {
                let responseSaveData = try JSONDecoder().decode(ResponseListRequest.self, from: response.data!)
                completion(responseSaveData)
                
            } catch {
                if let responseSaveData = response.result.value {
                    completion(responseSaveData)
                    return
                }else{
                    completion(response.result.value)
                    return
                }
            }
            
          }
      }
    
    
    
}


class RestApi {
    static let Post_Login = "INGRESAR"
    static let POST_Photo = "FOTO"
    static let GET_Data = "get"
    static let POST_SaveRequest = "GRABARSOLICITUD"
    static let Post_RandomNAMES = "NOMBRESALEATORIOS"
    static let Post_EnvioFacial = "ENVIODOCUMENTOSPORVALIDACIONFACIAL"
    static let Post_UploadArchivo = "UPLOADARCHIVO"
    static let Post_Documentacion = "DOCUMENTACION"
    static let Post_ControlIdentidad = "CONTROLIDENTIDAD"
    static let Post_VerificarReniec = "VERIFICARSERVICIORENIEC"
    static let Post_ListTypeDesembolso = "LISTARTIPODESEMBOLSO"
    static let Post_Refuses = "MOTIVOSRECHAZO"
    static let Post_RefusesSol = "RECHAZARSOLICITUD"
    static let POST_ListRequest = "LISTARSOLICITUDES"
    static let Post_SendResult = "ACEPTARSOLICITUD"
}

extension String: ParameterEncoding {

    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }

}
