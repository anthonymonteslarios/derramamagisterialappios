//
//  InterfacesModel.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 14/11/20.
//

import Foundation
import Alamofire
import SwiftyJSON

// MARK: - Alamofire response handlers
extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    fileprivate func decodableResponseSerializerMsg<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            if var json = String(data: data, encoding: String.Encoding.utf8){
                json = json.replacingOccurrences(of: "success:", with: "\"success\":")
                json = json.replacingOccurrences(of: "mensaje:", with: "\"mensaje\":")
                json = json.replacingOccurrences(of: "icono:", with: "\"icono\":")
                json = json.replacingOccurrences(of: "\'", with: "\"")
                
                let data = json.data(using: .utf8)!
                return Result { try JSONDecoder().decode(T.self, from: data) }
            }
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    fileprivate func decodableResponseSerializerMsgSignature<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { request, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            if var json = String(data: data, encoding: String.Encoding.utf8){
                json = json.replacingOccurrences(of: "success:", with: "\"success\":")
                json = json.replacingOccurrences(of: "mensaje:", with: "\"mensaje\":")
                json = json.replacingOccurrences(of: "icono:", with: "\"icono\":")
                json = json.replacingOccurrences(of: "signature:", with: "\"signature\":")
                json = json.replacingOccurrences(of: "\'", with: "\"")
                
                let data = json.data(using: .utf8)!
                return Result { try JSONDecoder().decode(T.self, from: data) }
            }
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    //signature
    
    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    @discardableResult
    fileprivate func responseDecodableMsg<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializerMsg(), completionHandler: completionHandler)
    }
    
    @discardableResult
    fileprivate func responseDecodableMsgSignature<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializerMsgSignature(), completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseLogin(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseLogin>) -> Void) -> Self {
         return responseDecodable(queue: queue, completionHandler: completionHandler)
     }
    
    @discardableResult
    func responsePhoto(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponsePhoto>) -> Void) -> Self {
         return responseDecodable(queue: queue, completionHandler: completionHandler)
     }
    
    @discardableResult
    func responseGetData(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseProductsAll>) -> Void) -> Self {
         return responseDecodable(queue: queue, completionHandler: completionHandler)
     }
    
    @discardableResult
    func requestSaveRequest(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ErrorElement>) -> Void) -> Self {
         return responseDecodableMsg(queue: queue, completionHandler: completionHandler)
     }
    

     @discardableResult
     func responseResponseMessage(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseMessage>) -> Void) -> Self {
         return responseDecodable(queue: queue, completionHandler: completionHandler)
     }
 
 //Random Names
    @discardableResult
    func responseResponseRandomName(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseRandomName>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseResponseControlIdentidad(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseIdentidad>) -> Void) -> Self {
        return responseDecodableMsgSignature(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseDocumentacion(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ErrorResponse>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }

    @discardableResult
    func requestListDesembolso(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ErrorResponse>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func requestRefuseSol(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ErrorElement>) -> Void) -> Self {
        return responseDecodableMsg(queue: queue, completionHandler: completionHandler)
    }
    
    @discardableResult
    func responseGetListRequest(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseListRequest>) -> Void) -> Self {
         return responseDecodable(queue: queue, completionHandler: completionHandler)
     }
    
}
