// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let responseLogin = try? newJSONDecoder().decode(ResponseLogin.self, from: jsonData)

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responseResponseLoginElement { response in
//     if let responseLoginElement = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - ResponseLoginElement
struct ResponseLoginElement: Codable {
    let asoid, asoapepatdni, asoapematdni, asonomdni: String?
    let asotipid, regpenid, sitcta, asoncta: String?
    let asoapenomdni: String?
    let asoemail: String?
    let autdesapo, uproid, upagoid, useid: String?

    enum CodingKeys: String, CodingKey {
        case asoid = "ASOID"
        case asoapepatdni = "ASOAPEPATDNI"
        case asoapematdni = "ASOAPEMATDNI"
        case asonomdni = "ASONOMDNI"
        case asotipid = "ASOTIPID"
        case regpenid = "REGPENID"
        case sitcta = "SITCTA"
        case asoncta = "ASONCTA"
        case asoapenomdni = "ASOAPENOMDNI"
        case asoemail = "ASOEMAIL"
        case autdesapo = "AUTDESAPO"
        case uproid = "UPROID"
        case upagoid = "UPAGOID"
        case useid = "USEID"
    }
}

typealias ResponseLogin = [ResponseLoginElement]

struct DMUser {
    
    var user : ResponseLogin?
    
    static var shared = DMUser()
    
    private init () { }
    
}

enum UserDefault: String{
    case user = "User"
    case DNIUser = "DNI"
    case SessionActive = "SessionActive"
    case photo = "Photo"
    case requestComplete = "RequestComplete"
    case requestTypeCredit = "requestTypeCredit"
    case evaluationComplete = "evaluationComplete"
    case countIntentos = "countIntentos"
    
    case validateDocument = "validateDocument"
}



// MARK: - ErrorElement
struct ErrorElement: Codable {
    let success: Bool?
    let mensaje: String?
    let icono: Int?
    
    init() {
        success = false
        mensaje = ""
        icono = 0
    }
}

typealias ErrorResponse = [ErrorElement]

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}


// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    @discardableResult
    func responseResponseLogin(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ResponseLogin>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}


// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
