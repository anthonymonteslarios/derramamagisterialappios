//
//  Desembolso.swift
//  DerramaMagisterialAPP
//
//  Created by everis on 24/11/20.
//

import UIKit

struct Desembolso: Codable {
    let tipdeseid: String?
    let tipdeseabr: String?
    let tipdesedes: String?
    let flgoficio: String?
    let tipdev: String?
    let fpagoid: String?
    let actbco: String?
    let flagimp: String?
    let flglista: String?
    let activo: String?
    let tipdes: String?
    let tipvigcon: String?
    let flashispre: String?
    let flacobpre: String?
    let flacreapro: String?

    enum CodingKeys: String, CodingKey {
        case tipdeseid = "TIPDESEID"
        case tipdeseabr = "TIPDESEABR"
        case tipdesedes = "TIPDESEDES"
        case flgoficio = "FLGOFICIO"
        case tipdev = "TIPDEV"
        case fpagoid = "FPAGOID"
        case actbco = "ACTBCO"
        case flagimp = "FLAGIMP"
        case flglista = "FLGLISTA"
        case activo = "ACTIVO"
        case tipdes = "TIP_DES"
        case tipvigcon = "TIPVIGCON"
        case flashispre = "FLAVIGPRE"
        case flacobpre = "FLACOBPRE"
        case flacreapro = "FLA_CRE_APRO"
    }
}

typealias ResponseDesembolso = [Desembolso]
