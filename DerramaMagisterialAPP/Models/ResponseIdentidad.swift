//
//  ResponseIdentidad.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 22/11/20.
//

import Foundation
import SwiftyJSON

// MARK: - ResponseMessage
struct ResponseIdentidad: Codable {
    var success: Bool?
    var mensaje: String?
    var icono: Int?
    let signature : String?

    static func parse(_ json: JSON) -> ResponseIdentidad{
        var objBE = ResponseIdentidad()
        if let success = json.dictionary?["SUCCESS"]?.number{
            objBE.success = success.boolValue
        }else{
            objBE.success = json.dictionary?["SUCCESS"]?.string == "1"
        }
        objBE.mensaje = json.dictionary?["RESPONSE"]?.string
        return objBE
    }
    
    // MARK: - ErrorElement
    init() {
        success = false
        mensaje = ""
        icono = 0
        signature = ""
    }
    
    
}
