//
//  RandomName.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 21/11/20.
//

import Foundation
import Alamofire

// MARK: - ResponseRandomNameElement
struct ResponseRandomName: Codable {
    let NOMBRE: String?
}

typealias ResponseRandomNameArray = [ResponseRandomName]
