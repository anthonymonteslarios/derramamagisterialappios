//
//  Response.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 21/11/20.
//

import Foundation
import Alamofire

// MARK: - ResponseMessage
struct ResponseMessage: Codable {
    let success: Bool
    let mensaje: String
    let icono: Int
    
    
    init() {
        success = false
        mensaje = ""
        icono = 0
    }
    
}


