//
//  Request.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 27/11/20.
//

import Foundation
import Alamofire

// MARK: - ResponseListRequestElement
struct ResponseListRequestElement: Codable {
    let codsol: Int?
    let asoid, asodni, asoapenom, asotipid: String?
    let fecsol, ofertaTipcreid: String?
    let ofertaMonmin, ofertaMonmax, ofertaCuomin, ofertaCuomax: Int
    let ofertaMonto, ofertaPlazo: Double?
    let ofertaCuota, ofertaPorint, ofertaPorflt, ofertaPordes: Double?
    let ofertaCappago: Double?
    let ofertaNotaabono, ofertaDiagracia, ofertaDiaproceso, ofertaDiaprocesogracia: Double?
    let ofertaMonintproc: Double?
    let fecemi, feccad, nompadremadre: String?
    let correo, celular: String?
    let codestsol: Int?
    let usureg, fecreg, usumod, fecmod: String?
    let ofertaTea, ofertaTcea: Double?
    let desestsol, ofertaCodpad: String?
    let ofertaHaber, ofertaDescuento, ofertaLiquido: Double?
    let monmin, cuomin, monmax, cuomax: Int?
    let tea, tcea: Double?
    let monoto, nrocuo: Double?
    let moncuo: Double?
    let notabn, mondese, mondisp, segComercial: Double?
    let segRiesgo, porTasaUltCreditoOtor: Double?
    let porint, monintpro: Double?
    let diagracia, diaproceso, diaprocesogracia: Double?
    let validen: String?

    enum CodingKeys: String, CodingKey {
        case codsol = "CODSOL"
        case asoid = "ASOID"
        case asodni = "ASODNI"
        case asoapenom = "ASOAPENOM"
        case asotipid = "ASOTIPID"
        case fecsol = "FECSOL"
        case ofertaTipcreid = "OFERTA_TIPCREID"
        case ofertaMonmin = "OFERTA_MONMIN"
        case ofertaMonmax = "OFERTA_MONMAX"
        case ofertaCuomin = "OFERTA_CUOMIN"
        case ofertaCuomax = "OFERTA_CUOMAX"
        case ofertaMonto = "OFERTA_MONTO"
        case ofertaPlazo = "OFERTA_PLAZO"
        case ofertaCuota = "OFERTA_CUOTA"
        case ofertaPorint = "OFERTA_PORINT"
        case ofertaPorflt = "OFERTA_PORFLT"
        case ofertaPordes = "OFERTA_PORDES"
        case ofertaCappago = "OFERTA_CAPPAGO"
        case ofertaNotaabono = "OFERTA_NOTAABONO"
        case ofertaDiagracia = "OFERTA_DIAGRACIA"
        case ofertaDiaproceso = "OFERTA_DIAPROCESO"
        case ofertaDiaprocesogracia = "OFERTA_DIAPROCESOGRACIA"
        case ofertaMonintproc = "OFERTA_MONINTPROC"
        case fecemi = "FECEMI"
        case feccad = "FECCAD"
        case nompadremadre = "NOMPADREMADRE"
        case correo = "CORREO"
        case celular = "CELULAR"
        case codestsol = "CODESTSOL"
        case usureg = "USUREG"
        case fecreg = "FECREG"
        case usumod = "USUMOD"
        case fecmod = "FECMOD"
        case ofertaTea = "OFERTA_TEA"
        case ofertaTcea = "OFERTA_TCEA"
        case desestsol = "DESESTSOL"
        case ofertaCodpad = "OFERTA_CODPAD"
        case ofertaHaber = "OFERTA_HABER"
        case ofertaDescuento = "OFERTA_DESCUENTO"
        case ofertaLiquido = "OFERTA_LIQUIDO"
        case monmin = "MONMIN"
        case cuomin = "CUOMIN"
        case monmax = "MONMAX"
        case cuomax = "CUOMAX"
        case tea = "TEA"
        case tcea = "TCEA"
        case monoto = "MONOTO"
        case nrocuo = "NROCUO"
        case moncuo = "MONCUO"
        case notabn = "NOTABN"
        case mondese = "MONDESE"
        case mondisp = "MONDISP"
        case segComercial = "SEG_COMERCIAL"
        case segRiesgo = "SEG_RIESGO"
        case porTasaUltCreditoOtor = "POR_TASA_ULT_CREDITO_OTOR"
        case porint = "PORINT"
        case monintpro = "MONINTPRO"
        case diagracia = "DIAGRACIA"
        case diaproceso = "DIAPROCESO"
        case diaprocesogracia = "DIAPROCESOGRACIA"
        case validen = "VALIDEN"
    }
}

typealias ResponseListRequest = [ResponseListRequestElement]
