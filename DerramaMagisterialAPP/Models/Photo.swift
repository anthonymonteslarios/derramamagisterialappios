//
//  Photo.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 17/11/20.
//

import Foundation
import Alamofire

// MARK: - Usuario
struct ResponsePhoto: Codable {
    let dni, apepat, apemat, nombre: String?
    let otrdoc, lugnac, nompad, nommad: String?
    let fecnac, estatura, sexo, estciv: String?
    let grains, lugdom, restricciones, fecins: String?
    let fecemidoc, foto, usuact: String?
    var firma : String?
    let fecact, ofiact, equusu: String?

    enum CodingKeys: String, CodingKey {
        case dni = "DNI"
        case apepat = "APEPAT"
        case apemat = "APEMAT"
        case nombre = "NOMBRE"
        case otrdoc = "OTRDOC"
        case lugnac = "LUGNAC"
        case nompad = "NOMPAD"
        case nommad = "NOMMAD"
        case fecnac = "FECNAC"
        case estatura = "ESTATURA"
        case sexo = "SEXO"
        case estciv = "ESTCIV"
        case grains = "GRAINS"
        case lugdom = "LUGDOM"
        case restricciones = "RESTRICCIONES"
        case fecins = "FECINS"
        case fecemidoc = "FECEMIDOC"
        case foto = "FOTO"
        case firma = "FIRMA"
        case usuact = "USUACT"
        case fecact = "FECACT"
        case ofiact = "OFIACT"
        case equusu = "EQUUSU"
    }
    
}

struct DMPhoto {
    
    var photo : ResponsePhoto?
    
    
    static var shared = DMPhoto()
    
    private init () { }
    
}
