//
//  Refuse.swift
//  DerramaMagisterialAPP
//
//  Created by everis on 25/11/20.
//

import UIKit
import SwiftyJSON

struct Refuse: Codable {
    var codmotivo: Int?
    var desmotivo: String?
    
    
    static func parse(_ json: JSON) -> Refuse{
        var objBE = Refuse()
        objBE.codmotivo = json.dictionary?["CODMOTIVO"]?.int
        objBE.desmotivo = json.dictionary?["DESMOTIVO"]?.string
        return objBE
    }
    
    enum CodingKeys: String, CodingKey {
        case codmotivo = "CODMOTIVO"
        case desmotivo = "DESMOTIVO"
    }
}

typealias ResponseRefuses = [Refuse]
