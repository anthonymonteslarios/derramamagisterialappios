//
//  Products.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 14/11/20.
//

import Foundation
import Alamofire

// MARK: - ProductsAll
struct ResponseProductsAll: Codable {
    let montoMaximoOtorgar, montoMinimoOtorgar, plazoMaximoOtorgar, plazoMinimoOtorgar: String?
    let plazoOtorgar, success: String?
    let mensajeError: String?
    let tea, tcea, interesCredito: String?
    let montoDesembolsar: String?
    let montoOtorgar, cuotaOtorgar: String?
    let mensajeReultadoEvaluacion, ind, tipoCredito, flatCredito: String?
    let desgravamen, capacidadPagoMontoDisponible, diasDeGracia, diasEnProceso: String?
    let diasProcesoGracia, montoInteresProceso: String?
    let estadoSolicitud, estadoSolicitudLabel: String?
    let respuestaSolicitud, fechaEmision, fechaCaducidad: String?
    let celular, correo: String?
    let nombrePadre, nombreMadre, codpad, haber: String?
    let descuento, liquido: String?
    let username, notaAbono, sitcta, asoncta: String?
    let credid, codsol, autPorflt, autPorint: String?
    var autMonmin: String?
    let autCuomin, autMonmax, autCuomax: String?
    let autMonoto, autNrocuo, autMoncuo, autTea: String?
    let autTcea, autMonintpro, autDiagracia, autDiaproceso: String?
    let autDiaprocesogracia, autNotabn, autMensajeReultadoEvaluacion: String?
    var validen: String?
    let aceptadoMonoto, aceptadoNrocuo, aceptadoMoncuo, aceptadoTea: String?
    let aceptadoTcea, tipdeseid, autMontodesembolsar, aceptadoMontodesembolsar: String?
    let aceptadoNotabn, autorizacionDescuentoOnp: String?
    var recogerdoc: String?
    let asoid, asoapenomdni: String?
    let nrointento: String?
    let servicioReniecActivo: Bool
    let nrointentosmaximos: String?
    let activarTeapoyo: Bool
    let paralelo: String?
    let tipoparalelo: String?
    let asodni: String?
    let validaTeapoyo, plazoMinimoPosible, tasaPromocionalMaxima, ahorroCuotaPromocional: String?
    let ahorroTotalPromocional: String?

    enum CodingKeys: String, CodingKey {
        case montoMaximoOtorgar = "MONTO_MAXIMO_OTORGAR"
        case montoMinimoOtorgar = "MONTO_MINIMO_OTORGAR"
        case plazoMaximoOtorgar = "PLAZO_MAXIMO_OTORGAR"
        case plazoMinimoOtorgar = "PLAZO_MINIMO_OTORGAR"
        case plazoOtorgar = "PLAZO_OTORGAR"
        case success = "SUCCESS"
        case mensajeError = "MENSAJE_ERROR"
        case tea = "TEA"
        case tcea = "TCEA"
        case interesCredito = "INTERES_CREDITO"
        case montoDesembolsar = "MONTO_DESEMBOLSAR"
        case montoOtorgar = "MONTO_OTORGAR"
        case cuotaOtorgar = "CUOTA_OTORGAR"
        case mensajeReultadoEvaluacion = "MENSAJE_REULTADO_EVALUACION"
        case ind = "IND"
        case tipoCredito = "TIPO_CREDITO"
        case flatCredito = "FLAT_CREDITO"
        case desgravamen = "DESGRAVAMEN"
        case capacidadPagoMontoDisponible = "CAPACIDAD_PAGO_MONTO_DISPONIBLE"
        case diasDeGracia = "DIAS_DE_GRACIA"
        case diasEnProceso = "DIAS_EN_PROCESO"
        case diasProcesoGracia = "DIAS_PROCESO_GRACIA"
        case montoInteresProceso = "MONTO_INTERES_PROCESO"
        case estadoSolicitud = "ESTADO_SOLICITUD"
        case estadoSolicitudLabel = "ESTADO_SOLICITUD_LABEL"
        case respuestaSolicitud = "RESPUESTA_SOLICITUD"
        case fechaEmision = "FECHA_EMISION"
        case fechaCaducidad = "FECHA_CADUCIDAD"
        case celular = "CELULAR"
        case correo = "CORREO"
        case nombrePadre = "NOMBRE_PADRE"
        case nombreMadre = "NOMBRE_MADRE"
        case codpad = "CODPAD"
        case haber = "HABER"
        case descuento = "DESCUENTO"
        case liquido = "LIQUIDO"
        case username = "USERNAME"
        case notaAbono = "NOTA_ABONO"
        case sitcta = "SITCTA"
        case asoncta = "ASONCTA"
        case credid = "CREDID"
        case codsol = "CODSOL"
        case autPorflt = "AUT_PORFLT"
        case autPorint = "AUT_PORINT"
        case autMonmin = "AUT_MONMIN"
        case autCuomin = "AUT_CUOMIN"
        case autMonmax = "AUT_MONMAX"
        case autCuomax = "AUT_CUOMAX"
        case autMonoto = "AUT_MONOTO"
        case autNrocuo = "AUT_NROCUO"
        case autMoncuo = "AUT_MONCUO"
        case autTea = "AUT_TEA"
        case autTcea = "AUT_TCEA"
        case autMonintpro = "AUT_MONINTPRO"
        case autDiagracia = "AUT_DIAGRACIA"
        case autDiaproceso = "AUT_DIAPROCESO"
        case autDiaprocesogracia = "AUT_DIAPROCESOGRACIA"
        case autNotabn = "AUT_NOTABN"
        case autMensajeReultadoEvaluacion = "AUT_MENSAJE_REULTADO_EVALUACION"
        case validen = "VALIDEN"
        case aceptadoMonoto = "ACEPTADO_MONOTO"
        case aceptadoNrocuo = "ACEPTADO_NROCUO"
        case aceptadoMoncuo = "ACEPTADO_MONCUO"
        case aceptadoTea = "ACEPTADO_TEA"
        case aceptadoTcea = "ACEPTADO_TCEA"
        case tipdeseid = "TIPDESEID"
        case autMontodesembolsar = "AUT_MONTODESEMBOLSAR"
        case aceptadoMontodesembolsar = "ACEPTADO_MONTODESEMBOLSAR"
        case aceptadoNotabn = "ACEPTADO_NOTABN"
        case autorizacionDescuentoOnp = "AUTORIZACION_DESCUENTO_ONP"
        case recogerdoc = "RECOGERDOC"
        case asoid = "ASOID"
        case asoapenomdni = "ASOAPENOMDNI"
        case nrointento = "NROINTENTO"
        case servicioReniecActivo = "SERVICIO_RENIEC_ACTIVO"
        case nrointentosmaximos = "NROINTENTOSMAXIMOS"
        case activarTeapoyo = "ACTIVAR_TEAPOYO"
        case paralelo = "PARALELO"
        case tipoparalelo = "TIPOPARALELO"
        case asodni = "ASODNI"
        case validaTeapoyo = "VALIDA_TEAPOYO"
        case plazoMinimoPosible = "PLAZO_MINIMO_POSIBLE"
        case tasaPromocionalMaxima = "TASA_PROMOCIONAL_MAXIMA"
        case ahorroCuotaPromocional = "AHORRO_CUOTA_PROMOCIONAL"
        case ahorroTotalPromocional = "AHORRO_TOTAL_PROMOCIONAL"
    }
}

struct DMProduct {
    
    var credit : ResponseProductsAll?
    var creditTeApoyo : ResponseProductsAll?
    
    static var shared = DMProduct()
    
    private init () { }
    
}

struct DMMyProductsModel {
    let title : String
    let desc : String
    let bold : [String]
}
