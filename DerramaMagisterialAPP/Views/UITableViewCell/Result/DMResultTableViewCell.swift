//
//  DMResultTableViewCell.swift
//  DerramaMagisterialAPP
//
//  Created by everis on 26/11/20.
//

import UIKit

class DMResultTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
        
    var index : IndexPath!
    
    static let cellId = "DMResultTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .black
        titleLabel.personalizeLabelStyle(fontSize: 10, isBold: false)
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.checkButton.imageView?.tintColor = Colors.primaryColor()
    }

    func loadData(title: String, isSelect: Bool){
        titleLabel.text = title
        checkButton.isSelected = isSelect
    }
    
    //MARK: - Class methods
    static func getNib() -> UINib {
        return UINib(nibName: DMResultTableViewCell.cellId, bundle: Bundle(for: DMResultTableViewCell.self))
    }
}
