//
//  DMProfileTableViewCell.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import UIKit

class DMProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleCategoryLabel: UILabel?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var lineView: UIView?
    @IBOutlet weak var selectView: UIView!
    
    static let cellId = "DMProfileTableViewCell"
    static let cellWithTitleId = "DMProfileTitleTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = Colors.textMenuColor()
        titleLabel.personalizeLabelStyle(fontSize: 12, isBold: true)
        
        titleCategoryLabel?.numberOfLines = 0
        titleCategoryLabel?.textAlignment = .left
        titleCategoryLabel?.textColor = Colors.textMenuColor()
        titleCategoryLabel?.personalizeLabelStyle(fontSize: 12, isBold: false)
        
        selectView.layer.maskedCorners = self.getMaskedCorners()
        selectView.layer.cornerRadius  = 8
        
        lineView?.backgroundColor = Colors.separatorMenuCellColor()
        
        menuImageView.tintColor = Colors.textMenuColor()
    }
    
    func loadCell(type: DMMenuType) {
        
        var title = ""
        var image = ""
        var titleCategory = ""
        
        switch type {
        case .products:
            title = "Mis Productos"
            image = "icon_products_menu"
        case .requestCredit:
            title = "Solicita tu crédito"
            image = "icon_request_credit_menu"
        case .request:
            title = "Mis solicitudes"
            image = "icon_request_menu"
        case .individual:
            title = "Mi Cuenta Individual"
            image = "icon_individual_menu"
        case .mycredits:
            title = "Mis Créditos"
            image = "icon_my_credits_menu"
        case .legal:
            title = "Asesoría Legal Gratuita"
            image = "icon_legal_menu"
        case .notification:
            title = "Notificaciones"
            image = "icon_notification_menu"
            titleCategory = "Configuración"
        case .digital:
            title = "Cambia tu clave digital"
            image = "ico-lock"
            titleCategory = "Seguridad"
        case .updatedata:
            title = "Actualiza tus datos"
            image = "icon_update_menu"
            titleCategory = "Mi Perfil"
        default:
            title = "Cerrar sesión"
            image = "icon_legal_menu"
        }
        
        titleCategoryLabel?.text = titleCategory
        titleLabel.text = title
        menuImageView.image = UIImage(named: image)?.withRenderingMode(.alwaysTemplate)
        
        layoutIfNeeded()
    }
    
    func select(_ seleccionar : Bool){
        
        let textColor = seleccionar == true ? Colors.textSelectMenuColor() : Colors.textMenuColor()
        let viewColor = seleccionar == true ? Colors.viewSelectMenuColor() : .clear
        titleLabel.textColor = textColor
        selectView.backgroundColor = viewColor
        menuImageView.tintColor = textColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        select(selected)
    }
    
    public func getMaskedCorners() -> CACornerMask {
        
        var arrayCornerMask = [CACornerMask]()
        
        arrayCornerMask.append(.layerMinXMinYCorner)
        arrayCornerMask.append(.layerMinXMaxYCorner)
        
        return arrayCornerMask.count != 0 ? CACornerMask(arrayCornerMask) : [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }
    
    //MARK: - Class methods
    static func getNib() -> UINib {
        return UINib(nibName: DMProfileTableViewCell.cellId, bundle: Bundle(for: DMProfileTableViewCell.self))
    }
    
    static func getNibWithTile() -> UINib {
        return UINib(nibName: DMProfileTableViewCell.cellWithTitleId, bundle: Bundle(for: DMProfileTableViewCell.self))
    }
}
