//
//  DMProductsTableViewCell.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/14/20.
//

import UIKit

protocol DMProductsDelegate{
    func GoToRequest(index:Int)
}

class DMProductsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var iWantItButton : UIButton!
    var delegate : DMProductsDelegate?

    static let cellId = "DMProductsTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        backgroundColor = .clear
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = Colors.productTextColor()
        titleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .left
        
        containerView.layer.cornerRadius = 12.0
        containerView.backgroundColor = Colors.cardColor()
        
        iWantItButton.personalizeButtonBasicStyle(with: "¡LO QUIERO!")
    }
    
    func loadCell(loadData: ResponseProductsAll) {
        
        var title = ""
        var desc = ""
        var bolds : [String] = []
        
        if loadData.paralelo == Constant.CreditoConsumo{
            title = "Crédito de Consumo"
            desc = "Tienes un crédito de hasta \(formatAmount(data: loadData)) con una tasa especial. Elige el monto y plazo que se ajuste a tus necesidades."
            bolds = ["\(formatAmount(data: loadData))"]
        }else{
            title = "Crédito #teApoyo"
            desc = "Para tus necesidades inmediatas De \(formatAmount(data: loadData)) a \(loadData.plazoOtorgar ?? "") meses para pagar y con una tasa especial de \(loadData.interesCredito ?? "")%."
            bolds = ["\(formatAmount(data: loadData))", "tasa especial de \(loadData.interesCredito ?? "")%."]
        }
        
        titleLabel.text = title
        descLabel.attributedText = getAttributeDesc(text: desc, boldStrings: bolds)
        layoutIfNeeded()
    }
    
    private func formatAmount(data: ResponseProductsAll) -> String {
        let credit = data.montoMaximoOtorgar ?? ""
        guard let amount = Double(credit) else {
            return ""
        }
        return Constant.coinSymbolPEN+" "+Utils.formatIntegerAmount(amount: amount)
    }
    
    private func getAttributeDesc(text: String, boldStrings: [String]) -> NSAttributedString {
        let description = NSMutableString(string: text)
        let attributeString = NSMutableAttributedString(string: text)
        attributeString.addAttribute(.font, value: UIFont(name: Constant.dmFont, size: 13)!, range: NSRange(location: 0, length: attributeString.length))
        
        for boldString in boldStrings {
            let range : NSRange = description.range(of: boldString, options: .caseInsensitive)
            attributeString.addAttribute(.font, value: UIFont(name: Constant.dmFontBold, size: 13)!, range: range)
            attributeString.addAttribute(.foregroundColor, value: Colors.productCellTextColor(), range: range)
        }
        return attributeString
    }
    
    @IBAction func goToRequest(_ sender: UIButton) {
        delegate?.GoToRequest(index: sender.tag)
       }
    
    //MARK: - Class methods
    static func getNib() -> UINib {
        return UINib(nibName: DMProductsTableViewCell.cellId, bundle: Bundle(for: DMProductsTableViewCell.self))
    }
}
