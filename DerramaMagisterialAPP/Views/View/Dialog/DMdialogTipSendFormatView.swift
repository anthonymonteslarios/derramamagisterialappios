//
//  DMdialogTipSendFormatView.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 19/11/20.
//

import UIKit

protocol DMdialogTipDelegate {
    func DocumentacionSend(email : String)
}

class DMdialogTipSendFormatView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet var emailTextfield: UITextField!
    @IBOutlet weak var checkbox: CheckBox!
    @IBOutlet weak var containerEmailView: UIView!
    var delegate : DMdialogTipDelegate?
    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()
        descriptionLabel.text = "Estimado docente, asegúrese que la foto tomada muestre toda la información del documento y esta sea legible, este es un requisito indispensable  para la aprobación de su crédito.\n\nMuchas gracias"
    }
    
    private func addStyleToElements() {
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .white
        containerEmailView.layer.cornerRadius = 4.0
        containerEmailView.layer.borderWidth = 1.0
        containerEmailView.layer.borderColor = UIColor.black.cgColor
        
        //titleLabel.personalizeLabelStyle(fontSize: 12, isBold: false)
                
        //containerView.backgroundColor = Colors.primaryColor()
    }
    @IBAction func cancelButtonAction(_ sender: Any) {
        removeFromSuperview()
    }
    
    @IBAction func acceptButtonAction(_ sender: Any) {
        delegate?.DocumentacionSend(email: emailTextfield.text!)
        removeFromSuperview()
    }
    func setupView(title: String, image: UIImage) {
        titleLabel.text = title
        
        layoutIfNeeded()
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMdialogTipSendFormatView{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMdialogTipSendFormatView", owner: self, options: nil)?.first else {
            return DMdialogTipSendFormatView.init()
        }
        return view as! DMdialogTipSendFormatView
    }
    
}


