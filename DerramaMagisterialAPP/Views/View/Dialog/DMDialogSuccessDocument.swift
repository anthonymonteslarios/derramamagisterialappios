//
//  DMDialogSuccessDocument.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 23/11/20.
//

import UIKit


class DMDialogSuccessDocument: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    

    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()
        titleLabel.text = "Correo enviado correctamente"
        descriptionLabel.text = "\nDocumentos enviados:\n\n-Contrato del Crédito\n-Autorización de Descuento\n-Hoja de Resumen\n-Pagaré\n"
    }
    
    private func addStyleToElements() {
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .white
        titleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        descriptionLabel.personalizeLabelStyle(fontSize: 16, isBold: false)
    }
    

    @IBAction func acceptButtonAction(_ sender: Any) {
        removeFromSuperview()
    }
    func setupView(title: String, image: UIImage) {
        titleLabel.text = title
        
        layoutIfNeeded()
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMDialogSuccessDocument{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMDialogSuccessDocument", owner: self, options: nil)?.first else {
            return DMDialogSuccessDocument.init()
        }
        return view as! DMDialogSuccessDocument
    }
    
}


