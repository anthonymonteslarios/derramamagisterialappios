//
//  DMdialogIdentityCheckView.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 19/11/20.

import UIKit

protocol DMdialogIdentityCheckDelegate{
    
    func acceptActionDelegate(fechaEmision: String, fechaCaducidad: String, nombrePadres: String, caduca: String)
    
}

class DMdialogIdentityCheckView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noCaducaLabel: UILabel!
    
    @IBOutlet weak var CheckBox: CheckBox!
    @IBOutlet var fechaEmisionTextfield: UITextField!
    @IBOutlet var fechaCaducidadTextfield: UITextField!
    @IBOutlet var nombrePadreMadreTextfield: UITextField!
    @IBOutlet weak var nameFatherView: UIView!
    @IBOutlet weak var checkboxButton: UIButton!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    

    var delegate : DMdialogIdentityCheckDelegate?
    
    @IBOutlet weak var fechaCaducidadView: UIView!
    
    
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    
    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()

    }
    
    func dismissView(){
        removeFromSuperview()
    }
    
    
    @IBAction func acceptButtonAction(_ sender: Any) {
        delegate?.acceptActionDelegate(fechaEmision: fechaEmisionTextfield.text!, fechaCaducidad: fechaCaducidadTextfield.text! , nombrePadres: nombrePadreMadreTextfield.text!, caduca: CheckBox.isChecked ? "S" : "N")
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        removeFromSuperview()
    }
    private func addStyleToElements() {
        titleLabel.personalizeLabelStyle(fontSize: 18, isBold: true)
        
        nameFatherView.layer.cornerRadius = 5.0
        nameFatherView.layer.borderWidth = 1.0
        nameFatherView.layer.borderColor = UIColor.colorFromHexString("#D9D9D9", withAlpha: 1.0).cgColor
        
        containerHeightConstraint.constant = 50
        //containerView.backgroundColor = Colors.primaryColor()
        
        fechaEmisionTextfield.tintColor = UIColor.clear
        fechaCaducidadTextfield.tintColor = UIColor.clear
        nombrePadreMadreTextfield.tintColor = UIColor.clear
    }
    
    func setupView(title: String, image: UIImage) {
        titleLabel.text = title
        
        layoutIfNeeded()
    }
    @IBAction func checkboxAction(_ sender: Any) {
        
        if !CheckBox.isChecked{
            containerHeightConstraint.constant = 0
            fechaCaducidadView.isHidden = true
            fechaCaducidadView.alpha = 0
        }else{
            containerHeightConstraint.constant = 50
            fechaCaducidadView.isHidden = false
            fechaCaducidadView.alpha = 1
        }
        
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMdialogIdentityCheckView{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMdialogIdentityCheckView", owner: self, options: nil)?.first else {
            return DMdialogIdentityCheckView.init()
        }
        return view as! DMdialogIdentityCheckView
    }
    
}


