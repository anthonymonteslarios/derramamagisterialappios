//
//  DMDialogRefuseResult.swift
//  DerramaMagisterialAPP
//
//  Created by everis on 24/11/20.
//

import UIKit

protocol DMDialogRefuseResultDelegate{
    
    func acceptActionDelegate(motivo: String, cod: String)
    func cancelActionDelegate()
}

class DMDialogRefuseResultView: UIView {

    @IBOutlet weak var containerView: UIView!

    @IBOutlet var nombrePadreMadreTextfield: UITextField!
    @IBOutlet weak var nameFatherView: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!

    var delegate : DMDialogRefuseResultDelegate?
    var cod: Int!
    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()

    }
    @IBAction func acceptButtonAction(_ sender: Any) {
        delegate?.acceptActionDelegate(motivo: nombrePadreMadreTextfield.text!, cod: "\(cod ?? 0)")
        removeFromSuperview()
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        delegate?.cancelActionDelegate()
        removeFromSuperview()
    }
    private func addStyleToElements() {

        nameFatherView.layer.cornerRadius = 5.0
        nameFatherView.layer.borderWidth = 1.0
        nameFatherView.layer.borderColor = UIColor.colorFromHexString("#D9D9D9", withAlpha: 1.0).cgColor
    
        nombrePadreMadreTextfield.tintColor = UIColor.clear
    }

    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMDialogRefuseResultView{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMDialogRefuseResultView", owner: self, options: nil)?.first else {
            return DMDialogRefuseResultView.init()
        }
        return view as! DMDialogRefuseResultView
    }
    
}
