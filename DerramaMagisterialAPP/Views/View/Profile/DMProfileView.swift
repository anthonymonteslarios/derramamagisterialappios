//
//  DMProfileView.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import UIKit

class DMProfileView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()
    }
    
    private func addStyleToElements() {
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.textColor = .white
        titleLabel.personalizeLabelStyle(fontSize: 15, isBold: false)
        
        imageView.tintColor = .white
        containerView.backgroundColor = Colors.primaryColor()
    }
    
    func setupView(title: String, image: UIImage) {
        
        self.imageView.image = UIImage()        
        DispatchQueue.main.async {
            self.setNeedsDisplay()
            self.layoutIfNeeded()
            self.titleLabel.text = title
            self.imageView.layer.borderWidth = 1
            self.imageView.layer.masksToBounds = true
            self.imageView.layer.borderColor = UIColor.black.cgColor
            self.imageView.image = image
            self.imageView.layer.cornerRadius = self.imageView.frame.height/2
            self.imageView.clipsToBounds = true
            self.imageView.contentMode = .scaleAspectFill
            
        }
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMProfileView{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMProfileView", owner: self, options: nil)?.first else {
            return DMProfileView.init()
        }
        return view as! DMProfileView
    }
    
}
