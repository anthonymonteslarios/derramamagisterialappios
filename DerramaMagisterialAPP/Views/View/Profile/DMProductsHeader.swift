//
//  DMProductsHeader.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/14/20.
//

import UIKit

class DMProductsHeader: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        addStyleToElements()
    }
    
    private func addStyleToElements() {
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = Colors.productTextColor()
        titleLabel.personalizeLabelStyle(fontSize: 17, isBold: true)

        containerView.backgroundColor = .clear
        backgroundColor = .clear
    }
    
    func setupView(title: String) {
        titleLabel.text = title
        layoutIfNeeded()
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMProductsHeader{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMProductsHeader", owner: self, options: nil)?.first else {
            return DMProductsHeader.init()
        }
        return view as! DMProductsHeader
    }
}
