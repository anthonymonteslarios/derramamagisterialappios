//
//  DMHeaderReqCreditView.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/15/20.
//

import UIKit

enum TabsUnlocktype: Int{
    case none = 0
    case enviado = 1
    case proceso = 2
    case evaluado = 3
    case otorgado = 6
    
    func getIndex() -> Int? {
        switch self {
        case .enviado:
            return 1
        case .proceso:
            return 1
        case .evaluado:
            return 2
        case .otorgado:
            return 3
        default:
            return 0
        }
    }
}

protocol DMHeaderReqCreditViewDelegate: class {
    func selectTab(index: Int)
}

class DMHeaderReqCreditView: UIView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var sendView: UIView!
    @IBOutlet weak var tabsView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var constraintSelected: NSLayoutConstraint!
    
    @IBOutlet var arrayTabView: [UIView]!
    @IBOutlet var arrayTabImage: [UIImageView]!
    @IBOutlet var arrayTabLabel: [UILabel]!
    
    weak var delegate: DMHeaderReqCreditViewDelegate?
    
    //MARK: - Private methods
    override func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    private func initView() {
        setData()
        addStyleToElements()
        setStyleLabels()
        setGestures()
    }
    
    func loadTitle(title:String?){
        if let title = title{
            titleLabel.text = "ESTADO: \(title)"
        }
    }
    
    private func addStyleToElements() {
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.personalizeRoundStyle()
        titleLabel.personalizeLabelStyle(fontSize: 13, isBold: true)
        sendView.backgroundColor = Colors.primaryColor()
        tabsView.backgroundColor = Colors.primaryColor()
    }
    
    private func setGestures(){
        for view in arrayTabView{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectTab))
            view.addGestureRecognizer(tapGesture)
        }
    }
    
    @objc func selectTab(gesture: UITapGestureRecognizer) {
        if let index = gesture.view?.tag{
            setColorsSelect(index)
            delegate?.selectTab(index: index)
        }
    }
    
    private func setStyleLabels(){
        for view in arrayTabLabel{
            view.numberOfLines = 0
            view.textAlignment = .center
            view.personalizeLabelStyle(fontSize: 12, isBold: false)
        }
    }
    
    func disableTabs(stepType:TabsUnlocktype){
        for view in arrayTabView{
            switch stepType {
            case .none:
                view.isUserInteractionEnabled = view.tag == 0
            case .enviado:
                view.isUserInteractionEnabled = view.tag == 0 || view.tag == 1
            case .proceso:
                view.isUserInteractionEnabled = view.tag == 0 || view.tag == 1 || view.tag == 2
            case .evaluado:
                view.isUserInteractionEnabled = view.tag == 0 || view.tag == 1 || view.tag == 2
           default:
                view.isUserInteractionEnabled = view.tag == 0 || view.tag == 1 || view.tag == 2 || view.tag == 3
            }
        }
    }
    
    func setColorsSelect(_ indexSelect: Int){
        
        for (index, view) in arrayTabView.enumerated(){
            if index == indexSelect{
                animateUnderlineSection(view)
            }
        }
        
        for (index, view) in arrayTabLabel.enumerated(){
            if index == indexSelect{
                view.textColor = .white
            }else{
                view.textColor = Colors.tabDeselectColor()
            }
        }
        
        for (index, view) in arrayTabImage.enumerated(){
            if index == indexSelect{
                view.tintColor = .white
            }else{
                view.tintColor = Colors.tabDeselectColor()
            }
        }
    }
    
    private func setData(){
        arrayTabImage[0].image = UIImage(named: "icon_evaluation")?.withRenderingMode(.alwaysTemplate)
        arrayTabImage[1].image = UIImage(named: "icon_notification_menu")?.withRenderingMode(.alwaysTemplate)
        arrayTabImage[2].image = UIImage(named: "icon_result")?.withRenderingMode(.alwaysTemplate)
        arrayTabImage[3].image = UIImage(named: "icon_camera")?.withRenderingMode(.alwaysTemplate)
        
        arrayTabLabel[0].text = "Evaluación"
        arrayTabLabel[1].text = "Información"
        arrayTabLabel[2].text = "Resultados"
        arrayTabLabel[3].text = "Documentos"
    }
    
    private func animateUnderlineSection(_ view: UIView){
        
        UIView.animate(withDuration: 0.35, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
            
            self.constraintSelected.constant = view.frame.origin.x + (view.bounds.size.width / 2) - (UIScreen.main.bounds.size.width / 2)
            self.layoutIfNeeded()
            
        }, completion: nil)
    }
    
    //MARK: - Class methods
    class func instanceViewFromXIB() -> DMHeaderReqCreditView{
        let bundle = Bundle(for: self)
        guard let view = bundle.loadNibNamed("DMHeaderReqCreditView", owner: self, options: nil)?.first else {
            return DMHeaderReqCreditView.init()
        }
        return view as! DMHeaderReqCreditView
    }
}
