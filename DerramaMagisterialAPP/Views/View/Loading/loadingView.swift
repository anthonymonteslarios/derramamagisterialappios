//
//  loadingView.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 8/12/20.
//

import UIKit
import NVActivityIndicatorView

class loadingView: UIView {

    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    
    override func awakeFromNib() {
        containerView.layer.cornerRadius = 5.0
    }
    
    class func instanceFromNib() -> loadingView {
        return UINib(nibName: "loadingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! loadingView
    }


}
