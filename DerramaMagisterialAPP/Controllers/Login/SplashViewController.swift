//
//  SplashViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 14/11/20.
//

import UIKit

class SplashViewController: UIViewController {
    @IBOutlet weak var progressBar: UIProgressView!
    var progressBarTimer: Timer!
    var sessionTime = false
        
    override func viewDidLoad() {
        super.viewDidLoad()

        progressBar.progress = 0.0
        progressBar.layer.cornerRadius = 10
        progressBar.clipsToBounds = true
        progressBar.layer.sublayers![1].cornerRadius = 10
        progressBar.subviews[1].clipsToBounds = true
        progressBar.progress = 0.0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [self] in
            self.progressBarTimer = Timer.scheduledTimer(timeInterval: 0.15, target: self, selector:
                                                            #selector(self.updateProgressView), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateProgressView(){
        progressBar.progress += 0.1
        progressBar.setProgress(progressBar.progress, animated: true)
        if(progressBar.progress == 1.0)
        {
            progressBarTimer.invalidate()            
            goToController(animated: true)
        }
    }
    
    func goToController(animated : Bool){
        let vc = UIStoryboard.init(name: "Login", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
        vc?.modalPresentationStyle = .fullScreen
        vc?.sessionTime = sessionTime
        sessionTime = false
        present(vc!, animated: animated, completion: nil)
    }
    
}
