//
//  LoginViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 14/11/20.
//

import UIKit
import NVActivityIndicatorView
import SafariServices

class LoginViewController: UIViewController, NVActivityIndicatorViewable {

    
    //Mark: UI
    @IBOutlet weak var containerDocumentView: UIView!
    @IBOutlet weak var containerPasswordView: UIView!
        
    @IBOutlet weak var documentTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var sessionTime : Bool = false
    
    let group = DispatchGroup()
    var loading : loadingView?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(false, forKey: UserDefault.SessionActive.rawValue)
        addStyleElements()
        documentTextfield.text = "" // 02868313 //47868920 //43376692
    }
    
    override func viewWillAppear(_ animated: Bool) {
        documentTextfield.text = ""
        passwordTextfield.text = ""
        
        if sessionTime{
            sessionTime = false
            let alert = UIAlertController(title: "", message:"Su sesión ha expirado, por favor vuelva a ingresar", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))            
            present(alert, animated: true, completion: nil)
        }
    }
    
    
    func addStyleElements(){
        containerDocumentView.layer.borderWidth = 1.0
        containerDocumentView.layer.borderColor = UIColor.gray.cgColor
        containerDocumentView.layer.cornerRadius = 4.0
        
        containerPasswordView.layer.borderWidth = 1.0
        containerPasswordView.layer.borderColor = UIColor.gray.cgColor
        containerPasswordView.layer.cornerRadius = 4.0
        
        loginButton.layer.cornerRadius = 4.0
        
        documentTextfield.smartInsertDeleteType = UITextSmartInsertDeleteType.no
    }
    
    func getPhoto(){
        
        //DispatchQueue.global(qos: .default).async {
            
            Manager.shared.requestPhotoDonor(Dni: self.documentTextfield.text!, completion: {(photo, error) in
                if (error.first != nil) {
                    print("error: \(String(describing: error.first?.mensaje))")
                    self.stopLoadingAnimating()
                }
                

                guard let photo = photo else {
                    self.stopLoadingAnimating()

                    let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: false, completion: nil)
                    return
                }
                
                DMPhoto.shared.photo = photo
                
                UserDefaults.standard.set(try? PropertyListEncoder().encode(photo), forKey:UserDefault.photo.rawValue)

                self.stopLoadingAnimating()

                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SWRevealViewController") as? SWRevealViewController
                vc?.modalPresentationStyle = .fullScreen
                self.present(vc!, animated: false, completion: nil)
            })
        //}
    }
    
    func loadingAnimation(){
        loading = loadingView.instanceFromNib()
        loading?.frame = view.frame
        view.addSubview(loading!)
        loading!.loadingView.startAnimating()
    }
    
    func stopLoadingAnimating(){
        loading!.loadingView.stopAnimating()
        loading?.removeFromSuperview()
        //view.removeFromSuperview()
    }
    
    @IBAction func loginButtonAction(_ sender: Any) {
        view.endEditing(true)
        if self.documentTextfield.hasText && self.passwordTextfield.hasText{
            //startAnimating()
            loadingAnimation()
            
        Manager.shared.requestLoginDonor(Dni: self.documentTextfield.text!, Password: self.passwordTextfield.text!) { [self](user, error) in
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.stopLoadingAnimating()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: Constant.acceptButton)
                return
            }

            UserDefaults.standard.set(try? PropertyListEncoder().encode(user), forKey:UserDefault.user.rawValue)
            UserDefaults.standard.set(self.documentTextfield.text, forKey: UserDefault.DNIUser.rawValue)
            UserDefaults.standard.set(true, forKey: UserDefault.SessionActive.rawValue)
            UserDefaults.standard.set(false, forKey: UserDefault.requestComplete.rawValue)
            //Validate
            UserDefaults.standard.set(true, forKey: UserDefault.validateDocument.rawValue)
            
            print("- Done")
            self.getPhoto()
            
            guard let user = user else {
                return
            }
                            
            DMUser.shared.user = user


        }
        }else{
            alertasNativas(title: Constant.textEmpty, message: Constant.validateLogin, acceptButton: Constant.acceptButton)
        }
    }
    
    @IBAction func createAccountAction(_ sender: Any) {
        if let url = URL(string: Constant.URL_EXTERNAL) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
        //Appstore rechazo abrir el navegador de externo
        /*if let url = URL(string: Constant.URL_EXTERNAL) {
            UIApplication.shared.open(url)
        }*/
    }
    @IBAction func callAction(_ sender: Any) {
        alertasNativas(title: Constant.textEmpty, message: Constant.textNextVersion, acceptButton: Constant.acceptButton)
    }
    
    @IBAction func locationAction(_ sender: Any) {
        alertasNativas(title: Constant.textEmpty, message: Constant.textNextVersion, acceptButton: Constant.acceptButton)
    }
    
}
extension LoginViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if textField == documentTextfield {
            if count >= 8 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                    passwordTextfield.becomeFirstResponder()
                }
            }
        }
        
        return count <= (textField == documentTextfield ? 8 : 4)
    }
    
}
