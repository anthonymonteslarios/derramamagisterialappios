//
//  DMMenuViewController.swift
//  DemoNavigation
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import UIKit

enum DMMenuType {
    case products
    case requestCredit
    case request
    case individual
    case mycredits
    case legal
    case notification
    case digital
    case updatedata
    case logout
}

struct DMMenuViewCell {
    let viewController : UIViewController
    let type : DMMenuType
}

class DMMenuViewController: UIViewController{

    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var dmProfileView: DMProfileView!
    var Photo: ResponsePhoto?
    
    //MarK: Call User Model
    var User: ResponseLogin?

    lazy var menus : [DMMenuViewCell] = {
        var array = [DMMenuViewCell]()
        
        let controllerProduct = UIViewController.getController(storyBoard: "Products", indentifier: "Products")
        let controllerRequestCredit = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "RequestCredit")
        let controllerRequest = UIViewController.getController(storyBoard: "MyRequest", indentifier: "Request")
        let controllerIndividual = UIViewController()
        let controllerMyCredits = UIViewController()
        let controllerLegalAdvisory = UIViewController()
        let controllerNotification = UIViewController()
        let controllerDigitalPassword = UIViewController()
        let controllerUpdateData = UIViewController()
        let controllerLogOut = UIViewController()
        
        let menuProdcut = DMMenuViewCell(viewController: controllerProduct, type: .products)
        let menuRequestCredit = DMMenuViewCell(viewController: controllerRequestCredit, type: .requestCredit)
        let menuRequest = DMMenuViewCell(viewController: controllerRequest, type: .request)
        let menuIndividual = DMMenuViewCell(viewController: controllerIndividual, type: .individual)
        let menuMyCredits = DMMenuViewCell(viewController: controllerMyCredits, type: .mycredits)
        let menuLegalAdvisory = DMMenuViewCell(viewController: controllerLegalAdvisory, type: .legal)
        let menuNotification = DMMenuViewCell(viewController: controllerNotification, type: .notification)
        let menuDigitalPassword = DMMenuViewCell(viewController: controllerDigitalPassword, type: .digital)
        let menuUpdateData = DMMenuViewCell(viewController: controllerUpdateData, type: .updatedata)
        let menuLogOut = DMMenuViewCell(viewController: controllerLogOut, type: .logout)
        
        array.append(menuProdcut)
        array.append(menuRequestCredit)
        array.append(menuRequest)
        array.append(menuIndividual)
        array.append(menuMyCredits)
        array.append(menuLegalAdvisory)
        array.append(menuNotification)
        array.append(menuDigitalPassword)
        array.append(menuUpdateData)
        array.append(menuLogOut)
        
        return array
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        loadNotification()
    }

    
    func loadNotification(){
        let notificationName = Notification.Name("ShowToRequest")
        NotificationCenter.default.addObserver(self, selector: #selector(ShowToRequest), name: notificationName, object: nil)
        let notificationNameProducts = Notification.Name("ShowToProducts")
        NotificationCenter.default.addObserver(self, selector: #selector(ShowToProducts), name: notificationNameProducts, object: nil)
    }
    
    
    // MARK: - Own Methods
    private func initView() {
        getUserPhoto()
        getUserData()
        addScreenValues()
        configureTableView()        
    }
    
    private func getUserPhoto(){
        if let data = UserDefaults.standard.value(forKey:UserDefault.photo.rawValue) as? Data {
            let userData = try? PropertyListDecoder().decode(ResponsePhoto.self, from: data)
            Photo = userData
        }
    }
    
    private func addScreenValues() {

        dmProfileView = DMProfileView.instanceViewFromXIB()
        dmProfileView.setupView(title: User?.first?.asoapenomdni ?? "", image: (Photo == nil) ? UIImage(named: "img-profile-fake")! : Utils.convertBase64ToImage(imageString: Photo!.foto!))
        
        profileView.addSubview(dmProfileView)
        var frame = dmProfileView.frame
        frame.origin = CGPoint.zero
        frame.size = profileView.frame.size
        dmProfileView.frame = frame
    }
    
    private func configureTableView() {
        tableView.register(DMProfileTableViewCell.getNib(), forCellReuseIdentifier: DMProfileTableViewCell.cellId)
        tableView.register(DMProfileTableViewCell.getNibWithTile(), forCellReuseIdentifier: DMProfileTableViewCell.cellWithTitleId)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)


    }
    
    func getUserData(){
        if let data = UserDefaults.standard.value(forKey:UserDefault.user.rawValue) as? Data {
            let userData = try? PropertyListDecoder().decode(ResponseLogin.self, from: data)
            User = userData
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc public func ShowToRequest(){
        tableView.selectRow(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .none)
        self.revealViewController().frontViewController = menus[2].viewController        
    }
    
    @objc public func ShowToProducts(){
        tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        self.revealViewController().frontViewController = menus[0].viewController
    }
    
    
}

// MARK: - UITableViewDelegate
extension DMMenuViewController : UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let type = menus[indexPath.row].type
        if type == .notification || type == .digital || type == .updatedata{
            return cellProfileWithTitle(tableView, cellForRowAt: indexPath)
        }else{
            return cellProfile(tableView, cellForRowAt: indexPath)
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let type = menus[indexPath.row].type
        if type == .products || type == .requestCredit || type == .request{
            self.revealViewController().frontViewController = menus[indexPath.row].viewController
            self.revealViewController().perform(#selector(SWRevealViewController.revealToggle(_:)), with: nil, afterDelay: 0.2)
            UserDefaults.standard.set(nil, forKey: UserDefault.requestTypeCredit.rawValue)
        }else if type == .logout{
            UserDefaults.standard.set("", forKey:UserDefault.user.rawValue)
            UserDefaults.standard.set("", forKey: UserDefault.DNIUser.rawValue)
            UserDefaults.standard.set(false, forKey: UserDefault.SessionActive.rawValue)
            UserDefaults.standard.set(false, forKey: UserDefault.requestComplete.rawValue)
            //Validate
            UserDefaults.standard.set(true, forKey: UserDefault.validateDocument.rawValue)
            
            self.revealViewController().revealToggle(nil)
            self.revealViewController().dismiss(animated: false, completion: nil)
        }else{
            print("Estamos trabajando en los demas modulos")            
            alertasNativas(title: "", message: "Disponible próximamente", acceptButton: "Aceptar")
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let type = menus[indexPath.row].type
        return (type == .notification || type == .digital || type == .updatedata) ? 80 : 45
    }
    
    private func cellProfileWithTitle(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: DMProfileTableViewCell.cellWithTitleId, for: indexPath) as! DMProfileTableViewCell
        cell.loadCell(type: menus[indexPath.row].type)
        return cell
    }
    
    private func cellProfile(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: DMProfileTableViewCell.cellId, for: indexPath) as! DMProfileTableViewCell
        cell.loadCell(type: menus[indexPath.row].type)
        return cell
    }
}

extension DMMenuViewController : ManualDelegate{
    func goToMyRequest() {
        tableView.selectRow(at: IndexPath(row: 2, section: 0), animated: false, scrollPosition: .none)
        self.revealViewController().frontViewController = menus[2].viewController
        self.revealViewController().perform(#selector(SWRevealViewController.revealToggle(_:)), with: nil, afterDelay: 0.2)
    }
}


