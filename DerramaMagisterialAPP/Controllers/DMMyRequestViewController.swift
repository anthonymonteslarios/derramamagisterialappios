//
//  DMMyRequestViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 26/11/20.
//

import UIKit


enum SolicitudEstado: String {
    case ENVIADO = "ENVIADO"
    case EN_PROCESO = "EN PROCESO"
    case EVALUADO = "EVALUADO"
    case RECHAZADO_EVALUACION = "RECHAZADO POR EVALUACIÓN"
    case RECHAZADO_DOCENTE = "RECHAZADO POR DOCENTE"
    case CREDITO_OTORGADO = "CRÉDITO OTORGADO"
    case CREDITO_DESEMBOLSADO = "CRÉDITO DESEMBOLSADO"
    case CREDITO_RECHAZADO = "CRÉDITO RECHAZADO"
    
    func getState() -> UIColor? {
        switch self {
        case .ENVIADO, .EN_PROCESO, .EVALUADO:
            return UIColor.colorFromHexString("#0099CC", withAlpha: 1.0)
        case .RECHAZADO_EVALUACION, .RECHAZADO_DOCENTE, .CREDITO_RECHAZADO:
            return UIColor.colorFromHexString("#CC0000", withAlpha: 1.0)
        case .CREDITO_OTORGADO,.CREDITO_DESEMBOLSADO:
            return UIColor.colorFromHexString("#007E33", withAlpha: 1.0)
        default:
            
            return UIColor.clear
        }
    }
}

class DMMyRequestViewController: DMBaseViewController {
    @IBOutlet weak var requestTableView: UITableView!
    
    var ListRequest : ResponseListRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getListRequest()
    }
    
    func getListRequest(){
        loadingAnimation()
        Manager.shared.requestGetListRequest(DNI: UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue) ?? "") { (response) in
            self.stopLoadingAnimating()
            self.ListRequest = response
            self.requestTableView.reloadData()
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension DMMyRequestViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ListRequest?.count == 0 || ListRequest == nil{
            tableView.setEmptyView(title: "", message: "Aún no hay solicitudes")
        }else{
            tableView.setEmptyView(title: "", message: "")
        }
        return ListRequest?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRequest", for: indexPath) as! DMResultRequestTableViewCell
        
        //cell.dateLabel.text = ListRequest?[indexPath.row].fecsol
        let dateString = ListRequest?[indexPath.row].fecsol
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        if let date = formatter.date(from: dateString!){
            formatter.dateFormat = "yyyy"
            let year = formatter.string(from: date)
            formatter.dateFormat = "MM"
            let month = formatter.string(from: date)
            formatter.dateFormat = "dd"
            let day = formatter.string(from: date)
            print(year, month, day) // 2018 12 24
        
            let monthName = formatter.monthSymbols[Int(month)! - 1]
            let prefixMonth = monthName.prefix(3).capitalized.uppercased()
            
            
            
            cell.dateLabel.text = "\(day)\n\(prefixMonth)\n\(year)"
        }

        
        let monto = ListRequest?[indexPath.row].monoto
        let plazo = ListRequest?[indexPath.row].nrocuo
        let tea = ListRequest?[indexPath.row].tea
        let montoCuota = ListRequest?[indexPath.row].moncuo
        
        cell.dataRequestLabel.text = "Solicitado S/ " + Utils.formatIntegerAmount(amount: monto!) + " a " + String(format: "%.0f", plazo!) + " meses"
        
        cell.TEALabel.text = "TEA " + String(format: "%.2f", tea!) + "% "
        
        cell.CuotaMensualLabel.text = "Cuota mensual S/ " + Utils.formatIntegerAmount(amount: montoCuota!)
        
        cell.stateView.backgroundColor = SolicitudEstado.getState(SolicitudEstado(rawValue: (ListRequest?[indexPath.row].desestsol)!)!)()
        cell.stateLabel.textColor = SolicitudEstado.getState(SolicitudEstado(rawValue: (ListRequest?[indexPath.row].desestsol)!)!)()
        
        
        
        cell.stateLabel.text = ListRequest?[indexPath.row].desestsol
        
        return cell
        
    }
    
    
}


extension UITableView {

func setEmptyView(title: String, message: String) {
    let emptyView = UIView(frame: CGRect(x: self.center.x, y: self.center.y, width: self.bounds.size.width, height: self.bounds.size.height))
    let titleLabel = UILabel()
    let messageLabel = UILabel()
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    messageLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.textColor = UIColor.black
    titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
    messageLabel.textColor = UIColor.lightGray
    messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
    emptyView.addSubview(titleLabel)
    emptyView.addSubview(messageLabel)
    titleLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor).isActive = true
    titleLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor).isActive = true
    messageLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true
    messageLabel.leftAnchor.constraint(equalTo: emptyView.leftAnchor, constant: 20).isActive = true
    messageLabel.rightAnchor.constraint(equalTo: emptyView.rightAnchor, constant: -20).isActive = true
    titleLabel.text = title
    messageLabel.text = message
    messageLabel.numberOfLines = 0
    messageLabel.textAlignment = .center
    // The only tricky part is here:
    self.backgroundView = emptyView
    self.separatorStyle = .none
    }
    func restore() {
    self.backgroundView = nil
    self.separatorStyle = .singleLine
    }
}
