//
//  DMBaseViewController.swift
//  DemoNavigation
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import UIKit
import NVActivityIndicatorView

protocol DMBaseDelegate {
    func selectMenu()
}


class DMBaseViewController: SWFrontGenericoViewController, NVActivityIndicatorViewable {

    var loading : loadingView?
    var isPhoto : Bool = false
    var delegateMenu : DMBaseDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        configureStyles()
    }
    // MARK: - Own Methods
    private func initView() {
        revealViewController().tapGestureRecognizer()
        revealViewController().panGestureRecognizer()

    }
    
    private func configureStyles(){
        self.view.backgroundColor = Colors.screenColor()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loadingAnimation(){
                
        if loading != nil{
            loading?.removeFromSuperview()
        }else{
            loading = loadingView.instanceFromNib()
            loading?.frame = (navigationController?.view.frame)!
            loading!.loadingView.startAnimating()
            navigationController?.view.addSubview(loading!)
            //view.addSubview(loading!)
        }
        
       
    }
    
    func stopLoadingAnimating(){
        
        loading?.loadingView.stopAnimating()
        loading?.removeFromSuperview()
        loading = nil
    }
    
    
    func stopLoadingAnimatingNavi(){
        
        loading?.removeFromSuperview()
        loading = nil
        
        
    }
    
}
