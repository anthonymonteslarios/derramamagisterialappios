//
//  DMResultViewController.swift
//  DerramaMagisterialAPP
//
//  Created by everis on 23/11/20.
//

import UIKit

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

class DMResultViewController: DMBaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var amountAproveLabel: UILabel!
    @IBOutlet weak var calculateContainerView: UIView!
    
    @IBOutlet weak var questionAmountLabel: UILabel!
    @IBOutlet weak var amountRangeLabel: UILabel!
    @IBOutlet weak var questionPlazoLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var calculateButton: UIButton!
    
    @IBOutlet weak var undeLineView: UIView!
    @IBOutlet weak var undeLineMonthView: UIView!
    
    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var monthTextfield: UITextField!
    
    @IBOutlet weak var requestContainerView: UIView!
    @IBOutlet weak var bottomRequestConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var refuseButton: UIButton!
    @IBOutlet weak var requestButton: UIButton!
    
    @IBOutlet weak var cuotaMensualContainerView: UIView!
    @IBOutlet weak var bottomCuotaMensualConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleCuotaMensualLabel: UILabel!
    @IBOutlet weak var cuotaMensualLabel: UILabel!
    
    @IBOutlet weak var titleOtorgadoLabel: UILabel!
    @IBOutlet weak var montoOtorgadoLabel: UILabel!
    
    @IBOutlet weak var titlePlazoLabel: UILabel!
    @IBOutlet weak var plazoLabel: UILabel!
    
    @IBOutlet weak var titleSaldoCreditoAntLabel: UILabel!
    @IBOutlet weak var saldoCreditoAntLabel: UILabel!
    
    @IBOutlet weak var titleCuotaLabel: UILabel!
    @IBOutlet weak var cuotaLabel: UILabel!
    
    @IBOutlet weak var titleTeaLabel: UILabel!
    @IBOutlet weak var teaLabel: UILabel!
    
    @IBOutlet weak var titleDesembolsarLabel: UILabel!
    @IBOutlet weak var desembolsarLabel: UILabel!
    
    @IBOutlet weak var titleTceaLabel: UILabel!
    @IBOutlet weak var tceaLabel: UILabel!
    
    @IBOutlet weak var selectTitleLabel: UILabel!
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var topBNContainerView: UIView!
    
    @IBOutlet weak var tableviewHeighConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightCalculateButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var topBNContainerConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var bNTitleLabel: UILabel!
    @IBOutlet weak var bNsubTitleLabel: UILabel!
    @IBOutlet weak var accountBnLabel: UILabel!
    @IBOutlet weak var descBnLabel: UILabel!
    @IBOutlet weak var ckeckBnButton: UIButton!
    
    @IBOutlet weak var resultCalculateView: UIView!
    
    @IBOutlet weak var heighResultConstraint: NSLayoutConstraint!
    
    private var DNI : String = UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue)!
    
    var selectedButtonState = false
    
    private var amount = ""
    private var cuotas = ""
    private var textRangeAmount = ""
    private var textRangePlazo = ""
    private var validateAmount = false
    private var validateCuota = false
    private var showMensualContainer = false
    private var timeIndexSelected : Int?
    private var typeUnlock : TabsUnlocktype = .evaluado
    private var completeForm = false
    private var indexRequestTypeCredit: Int?
    private var typeConsumo = Constant.CreditoConsumo
    
    var dmDialogIdentity: DMDialogRefuseResultView!
    
    var arrayRefuses : ResponseRefuses = []
    var arrayDesembolsos : ResponseDesembolso = []
    
    weak var delegate: DMReqCreditEvaluationDelegate?
    
    var openDialogIdentity = false
    var goToHome = false
    
    private lazy var refreshControl : UIRefreshControl = {
        
        let _refreshControl = UIRefreshControl()
        
        _refreshControl.tintColor       = .white
        _refreshControl.backgroundColor = .clear
        _refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: .valueChanged)
        
        return _refreshControl
    }()
    
    @objc private func pullToRefresh() {
        self.fetchRefreshDataGET(Paralelo: Constant.CreditoConsumo, withStartAnimation: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        viewWillAppear(false)
        getDesembolsos()
        addStyleToElements()
        configureTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        indexRequestTypeCredit = UserDefaults.standard.integer(forKey: UserDefault.requestTypeCredit.rawValue)
        
        selectTitleLabel.text = "Selecciona la forma de desembolso"
        bNTitleLabel.text = "MUY IMPORTANTE"
        bNsubTitleLabel.text = "CUENTA DE AHORROS BANCO DE LA NACIÓN"
        descBnLabel.text = "Certifico que es mi número de cuenta actual."
        titleOtorgadoLabel.text = "Monto Otorgado"
        titlePlazoLabel.text = "Plazo"
        titleSaldoCreditoAntLabel.text = "Saldo del crédito anterior"
        titleCuotaLabel.text = "Cuota"
        titleTeaLabel.text = "TEA"
        titleDesembolsarLabel.text = "Monto a desembolsar"
        titleTceaLabel.text = "TCEA"
        titleCuotaMensualLabel.text = "Tu cuota mensual sería:"
        
        completeForm = UserDefaults.standard.bool(forKey: UserDefault.evaluationComplete.rawValue)
        
        var state = ""
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
            }else{
                state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
            }
            
        }else{
            state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
        }
        
        let tipoSolicitud = Int(state) ?? 0
        typeUnlock = TabsUnlocktype.init(rawValue: tipoSolicitud) ?? .none
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                typeConsumo = Constant.CreditoTeApoyo
                titleLabel.text = "Crédito #teApoyo"
                descLabel.text = "Tu solicitud fue evaluada, tienes un monto aprobado de"
                questionAmountLabel.text = "Tienes un monto de"
                questionPlazoLabel.text = "a un plazo de"
                textRangeAmount = ""
                textRangePlazo = ""
                
                amountRangeLabel.text = textRangeAmount
                monthLabel.text = textRangePlazo
            }else{
                typeConsumo = Constant.CreditoConsumo
                var plazoMin = ""
                if typeUnlock != .evaluado{
                    plazoMin = DMProduct.shared.credit?.autCuomin ?? ""
                }else{
                    plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.autCuomin ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
                }
                
                var monmin = DMProduct.shared.credit?.autMonmin ?? ""
                
                if typeUnlock == .evaluado, DMProduct.shared.credit?.autNotabn ?? "" > DMProduct.shared.credit?.autMonmin ?? ""{
                    let nota = Double(DMProduct.shared.credit?.autNotabn ?? "") ?? 0.0
                    monmin = "\((nota + 1).rounded(.down))"
                }
                
                if Double(DMProduct.shared.credit?.autNotabn ?? "0") ?? 0  > 0  && Double(DMProduct.shared.credit?.autNotabn ?? "0") ?? 0  > Double(DMProduct.shared.credit?.autMonmin ?? "0") ?? 0 {
                    let nota = Double(DMProduct.shared.credit?.autNotabn ?? "") ?? 0.0
                    monmin = "\((nota + 1).rounded(.down))"
                }
                
                //&& (Double(DMProduct.shared.credit?.autNotabn) > Double(DMProduct.shared.credit?.autMonmin))
                
                titleLabel.text = "Crédito de Consumo"
                descLabel.text = "Tu solicitud fue evaluada, tienes un monto aprobado máximo de"
                questionAmountLabel.text = "¿Que monto quieres?"
                questionPlazoLabel.text = "¿En que plazo?"

                textRangeAmount = "Puedes elegir un monto de \(formatAmount(monmin)) y \(formatAmount(DMProduct.shared.credit?.autMonmax ?? ""))"
                textRangePlazo = "Puedes elegir un plazo entre \(plazoMin) y \(DMProduct.shared.credit?.autCuomax ?? "") meses"
                
                amountRangeLabel.text = textRangeAmount
                monthLabel.text = textRangePlazo
                
                if typeUnlock == .evaluado, amount == "", cuotas == ""{
                    amount = DMProduct.shared.credit?.autMonmax ?? ""
                    amountTextfield.text = formatNoSymbolAmount(amount)
                    cuotas = DMProduct.shared.credit?.autCuomax ?? ""
                    monthTextfield.text = cuotas
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        self.calculateButtonAction(nil)
                    }
                }
            }
            
        }else{
            titleLabel.text = "Crédito #teApoyo"
            descLabel.text = "Tu solicitud fue evaluada, tienes un monto aprobado de"
            questionAmountLabel.text = "Tienes un monto de"
            questionPlazoLabel.text = "a un plazo de"
            textRangeAmount = ""
            textRangePlazo = ""
            
            amountRangeLabel.text = textRangeAmount
            monthLabel.text = textRangePlazo
        }

        
        if let refresh = self.scrollView.subviews.first(where: {$0 is UIRefreshControl}){
            refresh.removeFromSuperview()
        }
        if typeUnlock != .proceso && typeUnlock != .evaluado{
            self.scrollView.addSubview(self.refreshControl)
        }
        
        accountBnLabel.text = DMProduct.shared.credit?.asoncta
        amountAproveLabel.text = formatAmount(DMProduct.shared.credit?.autMonmax ?? "")
    
        DispatchQueue.main.async {
            self.textHtml()
        }
        
        if typeUnlock != .evaluado{
            loadDataAceptado()
        }else{
            loadDataInitial()
        }
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                
                amount = DMProduct.shared.credit?.montoOtorgar ?? ""
                cuotas = DMProduct.shared.credit?.plazoOtorgar ?? ""
                cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                montoOtorgadoLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
                plazoLabel.text =  "\(DMProduct.shared.credit?.plazoOtorgar ?? "") Meses"
                cuotaLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                desembolsarLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
                amountTextfield.isUserInteractionEnabled = false
                monthTextfield.isUserInteractionEnabled = false
                
                
                if typeUnlock != .evaluado{
                    bottomRequestConstraint.constant = 30
                    requestContainerView.isHidden = true
                }else{
                    bottomRequestConstraint.constant = 90
                    requestContainerView.isHidden = false
                }

                calculateButton.alpha = 0
                heightCalculateButtonConstraint.constant = 0
                heighResultConstraint.constant = 277
                self.resultCalculateView.alpha = 1
                self.titleCuotaMensualLabel.alpha = 1
                self.cuotaMensualLabel.alpha = 1
                
                self.view.layoutIfNeeded()
                self.viewWillLayoutSubviews()
                self.viewDidLayoutSubviews()
                
            }
        }else{
            amount = DMProduct.shared.credit?.montoOtorgar ?? ""
            cuotas = DMProduct.shared.credit?.plazoOtorgar ?? ""
            cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
            montoOtorgadoLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
            plazoLabel.text =  "\(DMProduct.shared.credit?.plazoOtorgar ?? "") Meses"
            cuotaLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
            desembolsarLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
            amountTextfield.isUserInteractionEnabled = false
            monthTextfield.isUserInteractionEnabled = false
            if typeUnlock != .evaluado{
                bottomRequestConstraint.constant = 30
                requestContainerView.isHidden = true
            }else{
                bottomRequestConstraint.constant = 90
                requestContainerView.isHidden = false
            }
            calculateButton.alpha = 0
            heightCalculateButtonConstraint.constant = 0
            heighResultConstraint.constant = 277
            resultCalculateView.alpha = 1
            titleCuotaMensualLabel.alpha = 1
            cuotaMensualLabel.alpha = 1
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.viewDidLayoutSubviews()
        }
        
        amountTextfield.text = formatNoSymbolAmount(amount)
        monthTextfield.text = cuotas
        
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
    }
    
    override func viewWillLayoutSubviews() {
        self.bottomCuotaMensualConstraint.constant = self.cuotaMensualContainerView.frame.height + 20
    }
    override func viewDidLayoutSubviews() {
        self.bottomCuotaMensualConstraint.constant = self.cuotaMensualContainerView.frame.height + 20
    }
    
    private func loadDataInitial(){

        if completeForm{
            amount = DMProduct.shared.credit?.autMonoto ?? ""
            cuotas = DMProduct.shared.credit?.autNrocuo ?? ""
            
        }else{
            //titleCuotaMensualLabel.alpha = 0
            //cuotaMensualLabel.alpha = 0
            //resultCalculateView.alpha = 0
            //heighResultConstraint.constant = 0
            calculateButton.alpha = 1
            heightCalculateButtonConstraint.constant = 35
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.viewDidLayoutSubviews()
        }
        
        cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.autMoncuo ?? "")
        montoOtorgadoLabel.text = formatAmount(DMProduct.shared.credit?.autMonoto ?? "")
        plazoLabel.text =  "\(DMProduct.shared.credit?.autNrocuo ?? "") Meses"
        saldoCreditoAntLabel.text = formatAmount(DMProduct.shared.credit?.autNotabn ?? "")
        cuotaLabel.text = formatAmount(DMProduct.shared.credit?.autMoncuo ?? "")
        teaLabel.text = "\(DMProduct.shared.credit?.autTea ?? "") %"
        tceaLabel.text = "\(DMProduct.shared.credit?.autTcea ?? "") %"
        desembolsarLabel.text = formatAmount(DMProduct.shared.credit?.autMontodesembolsar ?? "")
        
        amountTextfield.isUserInteractionEnabled = true
        monthTextfield.isUserInteractionEnabled = true
    
    }
    
    private func loadDataAceptado(){
        
        amount = DMProduct.shared.credit?.aceptadoMonoto ?? ""
        cuotas = DMProduct.shared.credit?.aceptadoNrocuo ?? ""
        
        cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.aceptadoMoncuo ?? "")
        montoOtorgadoLabel.text = formatAmount(DMProduct.shared.credit?.aceptadoMonoto ?? "")
        plazoLabel.text = "\(DMProduct.shared.credit?.aceptadoNrocuo ?? "" ) Meses"
        saldoCreditoAntLabel.text = formatAmount(DMProduct.shared.credit?.aceptadoNotabn ?? "")
        cuotaLabel.text = formatAmount(DMProduct.shared.credit?.aceptadoMoncuo ?? "")
        teaLabel.text = "\(DMProduct.shared.credit?.aceptadoTea ?? "") %"
        tceaLabel.text = "\(DMProduct.shared.credit?.aceptadoTcea ?? "") %"
        desembolsarLabel.text = formatAmount(DMProduct.shared.credit?.aceptadoMontodesembolsar ?? "")
        
        amountTextfield.isUserInteractionEnabled = false
        monthTextfield.isUserInteractionEnabled = false
        
        bottomRequestConstraint.constant = 30
        requestContainerView.isHidden = true
        calculateButton.alpha = 0
        heightCalculateButtonConstraint.constant = 0
        heighResultConstraint.constant = 277
        resultCalculateView.alpha = 1
        titleCuotaMensualLabel.alpha = 1
        cuotaMensualLabel.alpha = 1
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
    }
    
    //MARK: - Private methods
    private func initView() {
        
        tableviewHeighConstraint.constant = 0
        topBNContainerView.alpha = 0 // falta logica para seleccionar la celda
        topBNContainerConstraint.constant = -topBNContainerView.frame.height // falta logica para seleccionar la celda
        cuotaMensualContainerView.isHidden = false
        bottomRequestConstraint.constant = 30
        requestContainerView.isHidden = true
        resultCalculateView.alpha = 0
        heighResultConstraint.constant = 0
        titleCuotaMensualLabel.alpha = 0
        cuotaMensualLabel.alpha = 0
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
    }

    private func addStyleToElements(){
        
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .center
        descLabel.textColor = Colors.tabDeselectColor()
        descLabel.personalizeLabelStyle(fontSize: 15, isBold: false)
        
        questionAmountLabel.numberOfLines = 0
        questionAmountLabel.textAlignment = .center
        questionAmountLabel.textColor = Colors.productCellTextColor()
        questionAmountLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        amountRangeLabel.numberOfLines = 0
        amountRangeLabel.textAlignment = .center
        amountRangeLabel.textColor = Colors.textMenuColor()
        amountRangeLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        questionPlazoLabel.numberOfLines = 0
        questionPlazoLabel.textAlignment = .center
        questionPlazoLabel.textColor = Colors.productCellTextColor()
        questionPlazoLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        monthLabel.numberOfLines = 0
        monthLabel.textAlignment = .center
        monthLabel.textColor = Colors.textMenuColor()
        monthLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        selectTitleLabel.numberOfLines = 0
        selectTitleLabel.textAlignment = .center
        selectTitleLabel.textColor = Colors.primaryColor()
        selectTitleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        bNTitleLabel.numberOfLines = 0
        bNTitleLabel.textAlignment = .left
        bNTitleLabel.textColor = Colors.textBnColor()
        bNTitleLabel.personalizeLabelStyle(fontSize: 12, isBold: true)
        
        bNsubTitleLabel.numberOfLines = 0
        bNsubTitleLabel.textAlignment = .left
        bNsubTitleLabel.textColor = Colors.textBnColor()
        bNsubTitleLabel.personalizeLabelStyle(fontSize: 12, isBold: true)
        
        accountBnLabel.numberOfLines = 0
        accountBnLabel.textAlignment = .center
        accountBnLabel.textColor = Colors.textAmountBnColor()
        accountBnLabel.personalizeLabelStyle(fontSize: 14, isBold: true)
        
        descBnLabel.numberOfLines = 0
        descBnLabel.textAlignment = .left
        descBnLabel.textColor = Colors.textAmountBnColor()
        descBnLabel.personalizeLabelStyle(fontSize: 12, isBold: true)
        
        amountAproveLabel.personalizeAmountRoundStyle()
        calculateButton.personalizeButtonRoundBasicStyle(with: "CALCULA TU CUOTA")
        refuseButton.personalizeGrayButtonRoundBasicStyle(with: "NO ACEPTO")
        requestButton.personalizeButtonRoundBasicStyle(with: "ACEPTAR")
        
        calculateContainerView.backgroundColor = Colors.cardColor()
        calculateContainerView.layer.cornerRadius = 10
        
        undeLineView.backgroundColor = Colors.textMenuColor()
        undeLineMonthView.backgroundColor = Colors.textMenuColor()
        
        amountTextfield.font = UIFont(name: Constant.dmFontBold, size: 16)
        monthTextfield.font = UIFont(name: Constant.dmFontBold, size: 16)
        amountTextfield.textAlignment = .center
        monthTextfield.textAlignment = .center
        amountTextfield.delegate = self
        monthTextfield.delegate = self
        amountTextfield.textColor = Colors.productCellTextColor()
        monthTextfield.textColor = Colors.productCellTextColor()
        
        topBNContainerView.backgroundColor = Colors.bnContainerColor()
        topBNContainerView.layer.cornerRadius = 10
        requestContainerView.backgroundColor = Colors.screenColor()
        scrollView.backgroundColor = Colors.primaryColor()
        bodyView.backgroundColor = Colors.screenColor()
        headerView.backgroundColor = .clear
        
        titleCuotaMensualLabel.numberOfLines = 0
        titleCuotaMensualLabel.textAlignment = .center
        titleCuotaMensualLabel.textColor = Colors.productCellTextColor()
        titleCuotaMensualLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        cuotaMensualLabel.numberOfLines = 0
        cuotaMensualLabel.textAlignment = .center
        cuotaMensualLabel.textColor = Colors.productCellTextColor()
        cuotaMensualLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        titleOtorgadoLabel.personlizeAmountTitleResult()
        titlePlazoLabel.personlizeAmountTitleResult()
        titleSaldoCreditoAntLabel.personlizeAmountTitleResult()
        titleCuotaLabel.personlizeAmountTitleResult()
        titleTeaLabel.personlizeAmountTitleResult()
        titleDesembolsarLabel.personlizeAmountTitleResult()
        titleTceaLabel.personlizeAmountTitleResult()
        
        montoOtorgadoLabel.personlizeAmountDescResult()
        plazoLabel.personlizeAmountDescResult()
        saldoCreditoAntLabel.personlizeAmountDescResult()
        cuotaLabel.personlizeAmountDescResult()
        teaLabel.personlizeAmountDescResult()
        desembolsarLabel.personlizeAmountDescResult()
        tceaLabel.personlizeAmountDescResult()
        
        view.backgroundColor = Colors.screenColor()
    }
    
    private func configureTableView() {
        self.tableview.dataSource = self
        self.tableview.delegate = self
        self.tableview.register(DMResultTableViewCell.getNib(), forCellReuseIdentifier: DMResultTableViewCell.cellId)
        self.tableview.separatorStyle = .none
        self.tableview.rowHeight = 45
    }
    
    private func formatNoSymbolAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Utils.formatIntegerAmount(amount: amount, withFraction: false)
    }

    
    private func formatAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Constant.coinSymbolPEN+" "+Utils.formatIntegerAmount(amount: amount)
    }
    
    private func textHtml(){
        let firmaBold = "FIRMA ELECTRÓNICA:"
        let sistemaBold = "El sistema intentará realizar está acción como primera opción y tendrás \(DMProduct.shared.credit?.nrointentosmaximos ?? "") intentos."
        let virutalBold = "VIRTUAL: "
        let oficinaBold = "EN OFICINA: "
        
        let desc1 = "<p>Completa la operación eligiendo el monto y plazo que más te convenga, y recuerda que para terminar la operación tendrás 3 opciones para la generación de documentos:</p>"
        let desc2 = "<p><strong>\(firmaBold)</strong> El sistema, a través del servicio del RENIEC, te identificará e insertará tu firma electrónica en todos los documentos del crédito (es requisito indispensable contar con una cámara). <strong>\(sistemaBold)</strong> Si no es posible la identificación biométrica facial, deberás elegir entre las siguientes opciones:</p>"
        let desc3 = "<ul><li>&nbsp;<strong>\(virutalBold)</strong> El sistema te permitirá descargar los documentos de crédito, deberás firmarlos y subirlos al sistema.</li>"
        let desc4 = " <li>&nbsp;<strong>\(oficinaBold)</strong> Esta opción te permite terminar la operación crediticia firmando de manera presencial los documentos del crédito en la oficina mas cercana.</li></ul>"
        let desc5 = "</br></br><li>&nbsp;El tiempo máximo para la confirmación de esta operación es de 48 horas, después de este tiempo la operación quedará rechazada de manera automática.</li></ul>"
        
        let descFinal = "\(desc1)\(desc2)\(desc3)\(desc4)\(desc5)"
        textView.attributedText = descFinal.htmlToAttributedString
        textView.textAlignment = .justified
    }
    
    private func fetchDataGET(Paralelo: String){
        self.view.isUserInteractionEnabled = false
        self.loadingAnimation()
        
        let nCuota = self.cuotas == Constant.textEmpty ? DMProduct.shared.credit?.autCuomax ?? "" : self.cuotas
        
        Manager.shared.requestGetProducts(ASODNI: DNI, RECALCULAR: Constant.Recalcular, MONTO_OTORGAR: DMProduct.shared.credit?.autMonmax ?? "0", NROCUOTAS:  DMProduct.shared.credit?.plazoMaximoOtorgar ?? "", TEM: DMProduct.shared.credit?.autPorint ?? "", MONTO_OTORGAR_X: amount, NROCUOTAS_X: nCuota, PARALELO: Paralelo)  {(dataGet, error) in
                        
            self.view.isUserInteractionEnabled = true
            
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.loadingAnimation()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }

            DMProduct.shared.credit = dataGet
            self.viewWillAppear(false)
            self.stopLoadingAnimating()
        }
    }
    
    private func fetchRefreshDataGET(Paralelo: String, withStartAnimation : Bool = true){
        if withStartAnimation{
                self.loadingAnimation()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.view.isUserInteractionEnabled = false
            Manager.shared.requestGetProducts(ASODNI: self.DNI, RECALCULAR: Constant.Recalcular, MONTO_OTORGAR: "0", NROCUOTAS: "0", TEM: "", MONTO_OTORGAR_X: "", NROCUOTAS_X: "", PARALELO: Paralelo)  {(dataGet, error) in
                            
                self.view.isUserInteractionEnabled = true
                
                if (error.first != nil) {
                    self.loadingAnimation()
                    self.refreshControl.endRefreshing()
                    print("error: \(String(describing: error.first?.mensaje))")
                    self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                    return
                }
                
                if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
                    if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                        DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                        DMProduct.shared.credit = dataGet
                    }else{
                        DMProduct.shared.credit = dataGet
                    }
                    
                }else{
                    DMProduct.shared.credit = dataGet
                }
                
                DispatchQueue.main.async {
                    self.initView()
                    self.viewWillAppear(false)
                    self.getDesembolsos()
                    self.delegate?.refreshPage(controller: self)
                    self.loadingAnimation()
                    self.refreshControl.endRefreshing()
                    if self.goToHome{
                        self.goToHome = false
                        self.typeUnlock = .none
                        UserDefaults.standard.set(false, forKey: UserDefault.requestComplete.rawValue)
                        NotificationCenter.default.post(name: Notification.Name("ShowToProducts"), object: nil)
                    }
                }
            }
        }
    }
    
    private func validateValues(isAmount : Bool){
        
        var monmin = DMProduct.shared.credit?.autMonmin ?? ""
        
        if typeUnlock == .evaluado, DMProduct.shared.credit?.autNotabn ?? "" > DMProduct.shared.credit?.autMonmin ?? ""{
            let nota = Double(DMProduct.shared.credit?.autNotabn ?? "") ?? 0.0
            monmin = "\((nota + 1).rounded(.down))"
        }
        
        if Double(DMProduct.shared.credit?.autNotabn ?? "0") ?? 0  > 0  && Double(DMProduct.shared.credit?.autNotabn ?? "0") ?? 0  > Double(DMProduct.shared.credit?.autMonmin ?? "0") ?? 0 {
            let nota = Double(DMProduct.shared.credit?.autNotabn ?? "") ?? 0.0
            monmin = "\((nota + 1).rounded(.down))"
        }
        
        let min = Double(monmin) ?? 0.0
        let max = Double(DMProduct.shared.credit?.autMonmax ?? "0") ?? 0.0
        
        var plazoMin = ""
        if typeUnlock != .evaluado{
            plazoMin = DMProduct.shared.credit?.autCuomin ?? ""
        }else{
            plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.autCuomin ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
        }
        let minPlazo = Double(plazoMin) ?? 0.0
        let maxPlazo = Double(DMProduct.shared.credit?.autCuomax ?? "0") ?? 0.0
        
        let amount = Double(self.amount) ?? 0.0
        let cuotas = Double(self.cuotas) ?? 0.0
        
        validateAmount = amount >= min && amount <= max
        validateCuota = cuotas >= minPlazo && cuotas <= maxPlazo
        
        if isAmount{
            if !validateAmount{
                self.alertasNativas(title: Constant.textEmpty, message: textRangeAmount, acceptButton: "Aceptar")
                return
            }
        }else{
            if !validateCuota{
                self.alertasNativas(title: Constant.textEmpty, message: textRangePlazo, acceptButton: "Aceptar")
                return
            }
        }
        if isAmount{
            fetchDataGET(Paralelo: Constant.CreditoConsumo)
        }
        
    }
    
    private func validateBothValues() -> Bool{
        var monmin = DMProduct.shared.credit?.autMonmin ?? ""
        
        if typeUnlock == .evaluado, DMProduct.shared.credit?.autNotabn ?? "" > DMProduct.shared.credit?.autMonmin ?? ""{
            let nota = Double(DMProduct.shared.credit?.autNotabn ?? "") ?? 0.0
            monmin = "\((nota + 1).rounded(.down))"
        }
        let min = Double(monmin) ?? 0.0
        let max = Double(DMProduct.shared.credit?.autMonmax ?? "0") ?? 0.0
        
        var plazoMin = ""
        if typeUnlock != .evaluado{
            plazoMin = DMProduct.shared.credit?.autCuomin ?? ""
        }else{
            plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.autCuomin ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
        }
        let minPlazo = Double(plazoMin) ?? 0.0
        let maxPlazo = Double(DMProduct.shared.credit?.autCuomax ?? "0") ?? 0.0
        
        let amount = Double(self.amount) ?? 0.0
        let cuotas = Double(self.cuotas) ?? 0.0
        
        validateAmount = amount >= min && amount <= max
        validateCuota = cuotas >= minPlazo && cuotas <= maxPlazo
        
        if !validateAmount{
            self.alertasNativas(title: Constant.textEmpty, message: textRangeAmount, acceptButton: "Aceptar")
            return false
        }
        if !validateCuota{
            self.alertasNativas(title: Constant.textEmpty, message: textRangePlazo, acceptButton: "Aceptar")
            return false
        }
        return true
    }
    
    private func getDesembolsos(){
        self.view.isUserInteractionEnabled = false
        self.loadingAnimation()
        Manager.shared.requestListDesembolso { (dataGet, error) in
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                
            }
            
            self.view.isUserInteractionEnabled = true
            self.loadingAnimation()
            
            guard let dataGet = dataGet else {
                return
            }
            
            self.arrayDesembolsos = dataGet
            
            if self.typeUnlock != .evaluado{
                let selectedSecondRow = DMProduct.shared.credit?.tipdeseid == "20"
                
                self.timeIndexSelected = selectedSecondRow ? 1 : 0
                self.ckeckBnButton.isSelected = selectedSecondRow && DMProduct.shared.credit?.sitcta == Constant.desembolso
                
                self.topBNContainerView.alpha = self.timeIndexSelected == 1 ? 1 : 0
                self.topBNContainerConstraint.constant = self.timeIndexSelected == 1 ? 0 : -self.topBNContainerView.frame.height
                self.tableview.allowsSelection = false
                self.ckeckBnButton.isUserInteractionEnabled = false
            }else{
                self.timeIndexSelected = nil
                self.topBNContainerView.alpha = 0
                self.topBNContainerConstraint.constant = -self.topBNContainerView.frame.height
                self.tableview.allowsSelection = true
                self.ckeckBnButton.isUserInteractionEnabled = true
            }
            self.tableview.reloadData()
            self.tableviewHeighConstraint.constant = (45.0 * CGFloat(self.arrayDesembolsos.count))
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.viewDidLayoutSubviews()
        }
    }
    
    private func getRefuses(){
        self.view.isUserInteractionEnabled = false
        self.loadingAnimation()
        Manager.shared.requestRefuses { (dataGet, error) in
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                
            }
            self.view.isUserInteractionEnabled = true
            self.loadingAnimation()
            guard let dataGet = dataGet else {
                return
            }
            
            self.arrayRefuses = dataGet
            self.openDialogIdentity = true
            self.loadDialogPicker()
        }
    }
    
    private func loadDialogPicker(){
        dmDialogIdentity = DMDialogRefuseResultView.instanceViewFromXIB()
                    
        var frame = dmDialogIdentity.frame
        frame.origin = CGPoint.zero
        frame.size = UIScreen.main.bounds.size
        dmDialogIdentity.frame = frame
        dmDialogIdentity.delegate = self
        
        dmDialogIdentity.nombrePadreMadreTextfield.delegate = self
        
        // service
        navigationController?.view.addSubview(dmDialogIdentity)
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        dmDialogIdentity.nombrePadreMadreTextfield.inputView = pickerView
        
        let toolBarPicker = UIToolbar()
        toolBarPicker.sizeToFit()
        let button = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(self.action))
        toolBarPicker.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), button], animated: true)
        toolBarPicker.isUserInteractionEnabled = true
        dmDialogIdentity.nombrePadreMadreTextfield.inputAccessoryView = toolBarPicker
    }
    
    private func refuseSol(ACEPTAR_CODSOL: String, MOTIVORECHAZO: String){
        self.loadingAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.view.isUserInteractionEnabled = false
            Manager.shared.requestRefuseSol(ACEPTAR_CODSOL: ACEPTAR_CODSOL, MOTIVORECHAZO: MOTIVORECHAZO) { (dataGet, error) in
                self.view.isUserInteractionEnabled = true
                self.stopLoadingAnimating()
                guard let dataGet = dataGet else {
                    return
                }
                if dataGet.success ?? false{
                    self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar", handlerAccept:  {
                        DispatchQueue.main.async {
                            self.goToHome = true
                            self.fetchRefreshDataGET(Paralelo: self.typeConsumo)
                        }
                        
                    })
                }else{
                    self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar")
                }
            }
        }
    }
    
    private func requestSendResult(){
        self.view.isUserInteractionEnabled = false
        self.loadingAnimation()
        
        Manager.shared.requestSendResult(ACEPTAR_CODSOL: DMProduct.shared.credit?.codsol ?? "0", ACEPTAR_MONTO: DMProduct.shared.credit?.autMonoto ?? "0", ACEPTAR_PLAZO: DMProduct.shared.credit?.autNrocuo ?? "0", ACEPTAR_INTERES: DMProduct.shared.credit?.autPorint ?? "0", ACEPTAR_CUOTA: DMProduct.shared.credit?.autMoncuo ?? "0", ACEPTAR_MONTO_INTERES_PROCESO: DMProduct.shared.credit?.autMonintpro ?? "0", ACEPTAR_NOTA_ABONO: DMProduct.shared.credit?.autNotabn ?? "0", ACEPTAR_TEA: DMProduct.shared.credit?.autTea ?? "0", ACEPTAR_TCEA: DMProduct.shared.credit?.autTcea ?? "0", ACEPTAR_DIASGRACIA: DMProduct.shared.credit?.autDiagracia ?? "0", ACEPTAR_DIASENPROCESO: DMProduct.shared.credit?.autDiaproceso ?? "0", ACEPTAR_DIASPROCESOGRACIA: DMProduct.shared.credit?.autDiaprocesogracia ?? "0", TIPODESEMBOLSO: self.arrayDesembolsos[timeIndexSelected ?? 0].tipdeseid ?? "", USUARIO: DMProduct.shared.credit?.username ?? "", CORREO: DMProduct.shared.credit?.correo ?? "", CELULAR: DMProduct.shared.credit?.celular ?? "", ASODNI: DMProduct.shared.credit?.asodni ?? "", ASOAPENOMDNI: DMProduct.shared.credit?.asoapenomdni ?? "", TIPOCANAL: Constant.canal) { (dataGet, error) in
            self.view.isUserInteractionEnabled = true
            self.loadingAnimation()
            guard let dataGet = dataGet else {
                return
            }
            if dataGet.success ?? false{
                self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar", handlerAccept:  {
                    self.fetchRefreshDataGET(Paralelo: self.typeConsumo)
                })
            }else{
                self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar")
            }
            
        }
        
    }
    
    private func requestTeApoyoSendResult(){
        self.view.isUserInteractionEnabled = false
        self.startAnimating()
        
        Manager.shared.requestSendResult(ACEPTAR_CODSOL: DMProduct.shared.credit?.codsol ?? "0", ACEPTAR_MONTO: DMProduct.shared.credit?.autMonoto ?? "0", ACEPTAR_PLAZO: DMProduct.shared.credit?.autNrocuo ?? "0", ACEPTAR_INTERES: DMProduct.shared.credit?.autPorint ?? "0", ACEPTAR_CUOTA: DMProduct.shared.credit?.autMoncuo ?? "0", ACEPTAR_MONTO_INTERES_PROCESO: DMProduct.shared.credit?.autMonintpro ?? "0", ACEPTAR_NOTA_ABONO: DMProduct.shared.credit?.autNotabn ?? "0", ACEPTAR_TEA: DMProduct.shared.credit?.autTea ?? "0", ACEPTAR_TCEA: DMProduct.shared.credit?.autTcea ?? "0", ACEPTAR_DIASGRACIA: DMProduct.shared.credit?.autDiagracia ?? "0", ACEPTAR_DIASENPROCESO: DMProduct.shared.credit?.autDiaproceso ?? "0", ACEPTAR_DIASPROCESOGRACIA: DMProduct.shared.credit?.autDiaprocesogracia ?? "0", TIPODESEMBOLSO: self.arrayDesembolsos[timeIndexSelected ?? 0].tipdeseid ?? "", USUARIO: DMProduct.shared.credit?.username ?? "", CORREO: DMProduct.shared.credit?.correo ?? "", CELULAR: DMProduct.shared.credit?.celular ?? "", ASODNI: DMProduct.shared.credit?.asodni ?? "", ASOAPENOMDNI: DMProduct.shared.credit?.asoapenomdni ?? "", TIPOCANAL: Constant.canal) { (dataGet, error) in
            self.view.isUserInteractionEnabled = true
            self.stopAnimating()
            guard let dataGet = dataGet else {
                return
            }
            if dataGet.success ?? false{
                self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar", handlerAccept:  {
                    
                    self.fetchRefreshDataGET(Paralelo: self.typeConsumo)
                })
            }else{
                self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar")
            }
            
        }
        
    }
    
    @objc func action() {
        self.openDialogIdentity = false
        dmDialogIdentity.endEditing(true)
        //dmDialogIdentity.removeFromSuperview()
    }
    
    @IBAction func calculateButtonAction(_ sender: Any?) {
        if validateBothValues() {
            self.fetchDataGET(Paralelo: Constant.CreditoConsumo)
            bottomRequestConstraint.constant = 90
            requestContainerView.isHidden = false
            heighResultConstraint.constant = 277
            self.resultCalculateView.alpha = 1
            self.titleCuotaMensualLabel.alpha = 1
            self.cuotaMensualLabel.alpha = 1
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.viewDidLayoutSubviews()
        }
    }

    @IBAction func requestHereButtonAction(_ sender: UIButton?) {
        if validateBothValues() {
            if sender?.tag == 0{
                if self.presentedViewController as? UIAlertController != nil {
                    print("Error A")
                    return
                }else{
                    openDialogIdentity = true
                    getRefuses()
                }
                
            }else if sender?.tag == 1{
                if openDialogIdentity{
                    print("Error B")
                    return
                }else{
                    self.view.isUserInteractionEnabled = false
                    if self.ckeckBnButton.isSelected && self.timeIndexSelected == 1 || self.timeIndexSelected == 0{
                        self.alertasNativas(title: Constant.textEmpty, message: "¿Está seguro de aceptar el monto y plazo?", acceptButton: "Aceptar", cancelButton: "Cancelar") {
                            if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
                                if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                                    DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                                    
                                    self.requestTeApoyoSendResult()
                                }else{
                                    self.requestSendResult()
                                }
                            }else{
                                self.requestSendResult()
                            }
                            self.view.isUserInteractionEnabled = true
                        } handlerCancel: {
                            self.view.isUserInteractionEnabled = true
                            print("cancelar")
                        }
                    }else{
                        self.view.isUserInteractionEnabled = true
                        self.alertasNativas(title: Constant.textEmpty, message: "Debes validar la cuenta bancaria", acceptButton: "Aceptar")
                    }
                }
                return
            }
        }
    }
    
    @IBAction func checkAccountBnButtonAction(_ sender: UIButton!) {
        sender.isSelected = !sender.isSelected
    }
    
    func isTimeSelected(index: Int) -> Bool {
         return (timeIndexSelected == nil) ? false : timeIndexSelected == index
    }
}

extension DMResultViewController: DMDialogRefuseResultDelegate{
    func acceptActionDelegate(motivo: String, cod: String) {
        openDialogIdentity = false
        if !motivo.isEmpty{
            self.refuseSol(ACEPTAR_CODSOL: DMProduct.shared.credit?.codsol ?? "2855", MOTIVORECHAZO: cod)
        }
    }
    
    func cancelActionDelegate(){
        openDialogIdentity = false
    }
}

extension DMResultViewController: UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == amountTextfield{
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    amount = ""
                    amountTextfield.text = ""
                }
            }
        }
        
        if openDialogIdentity{
            if textField == dmDialogIdentity.nombrePadreMadreTextfield{
                return false
            }
        }

        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == amountTextfield{
            cuotas = ""
            monthTextfield.text = ""
        }
        
        if textField == amountTextfield || textField == monthTextfield{
            bottomRequestConstraint.constant = 30
            requestContainerView.isHidden = true
            heighResultConstraint.constant = 0
            self.resultCalculateView.alpha = 0
            self.titleCuotaMensualLabel.alpha = 0
            self.cuotaMensualLabel.alpha = 0
            self.view.layoutIfNeeded()
            self.viewWillLayoutSubviews()
            self.viewDidLayoutSubviews()
        }
        
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        let count = textField.text?.count ?? 0
        
        if textField == amountTextfield {
            if count > 0 && amount != textField.text{
                amount = textField.text ?? "0"
                amount = amount.replacingOccurrences(of: ",", with: "")
                validateValues(isAmount: true)
            }
        }
        
        if textField == monthTextfield{
            if count > 0 && cuotas != textField.text{
                cuotas = textField.text ?? "0"
                validateValues(isAmount: false)
            }
        }
    }
    
}

extension DMResultViewController: UIPickerViewDelegate, UIPickerViewDataSource{
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayRefuses.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        dmDialogIdentity.cod = arrayRefuses[row].codmotivo
        dmDialogIdentity.nombrePadreMadreTextfield.text = arrayRefuses[row].desmotivo
        return arrayRefuses[row].desmotivo
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        dmDialogIdentity.cod = arrayRefuses[row].codmotivo
        dmDialogIdentity.nombrePadreMadreTextfield.text = arrayRefuses[row].desmotivo
    }
    
}

extension DMResultViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayDesembolsos.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DMResultTableViewCell.cellId, for: indexPath) as? DMResultTableViewCell else {
            return DMResultTableViewCell()
        }
        cell.loadData(title: self.arrayDesembolsos[indexPath.row].tipdesedes ?? "", isSelect: self.isTimeSelected(index: indexPath.row))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        timeIndexSelected = indexPath.row
        tableview.reloadData()

        if timeIndexSelected == 0{
            self.ckeckBnButton.isSelected = false
        }
        topBNContainerView.alpha = timeIndexSelected == 1 ? 1 : 0
        self.topBNContainerConstraint.constant = timeIndexSelected == 1 ? 0 : -topBNContainerView.frame.height
        self.view.layoutIfNeeded()
        self.viewWillLayoutSubviews()
        self.viewDidLayoutSubviews()
    }
}
