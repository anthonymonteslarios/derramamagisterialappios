//
//  DMResultRequestTableViewCell.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 26/11/20.
//

import UIKit

class DMResultRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dataRequestLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var TEALabel: UILabel!
    @IBOutlet weak var CuotaMensualLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 5.0
        containerView.layer.borderWidth = 1.0
        containerView.layer.borderColor = Colors.cardColor().cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
