//
//  DMContainerDocumentsViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 23/11/20.
//

import UIKit

class DMContainerDocumentsViewController: UIViewController{
    
    @IBOutlet weak var pageControllerHolderView: UIView!
      lazy var pageViewController: UIPageViewController = {
         return UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
      }()
      
      override func viewDidLoad() {
          super.viewDidLoad()
          // Do any additional setup after loading the view.
          setupPageViewController()
      }
      
      func setupPageViewController() {
          //set its datasource and delegate methods
          self.pageViewController.dataSource = self
          self.pageViewController.delegate = self
          self.pageViewController.view.frame = .zero
          
          //Show view controller with initial page - page zero
          let pageController = getPageFor(index: 0)
          guard let initialPageController = pageController else { return }
          self.pageViewController.setViewControllers([initialPageController], direction: .forward, animated: false, completion: nil)
          self.addChild(self.pageViewController)
          
          //Add to holder view
          self.pageControllerHolderView.addSubview(self.pageViewController.view)
          self.pageViewController.didMove(toParent: self)
         
          //Pin to super view - (holder view)
          self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
          self.pageViewController.view.topAnchor.constraint(equalTo: self.pageControllerHolderView.topAnchor).isActive = true
          self.pageViewController.view.leftAnchor.constraint(equalTo: self.pageControllerHolderView.leftAnchor).isActive = true
          self.pageViewController.view.bottomAnchor.constraint(equalTo: self.pageControllerHolderView.bottomAnchor).isActive = true
          self.pageViewController.view.rightAnchor.constraint(equalTo: self.pageControllerHolderView.rightAnchor).isActive = true
      }
      
      //Helper method to create view controllers for page view controller
      //for specified index
      func getPageFor(index: Int) -> DMDocumentsViewController? {
        
        guard  let pageController  = UIStoryboard.init(name: "RequestCredit", bundle: Bundle.main).instantiateViewController(withIdentifier: "DMDocumentViewController") as? DMDocumentsViewController else { return nil }
          //pageController.pageIndex = index
          return pageController
      }
  }

  extension DMContainerDocumentsViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
      
      func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore
          viewController: UIViewController) -> UIViewController? {
          guard let beforePage = viewController as? DMDocumentsViewController else { return nil }
          
          //since it is before we need to go back the index
          let newIndex = 0
          if newIndex < 0 { return nil }
          return getPageFor(index: newIndex)
      }
      
      func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
          guard let afterPage = viewController as? DMDocumentsViewController else { return nil }
          
          //since it is after we need to go forword
          let newIndex = 0
          if newIndex < 0 { return nil }
          return getPageFor(index: newIndex)
      }
      
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {}
      }
