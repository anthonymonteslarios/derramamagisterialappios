//
//  DMEvaluationViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/15/20.
//

import UIKit

protocol DMReqCreditEvaluationDelegate: class{
    func nextPage(controller: DMBaseViewController)
    func setTypeUnlock(controller: DMBaseViewController, type: TabsUnlocktype)
    func refreshPage(controller: DMBaseViewController)
}

class DMReqCreditEvaluationViewController: DMBaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var amountAproveLabel: UILabel!
    @IBOutlet weak var tasaLabel: UILabel!
    @IBOutlet weak var calculateContainerView: UIView!
    
    @IBOutlet weak var questionAmountLabel: UILabel!
    @IBOutlet weak var amountRangeLabel: UILabel!
    @IBOutlet weak var questionPlazoLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var calculateButton: UIButton!
    
    @IBOutlet weak var undeLineView: UIView!
    @IBOutlet weak var undeLineMonthView: UIView!
    
    @IBOutlet weak var amountTextfield: UITextField!
    @IBOutlet weak var monthTextfield: UITextField!
    
    @IBOutlet weak var requestContainerView: UIView!
    @IBOutlet weak var bottomRequestConstraint: NSLayoutConstraint!
    @IBOutlet weak var requestButton: UIButton!
    
    @IBOutlet weak var cuotaMensualContainerView: UIView!
    @IBOutlet weak var bottomCuotaMensualConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var titleCuotaMensualLabel: UILabel!
    @IBOutlet weak var cuotaMensualLabel: UILabel!
    @IBOutlet weak var descCuotaMensualLabel: UILabel!
    @IBOutlet weak var showMyProductsButton: UIButton!
    
    @IBOutlet weak var heightCalculateButtonConstraint: NSLayoutConstraint!
    
    private var DNI : String = UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue)!
    
    private var amount = ""
    private var cuotas = ""
    private var textRangeAmount = ""
    private var textRangePlazo = ""
    private var validateAmount = false
    private var validateCuota = false
    private var showMensualContainer = false
    private var indexRequestTypeCredit: Int?
    private var typeConsumo = Constant.CreditoConsumo
    
    weak var delegate: DMReqCreditEvaluationDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addStyleToElements()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        amount = ""
        cuotas = ""
    }
    
    //MARK: - Private methods
    private func initView() {
        indexRequestTypeCredit = UserDefaults.standard.integer(forKey: UserDefault.requestTypeCredit.rawValue) //0
        refreshData()
        let complete = UserDefaults.standard.bool(forKey: UserDefault.requestComplete.rawValue) //false
        let solicitud = DMProduct.shared.credit?.estadoSolicitud != nil //false
        
        let isAllFalse = (solicitud == false &&  complete == false) //true
        let isSomeTrue = (solicitud || complete) //false
        
        if (isAllFalse || isSomeTrue) && !showMensualContainer{
            showMensualContainer = false // Pasa
        }else{
            showMensualContainer = true
        }
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                
                typeConsumo = Constant.CreditoTeApoyo
                autoCompleteData(isSolicitudSend: true)
                if isSomeTrue && !showMensualContainer{
                    showMensualContainer = false
                }else{
                    showMensualContainer = true
                }
                needShowCuotaMensualContainer(show: showMensualContainer)
                
            }else{
                typeConsumo = Constant.CreditoConsumo //N
                

                
                autoCompleteData(isSolicitudSend: solicitud)
                needShowCuotaMensualContainer(show: showMensualContainer)
            }
            

        }else{
            
            typeConsumo = Constant.CreditoTeApoyo
            autoCompleteData(isSolicitudSend: true)
            if isSomeTrue && !showMensualContainer{
                showMensualContainer = false
            }else{
                showMensualContainer = true
            }
            needShowCuotaMensualContainer(show: showMensualContainer)
        }
    }

    private func refreshData(){
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            
            
            if  DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                    DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                textRangeAmount = ""
                textRangePlazo = ""
                
                titleLabel.text = "Crédito #teApoyo"
                descLabel.text = "Tienes un crédito pre aprobado de"
                tasaLabel.text = "con una tasa especial"
                questionAmountLabel.text = "Tienes un monto de"
                questionPlazoLabel.text = "a un plazo de"
                amountRangeLabel.text = textRangeAmount
                monthLabel.text = textRangePlazo
                
                amountAproveLabel.text = formatAmount(DMProduct.shared.credit?.montoMaximoOtorgar ?? "")
                
                titleCuotaMensualLabel.text = "Tu cuota mensual es:"
                cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                
                descCuotaMensualLabel.text = ""
            }else{
                var plazoMin = ""
                if  DMProduct.shared.credit?.estadoSolicitud != nil{
                    plazoMin = DMProduct.shared.credit?.plazoMinimoOtorgar ?? ""
                }else{
                    plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.plazoMinimoOtorgar ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
                }
                
                textRangeAmount = "Puedes elegir un monto de \(formatAmount(DMProduct.shared.credit?.montoMinimoOtorgar ?? "")) y \(formatAmount(DMProduct.shared.credit?.montoMaximoOtorgar ?? ""))"
                textRangePlazo = "Puedes elegir un plazo entre \(plazoMin) y \(DMProduct.shared.credit?.plazoMaximoOtorgar ?? "") meses"
                
                titleLabel.text = "Crédito de Consumo"
                descLabel.text = "Tienes un crédito pre aprobado de hasta"
                tasaLabel.text = "con una tasa especial" //  de \(DMProduct.shared.credit?.interesCredito ?? "0")%
                questionAmountLabel.text = "¿Que monto quieres?"
                questionPlazoLabel.text = "¿En que plazo?"
                amountRangeLabel.text = textRangeAmount
                monthLabel.text = textRangePlazo
                
                amountAproveLabel.text = formatAmount(DMProduct.shared.credit?.montoMaximoOtorgar ?? "")
                
                titleCuotaMensualLabel.text = "Tu cuota mensual sería:"
                cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                
                let boldTasa = "tasa mensual especial"
                let boldAhorro = "ahorro en tu cuenta mensual de \(formatAmount(DMProduct.shared.credit?.ahorroCuotaPromocional ?? ""))"
                let boldTotal = formatAmount(DMProduct.shared.credit?.ahorroTotalPromocional ?? "")
                let finalCuotaString = "Tu crédito tiene una \(boldTasa) lo que genera un \(boldAhorro) y un total de \(boldTotal) al término de tu cronograma."
                
                showMyProductsButton.isHidden = true
                
                if DMProduct.shared.credit?.estadoSolicitud != nil{
                    descCuotaMensualLabel.text = ""
                }else{
                    descCuotaMensualLabel.attributedText = getAttributeDesc(text: finalCuotaString, boldStrings: [boldTasa, boldAhorro, boldTotal])
                }
            }

        }else{
            textRangeAmount = ""
            textRangePlazo = ""
            
            titleLabel.text = "Crédito #teApoyo"
            descLabel.text = "Tienes un crédito pre aprobado de"
            tasaLabel.text = "con una tasa especial"
            questionAmountLabel.text = "Tienes un monto de"
            questionPlazoLabel.text = "a un plazo de"
            amountRangeLabel.text = textRangeAmount
            monthLabel.text = textRangePlazo
            
            amountAproveLabel.text = formatAmount(DMProduct.shared.credit?.montoMaximoOtorgar ?? "")
            
            titleCuotaMensualLabel.text = "Tu cuota mensual es:"
            cuotaMensualLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
            
            descCuotaMensualLabel.text = ""
        }

    }
    
    func autoCompleteData(isSolicitudSend: Bool){
        
        let complete = UserDefaults.standard.bool(forKey: UserDefault.requestComplete.rawValue)
        let tipoSolicitud = Int(DMProduct.shared.credit?.estadoSolicitud ?? "0") ?? 0
        let type = TabsUnlocktype.init(rawValue: tipoSolicitud) ?? .none
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            
            if  DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                    DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                amount = DMProduct.shared.credit?.montoOtorgar ?? ""
                cuotas = DMProduct.shared.credit?.plazoOtorgar ?? ""
            }else{
                amount = isSolicitudSend || complete ? DMProduct.shared.credit?.montoOtorgar ?? "" : amount
                cuotas = isSolicitudSend || complete ? DMProduct.shared.credit?.plazoOtorgar ?? "" : cuotas
            }

        }else{
            amount = DMProduct.shared.credit?.montoOtorgar ?? ""
            cuotas = DMProduct.shared.credit?.plazoOtorgar ?? ""
        }

        
        amountTextfield.text = formatNoSymbolAmount(amount)
        monthTextfield.text = cuotas
        amountTextfield.isUserInteractionEnabled = isSolicitudSend ? !isSolicitudSend : !complete
        monthTextfield.isUserInteractionEnabled = isSolicitudSend ? !isSolicitudSend : !complete
        
        calculateButton.alpha = isSolicitudSend ? 0 : (complete ? 0 : 1)
        heightCalculateButtonConstraint.constant = isSolicitudSend ? 0 : (complete ? 0 : 35)
        self.view.layoutIfNeeded()
        if type == .none{
            if complete{
                delegate?.setTypeUnlock(controller: self, type: .evaluado)
            }else{
                delegate?.setTypeUnlock(controller: self, type: .none)
            }
        }else{
            delegate?.setTypeUnlock(controller: self, type: type)
        }
    }
    
    private func addStyleToElements(){
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .center
        descLabel.textColor = Colors.tabDeselectColor()
        descLabel.personalizeLabelStyle(fontSize: 15, isBold: false)
        
        tasaLabel.numberOfLines = 0
        tasaLabel.textAlignment = .center
        tasaLabel.textColor = Colors.tabDeselectColor()
        tasaLabel.personalizeLabelStyle(fontSize: 15, isBold: false)
        
        questionAmountLabel.numberOfLines = 0
        questionAmountLabel.textAlignment = .center
        questionAmountLabel.textColor = Colors.productCellTextColor()
        questionAmountLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        amountRangeLabel.numberOfLines = 0
        amountRangeLabel.textAlignment = .center
        amountRangeLabel.textColor = Colors.textMenuColor()
        amountRangeLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        showMyProductsButton.titleLabel?.textColor = Colors.textMenuColor()
        
        questionPlazoLabel.numberOfLines = 0
        questionPlazoLabel.textAlignment = .center
        questionPlazoLabel.textColor = Colors.productCellTextColor()
        questionPlazoLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        monthLabel.numberOfLines = 0
        monthLabel.textAlignment = .center
        monthLabel.textColor = Colors.textMenuColor()
        monthLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        amountAproveLabel.personalizeAmountRoundStyle()
        calculateButton.personalizeButtonRoundBasicStyle(with: "CALCULA TU CUOTA")
        requestButton.personalizeButtonRoundBasicStyle(with: "SOLICÍTALO AQUÍ")
        
        calculateContainerView.backgroundColor = Colors.cardColor()
        calculateContainerView.layer.cornerRadius = 10
        
        undeLineView.backgroundColor = Colors.textMenuColor()
        undeLineMonthView.backgroundColor = Colors.textMenuColor()
        
        amountTextfield.font = UIFont(name: Constant.dmFontBold, size: 16)
        monthTextfield.font = UIFont(name: Constant.dmFontBold, size: 16)
        amountTextfield.textAlignment = .center
        monthTextfield.textAlignment = .center
        amountTextfield.delegate = self
        monthTextfield.delegate = self
        amountTextfield.textColor = Colors.productCellTextColor()
        monthTextfield.textColor = Colors.productCellTextColor()

        requestContainerView.backgroundColor = Colors.screenColor()
        scrollView.backgroundColor = Colors.primaryColor()
        bodyView.backgroundColor = Colors.screenColor()
        headerView.backgroundColor = .clear
        
        titleCuotaMensualLabel.numberOfLines = 0
        titleCuotaMensualLabel.textAlignment = .center
        titleCuotaMensualLabel.textColor = Colors.productCellTextColor()
        titleCuotaMensualLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        cuotaMensualLabel.numberOfLines = 0
        cuotaMensualLabel.textAlignment = .center
        cuotaMensualLabel.textColor = Colors.productCellTextColor()
        cuotaMensualLabel.personalizeLabelStyle(fontSize: 16, isBold: true)

        descCuotaMensualLabel.numberOfLines = 0
        descCuotaMensualLabel.textAlignment = .center
        descCuotaMensualLabel.textColor = Colors.textMenuColor()
        descCuotaMensualLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        view.backgroundColor = Colors.screenColor()
    }
    
    private func getAttributeDesc(text: String, boldStrings: [String]) -> NSAttributedString {
        let description = NSMutableString(string: text)
        let attributeString = NSMutableAttributedString(string: text)
        attributeString.addAttribute(.font, value: UIFont(name: Constant.dmFont, size: 13)!, range: NSRange(location: 0, length: attributeString.length))
        
        for boldString in boldStrings {
            let range : NSRange = description.range(of: boldString, options: .caseInsensitive)
            attributeString.addAttribute(.font, value: UIFont(name: Constant.dmFontBold, size: 13)!, range: range)
            attributeString.addAttribute(.foregroundColor, value: Colors.productCellTextColor(), range: range)
        }
        return attributeString
    }
    
    private func formatNoSymbolAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Utils.formatIntegerAmount(amount: amount, withFraction: false)
    }
    
    private func formatAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Constant.coinSymbolPEN+" "+Utils.formatIntegerAmount(amount: amount)
    }
    
    private func fetchDataGET(Paralelo: String, handler: (() -> Void)? = nil){
        self.view.isUserInteractionEnabled = false
        self.loadingAnimation()
        
        var nCuota = ""
        var mOtorgar = ""
        var nCuotaMax = ""
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            nCuota = self.cuotas == Constant.textEmpty ? DMProduct.shared.credit?.plazoMaximoOtorgar ?? "" : self.cuotas
            mOtorgar = DMProduct.shared.credit?.montoMaximoOtorgar ?? "0"
            nCuotaMax = DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0"
        }else{
            nCuota = self.cuotas == Constant.textEmpty ? DMProduct.shared.credit?.plazoMaximoOtorgar ?? "" : DMProduct.shared.credit?.plazoOtorgar ?? ""
            mOtorgar = DMProduct.shared.credit?.montoMaximoOtorgar ?? "0"
            nCuotaMax = DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0"
        }
         
        
        Manager.shared.requestGetProducts(ASODNI: DNI, RECALCULAR: Constant.Recalcular, MONTO_OTORGAR: amount, NROCUOTAS: nCuota, TEM: "", MONTO_OTORGAR_X: mOtorgar, NROCUOTAS_X: nCuotaMax, PARALELO: Paralelo)  {(dataGet, error) in
                        
            self.view.isUserInteractionEnabled = true
            
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.stopLoadingAnimating()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }
            
            self.needShowCuotaMensualContainer(show: self.showMensualContainer)
            
            if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
                DMProduct.shared.credit = dataGet
            }else{
                DMProduct.shared.credit = dataGet
            }
            
            self.initView()
            self.stopLoadingAnimating()
            handler?()
        }
    }
    
    private func needShowCuotaMensualContainer(show: Bool){
        bottomRequestConstraint.constant = show ? 90 : 30
        requestContainerView.isHidden = !show
        
        let complete = UserDefaults.standard.bool(forKey: UserDefault.requestComplete.rawValue)
        let solicitud = DMProduct.shared.credit?.estadoSolicitud != nil
        let isSomeTrue = (solicitud || complete)
                 
        bottomCuotaMensualConstraint.constant = isSomeTrue ? (cuotaMensualContainerView.frame.height + 20 ) : (show ? (cuotaMensualContainerView.frame.height + 20) : 10)
        cuotaMensualContainerView.alpha = isSomeTrue ? 1 : (show ? 1 : 0)
        self.view.layoutIfNeeded()
    }
    
    private func validateValues(isAmount : Bool){
        var min = 0.0
        var max = 0.0
        var minPlazo = 0.0
        var maxPlazo = 0.0
        
        var plazoMin = ""
        if  DMProduct.shared.credit?.estadoSolicitud != nil{
            plazoMin = DMProduct.shared.credit?.plazoMinimoOtorgar ?? ""
        }else{
            plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.plazoMinimoOtorgar ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
        }
        
        if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
            min = Double(DMProduct.shared.credit?.montoMinimoOtorgar ?? "0") ?? 0.0
            max = Double(DMProduct.shared.credit?.montoMaximoOtorgar ?? "0") ?? 0.0
            minPlazo = Double(plazoMin) ?? 0.0
            maxPlazo = Double(DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0") ?? 0.0
        }else{
            min = Double(DMProduct.shared.credit?.montoMinimoOtorgar ?? "0") ?? 0.0
            max = Double(DMProduct.shared.credit?.montoMaximoOtorgar ?? "0") ?? 0.0
            minPlazo = Double(plazoMin) ?? 0.0
            maxPlazo = Double(DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0") ?? 0.0
        }

        
        let amount = Double(self.amount) ?? 0.0
        let cuotas = Double(self.cuotas) ?? 0.0
        
        validateAmount = amount >= min && amount <= max
        validateCuota = cuotas >= minPlazo && cuotas <= maxPlazo
        
        if isAmount{
            if !validateAmount{
                self.alertasNativas(title: Constant.textEmpty, message: textRangeAmount, acceptButton: "Aceptar")
                return
            }
        }else{
            if !validateCuota{
                self.alertasNativas(title: Constant.textEmpty, message: textRangePlazo, acceptButton: "Aceptar")
                return
            }
        }
        showMensualContainer = false
        if isAmount{
            self.fetchDataGET(Paralelo: typeConsumo)
        }
    }
    
    @IBAction func showMyProductsAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name("ShowToProducts"), object: nil)
    }
    private func validateBothValues() -> Bool{
        
        if !(self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil){
            var min = 0.0
            var max = 0.0
            var minPlazo = 0.0
            var maxPlazo = 0.0
            
            var plazoMin = ""
            if  DMProduct.shared.credit?.estadoSolicitud != nil{
                plazoMin = DMProduct.shared.credit?.plazoMinimoOtorgar ?? ""
            }else{
                plazoMin = amount == Constant.textEmpty ? DMProduct.shared.credit?.plazoMinimoOtorgar ?? "" : DMProduct.shared.credit?.plazoMinimoPosible ?? ""
            }
            
            if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
                min = Double(DMProduct.shared.credit?.montoMinimoOtorgar ?? "0") ?? 0.0
                max = Double(DMProduct.shared.credit?.montoMaximoOtorgar ?? "0") ?? 0.0
                minPlazo = Double(plazoMin) ?? 0.0
                maxPlazo = Double(DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0") ?? 0.0
            }else{
                min = Double(DMProduct.shared.credit?.montoMinimoOtorgar ?? "0") ?? 0.0
                max = Double(DMProduct.shared.credit?.montoMaximoOtorgar ?? "0") ?? 0.0
                minPlazo = Double(plazoMin) ?? 0.0
                maxPlazo = Double(DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0") ?? 0.0
            }

            
            let amount = Double(self.amount) ?? 0.0
            let cuotas = Double(self.cuotas) ?? 0.0
            
            validateAmount = amount >= min && amount <= max
            validateCuota = cuotas >= minPlazo && cuotas <= maxPlazo
        }
        
        if !validateAmount{
            self.alertasNativas(title: Constant.textEmpty, message: textRangeAmount, acceptButton: "Aceptar")
            return false
        }
        if !validateCuota{
            self.alertasNativas(title: Constant.textEmpty, message: textRangePlazo, acceptButton: "Aceptar")
            return false
        }
        return true
    }
    
    @IBAction func calculateButtonAction(_ sender: Any?) {
        if validateBothValues() {
            showMensualContainer = true
            fetchDataGET(Paralelo: typeConsumo) {
                let bottomOffset = CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height)
                self.scrollView.setContentOffset(bottomOffset, animated: true)
            }
        }
    }

    @IBAction func requestHereButtonAction(_ sender: Any?) {
        if validateBothValues() {
            showMensualContainer = false
            UserDefaults.standard.set(true, forKey: UserDefault.requestComplete.rawValue)
            delegate?.nextPage(controller: self)
        }
    }
}

extension DMReqCreditEvaluationViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == amountTextfield{
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    amount = ""
                    amountTextfield.text = ""
                }
            }
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == amountTextfield{
            cuotas = ""
            monthTextfield.text = ""
        }
        showMensualContainer = false
        needShowCuotaMensualContainer(show: showMensualContainer)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        let count = textField.text?.count ?? 0
        
        if textField == amountTextfield {
            if count > 0 && amount != textField.text{
                amount = textField.text ?? "0"
                amount = amount.replacingOccurrences(of: ",", with: "")
                validateValues(isAmount: true)
            }
        }
        
        if textField == monthTextfield{
            if count > 0 && cuotas != textField.text{
                cuotas = textField.text ?? "0"
                validateValues(isAmount: false)
            }
        }
    }
}
