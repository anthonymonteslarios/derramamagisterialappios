//
//  DMRequestCreditViewController.swift
//  DemoNavigation
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/13/20.
//

import Foundation

class DMRequestCreditViewController: DMBaseViewController {
    
    @IBOutlet weak var headerReqCreditContainer: UIView!
    @IBOutlet weak var heighHeaderConstraint: NSLayoutConstraint!
    
    private var pageViewController: UIPageViewController?
    private var headerReqCreditView: DMHeaderReqCreditView!
    private var showSendView = false
    var numeberPages = 0
    var inTransition = false
    
    var evaluation : DMReqCreditEvaluationViewController!
    var information : DMInformationViewController!
    var result : DMResultViewController!
    var document : DMDocumentsViewController!
    var manualRegister : DMManualRegisterDocumentViewController!
    
    var pages : [UIViewController]!
    var countPages = 0
    
    var isManualRegister : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
        
        if !isPhoto{
            isPhoto = !isPhoto
            pages = createController()
        }
        validateSolicitud()
        setViewController()
    }
    
    func createController() -> [UIViewController]{
        var arrayPages = [UIViewController]()
        
        evaluation = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "DMReqCreditEvaluationViewController") as? DMReqCreditEvaluationViewController
        information = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "DMInformationViewController") as? DMInformationViewController
        result = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "DMResultViewController") as? DMResultViewController
        
        
        document = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "DMDocumentViewController") as? DMDocumentsViewController
        manualRegister = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "DMManualRegisterDocumentViewController") as? DMManualRegisterDocumentViewController
        
        evaluation.delegate = self
        information.delegate = self
        result.delegate = self
        document.delegate = self
        
        //let nroIntentos = UserDefaults.standard.integer(forKey:UserDefault.countIntentos.rawValue)
        //var nroIntentoMaximos = Int((DMProduct.shared.credit?.nrointentosmaximos)!)!
        //nroIntentoMaximos = 100
        
        
        if DMProduct.shared.credit?.validen == nil {
            arrayPages = [evaluation, information, result, document, manualRegister]
        }else{
            arrayPages = [evaluation, information, result, manualRegister]
        }
        
        
        if DMProduct.shared.credit?.recogerdoc != nil{
            arrayPages = [evaluation, information, result, manualRegister]
        }
        
        countPages = arrayPages.count
        
        return arrayPages
    }
    
    
    //MARK: - Private methods
    private func initView() {
        configureHeaderReqCreditView()
    }
    
    private func validateSolicitud(){
        showSendView = DMProduct.shared.credit?.estadoSolicitud != nil
        heighHeaderConstraint.constant = showSendView ? 120 : 70
        self.view.layoutIfNeeded()
    }
    
    private func configureHeaderReqCreditView() {
        headerReqCreditView = DMHeaderReqCreditView.instanceViewFromXIB()
        headerReqCreditView.delegate = self
        headerReqCreditView.loadTitle(title: DMProduct.shared.credit?.estadoSolicitudLabel)
        headerReqCreditContainer.addSubview(headerReqCreditView)
        var frame = headerReqCreditView.frame
        frame.origin = CGPoint.zero
        frame.size = headerReqCreditContainer.frame.size
        headerReqCreditView.frame = frame
    }
    
    private func setViewController() {
        let tipoSolicitud = Int(DMProduct.shared.credit?.estadoSolicitud ?? "0") ?? 0
        let type = TabsUnlocktype.init(rawValue: tipoSolicitud) ?? .none
        headerReqCreditView.setColorsSelect(type.getIndex() ?? 0)
        headerReqCreditView.disableTabs(stepType: type)            
        headerReqCreditView.loadTitle(title: DMProduct.shared.credit?.estadoSolicitudLabel)
    
        if !isManualRegister {
            self.pageViewController?.setViewControllers([self.pages[type.getIndex() ?? 0]], direction: .forward, animated: false, completion: nil)
        }else{
            validateManualController()
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "Page" {
            self.pageViewController = segue.destination as? UIPageViewController
            self.pageViewController?.delegate = self
        }
    }

    
}

extension DMRequestCreditViewController : DMHeaderReqCreditViewDelegate{
    func selectTab(index: Int) {
        validateSolicitud()
        let currentVC = self.pages[index]
        self.pageViewController?.setViewControllers([currentVC], direction: .forward, animated: false, completion: nil)
    }
}

extension DMRequestCreditViewController : DMDocumentsDelegate{
    func validateManualController() {
        let currentVC = self.pages[countPages - 1]
        isManualRegister = true
        self.pageViewController?.setViewControllers([currentVC], direction: .forward, animated: false, completion: nil)
    }
    
}


extension DMRequestCreditViewController : UIPageViewControllerDelegate{

    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        
        self.inTransition = true
        
        if let firstVC = pendingViewControllers.first, let index = self.pages.firstIndex(of: firstVC){
            self.numeberPages = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        self.inTransition = false
        
        if let previousVC = previousViewControllers.first, let previousIndex = self.pages.firstIndex(of: previousVC), !completed {
            self.numeberPages = previousIndex
        }
    }

}

extension DMRequestCreditViewController: DMReqCreditEvaluationDelegate{
    
    func refreshPage(controller: DMBaseViewController) {
        validateSolicitud()
        self.setViewController()
    }
    

    func setTypeUnlock(controller: DMBaseViewController, type: TabsUnlocktype) {
        validateSolicitud()
        headerReqCreditView.disableTabs(stepType: type)
        headerReqCreditView.loadTitle(title: DMProduct.shared.credit?.estadoSolicitudLabel)
    }

    func nextPage(controller: DMBaseViewController) {

        var index: Int?
        
        if controller is DMReqCreditEvaluationViewController{
            index = self.pages.firstIndex(of: information)
        }else if controller is DMInformationViewController{
            index = self.pages.firstIndex(of: evaluation)
        }
        
        if let index = index {
            self.pageViewController?.setViewControllers([self.pages[index]], direction: .forward, animated: false, completion: nil)
            headerReqCreditView.setColorsSelect(index)
        }
    }
}
