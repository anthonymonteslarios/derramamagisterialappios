//
//  DMInformationViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/16/20.
//

import UIKit

class DMInformationViewController: DMBaseViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var requestAmountLabel: UILabel!
    @IBOutlet weak var plazoTitleLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var quotaTitleLabel: UILabel!
    @IBOutlet weak var quotaLabel: UILabel!
    @IBOutlet weak var dataContainerView: UIView!
    
    @IBOutlet weak var dataTitleLabel: UILabel!
    @IBOutlet weak var calculateTitleLabel: UIButton!
    
    @IBOutlet weak var undeLineView: UIView!
    @IBOutlet weak var undeLineEmailView: UIView!
    
    @IBOutlet weak var phoneTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    
    @IBOutlet weak var requestContainerView: UIView!
    @IBOutlet weak var bottomRequestConstraint: NSLayoutConstraint!
    @IBOutlet weak var requestButton: UIButton!
    
    private var indexRequestTypeCredit: Int?
    private var typeConsumo = Constant.CreditoConsumo
    
    private lazy var refreshControl : UIRefreshControl = {
        
        let _refreshControl = UIRefreshControl()
        
        _refreshControl.tintColor       = .white
        _refreshControl.backgroundColor = .clear
        _refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: .valueChanged)
        
        return _refreshControl
    }()
    
    @objc private func pullToRefresh() {
        fetchRefreshDataGET(Paralelo: typeConsumo)
    }
    
    private var DNI : String = UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue)!
    
    private var phoneSend = ""
    private var emailSend = ""
    private var msgError = ""
    
    weak var delegate: DMReqCreditEvaluationDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addStyleToElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }
    
    //MARK: - Private methods
    private func initView() {
        indexRequestTypeCredit = UserDefaults.standard.integer(forKey: UserDefault.requestTypeCredit.rawValue)
        
        descLabel.text = "Monto solicitado"
        plazoTitleLabel.text = "Plazo"
        quotaTitleLabel.text = "Cuota Mensual"
        dataTitleLabel.text = "Ingresa tus datos de contacto"
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                typeConsumo = Constant.CreditoTeApoyo
                quotaLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                monthLabel.text = "\(DMProduct.shared.credit?.plazoOtorgar ?? "") meses"
                requestAmountLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
                titleLabel.text = "Crédito #teApoyo"
            }else{
                typeConsumo = Constant.CreditoConsumo
                quotaLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
                monthLabel.text = "\(DMProduct.shared.credit?.plazoOtorgar ?? "") meses"
                requestAmountLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
                titleLabel.text = "Crédito de Consumo"
            }
            
        }else{
            typeConsumo = Constant.CreditoTeApoyo
            quotaLabel.text = formatAmount(DMProduct.shared.credit?.cuotaOtorgar ?? "")
            monthLabel.text = "\(DMProduct.shared.credit?.plazoOtorgar ?? "") meses"
            requestAmountLabel.text = formatAmount(DMProduct.shared.credit?.montoOtorgar ?? "")
            titleLabel.text = "Crédito #teApoyo"
        }
    
        emailTextfield.delegate = self
        phoneTextfield.delegate = self
        
        
        var state = ""
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                let solicitud = DMProduct.shared.credit?.estadoSolicitud != nil
                showEvaluationAgin(isSolicitudSend: solicitud)
                state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
            }else{
                let solicitud = DMProduct.shared.credit?.estadoSolicitud != nil
                showEvaluationAgin(isSolicitudSend: solicitud)
                state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
            }

        }else{
            let solicitud = DMProduct.shared.credit?.estadoSolicitud != nil
            showEvaluationAgin(isSolicitudSend: solicitud)
            state = DMProduct.shared.credit?.estadoSolicitud ?? "0"
        }
        
        let tipoSolicitud = Int(state) ?? 0
        let type = TabsUnlocktype.init(rawValue: tipoSolicitud) ?? .none
        delegate?.setTypeUnlock(controller: self, type: type)
        
        if let refresh = self.scrollView.subviews.first(where: {$0 is UIRefreshControl}){
            refresh.removeFromSuperview()
        }
        if type != .none{
            self.scrollView.addSubview(self.refreshControl)
        }
        
    }
    
    private func addStyleToElements(){
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .center
        titleLabel.textColor = .white
        titleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        descLabel.numberOfLines = 0
        descLabel.textAlignment = .center
        descLabel.textColor = Colors.tabDeselectColor()
        descLabel.personalizeLabelStyle(fontSize: 12, isBold: false)
        
        requestAmountLabel.numberOfLines = 0
        requestAmountLabel.textAlignment = .center
        requestAmountLabel.textColor = .white
        requestAmountLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        plazoTitleLabel.numberOfLines = 0
        plazoTitleLabel.textAlignment = .center
        plazoTitleLabel.textColor = Colors.tabDeselectColor()
        plazoTitleLabel.personalizeLabelStyle(fontSize: 12, isBold: false)
        
        monthLabel.numberOfLines = 0
        monthLabel.textAlignment = .center
        monthLabel.textColor = .white
        monthLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        quotaTitleLabel.numberOfLines = 0
        quotaTitleLabel.textAlignment = .center
        quotaTitleLabel.textColor = Colors.tabDeselectColor()
        quotaTitleLabel.personalizeLabelStyle(fontSize: 12, isBold: false)
        
        quotaLabel.numberOfLines = 0
        quotaLabel.textAlignment = .center
        quotaLabel.textColor = .white
        quotaLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        dataTitleLabel.numberOfLines = 0
        dataTitleLabel.textAlignment = .left
        dataTitleLabel.textColor = Colors.productCellTextColor()
        dataTitleLabel.personalizeLabelStyle(fontSize: 16, isBold: true)
        
        calculateTitleLabel.personalizeButtonTransparenttyle(with: "CALCULA OTRA VEZ")
        
        emailTextfield.keyboardType = .asciiCapable
        
        dataContainerView.backgroundColor = Colors.cardColor()
        dataContainerView.layer.cornerRadius = 10
        
        undeLineView.backgroundColor = Colors.textMenuColor()
        undeLineEmailView.backgroundColor = Colors.textMenuColor()
        
        scrollView.backgroundColor = Colors.primaryColor()
        bodyView.backgroundColor = Colors.screenColor()
        headerView.backgroundColor = .clear
        view.backgroundColor = Colors.screenColor()
        
        requestContainerView.backgroundColor = Colors.screenColor()
        
        requestButton.personalizeButtonRoundBasicStyle(with: "ENVIAR")
        
    }
    
    private func showEvaluationAgin(isSolicitudSend: Bool){
        self.calculateTitleLabel.isHidden = isSolicitudSend
        
        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                phoneSend = isSolicitudSend ? DMProduct.shared.credit?.celular ?? "" : ""
                emailSend = isSolicitudSend ? DMProduct.shared.credit?.correo ?? "" : ""
            }else{
                phoneSend = isSolicitudSend ? DMProduct.shared.credit?.celular ?? "" : ""
                emailSend = isSolicitudSend ? DMProduct.shared.credit?.correo ?? "" : ""
            }

        }else{
            phoneSend = isSolicitudSend ? DMProduct.shared.credit?.celular ?? "" : ""
            emailSend = isSolicitudSend ? DMProduct.shared.credit?.correo ?? "" : ""
        }

        phoneTextfield.text = phoneSend
        emailTextfield.text = emailSend
        phoneTextfield.isUserInteractionEnabled = !isSolicitudSend
        emailTextfield.isUserInteractionEnabled = !isSolicitudSend
        
        requestContainerView.isHidden = isSolicitudSend
    }
    
    private func formatAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Constant.coinSymbolPEN+" "+Utils.formatIntegerAmount(amount: amount)
    }
    
    private func isPhoneValid() -> Bool {
        if !phoneTextfield.hasText {
            msgError = Constant.errorPhoneNumberEmpty
            return false
        }
        
        if phoneTextfield.text?.count ?? 0 < 9{
            msgError = Constant.errorPhoneNumberInvalid
            return false
        }
        
        if phoneTextfield.isFirstResponder {
            phoneTextfield.resignFirstResponder()
        }
        return true
    }
    
    private func isEmailValid() -> Bool {
        if !emailTextfield.hasText {
            msgError = Constant.errorEmailEmpty
            return false
        }
        
        if emailTextfield.isFirstResponder {
            emailTextfield.resignFirstResponder()
        }
        
        if self.validateFormatEmail(with: emailSend) {
            msgError = Constant.textEmpty
            return true
        } else {
            msgError = Constant.errorEmailInvalid
            return false
        }
    }
    
    private func validateFormatEmail(with email : String) -> Bool {
        if !email.contains("@-") && !email.contains("@_")
            && !email.contains("@.") && !email.contains("@+")
            && !email.contains("@'") && !email.contains(".@")
            && !email.contains("+@") && !email.contains("'@")
            && !email.contains("ñ") && !email.contains("Ñ"){
            
            let stricterFilterString = "^[a-zA-Z0-9]{1}[-+._'a-zA-Z0-9]+([-+._']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
            
            let emailTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
            return emailTest.evaluate(with: email)
        }
        return false
    }
}

// MARK: - UITextFieldDelegate
extension DMInformationViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        
        if textField == phoneTextfield {
            if count >= 9 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [self] in
                    emailTextfield.becomeFirstResponder()
                }
            }
        }
        
        return textField == phoneTextfield ? (count <= 9) : true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let shouldReturn = false
        if textField == emailTextfield {
            emailSend = emailTextfield.text ?? ""
            _ = isEmailValid()
            emailTextfield.resignFirstResponder()
        }else{
            phoneSend = phoneTextfield.text ?? ""
            _ = isPhoneValid()
            phoneTextfield.resignFirstResponder()
        }
        return shouldReturn
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == emailTextfield {
            emailSend = emailTextfield.text ?? ""
            _ = isEmailValid()
        }else{
            phoneSend = phoneTextfield.text ?? ""
            _ = isPhoneValid()
        }

    }
    
    @IBAction func calculateAgainButtonAction(_ sender: Any?) {
        UserDefaults.standard.set(false, forKey: UserDefault.requestComplete.rawValue)
        delegate?.nextPage(controller: self)
    }
    
    @IBAction func sendButtonAction(_ sender: Any?) {
        if !isPhoneValid(){
            self.alertasNativas(title: Constant.textEmpty, message: msgError, acceptButton: "Aceptar")
            return
        }
        
        if !isEmailValid(){
            self.alertasNativas(title: Constant.textEmpty, message: msgError, acceptButton: "Aceptar")
            return
        }
        
        self.loadingAnimation()

        if indexRequestTypeCredit == 0 || indexRequestTypeCredit == nil{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                loadTeApoyo()
            }else{
                loadConsumo()
            }
            
        }else{
            loadTeApoyo()
        }
    }
    
    private func loadConsumo(){
        Manager.shared.requestSaveRequest(ASOID: DMProduct.shared.credit?.asoid ?? "", ASONCTA: DMProduct.shared.credit?.asoncta ?? "", TIPOCANAL: Constant.canal, ASOAPENOMDNI: DMProduct.shared.credit?.asoapenomdni ?? "", CUENTA_VALIDADA: "", OFERTA_TIPOCREID: DMProduct.shared.credit?.tipoCredito ?? "0", OFERTA_MONMIN: DMProduct.shared.credit?.montoMinimoOtorgar ?? "0", OFERTA_MONMAX: DMProduct.shared.credit?.montoMaximoOtorgar ?? "0", OFERTA_CUOMIN: DMProduct.shared.credit?.plazoMinimoOtorgar ?? "0", OFERTA_CUOMAX: DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0", OFERTA_MONTO: DMProduct.shared.credit?.montoOtorgar ?? "0", OFERTA_PLAZO: DMProduct.shared.credit?.plazoOtorgar ?? "0", OFERTA_CUOTA: DMProduct.shared.credit?.cuotaOtorgar ?? "0", OFERTA_PORINT: DMProduct.shared.credit?.interesCredito ?? "0", OFERTA_PORFLT: DMProduct.shared.credit?.flatCredito ?? "0", OFERTA_PORDES: DMProduct.shared.credit?.desgravamen ?? "0", OFERTA_CAPPAGO: DMProduct.shared.credit?.capacidadPagoMontoDisponible ?? "0", FECEMI: "", FECCAP: "", NOMPADREMADRE: "", CORREO: emailSend, CELULAR: phoneSend, OFERTA_NOTAABONO: DMProduct.shared.credit?.notaAbono ?? "", OFERTA_DIAGRACIA: DMProduct.shared.credit?.diasDeGracia ?? "0", OFERTA_DIAPROCESO: DMProduct.shared.credit?.diasEnProceso ?? "0", OFERTA_DIAPROCESOGRACIA: DMProduct.shared.credit?.diasProcesoGracia ?? "0", OFERTA_MONINTPRO: DMProduct.shared.credit?.montoInteresProceso ?? "0", OFERTA_TEA: DMProduct.shared.credit?.tea ?? "", OFERTA_TCEA: DMProduct.shared.credit?.tcea ?? "", OFERTA_CODPAD: DMProduct.shared.credit?.codpad ?? "0", OFERTA_HABER: DMProduct.shared.credit?.haber ?? "0", OFERTA_DESCUENTO: DMProduct.shared.credit?.descuento ?? "0", OFERTA_LIQUIDO: DMProduct.shared.credit?.liquido ?? "0", FIRMA: "", PARALELO: DMProduct.shared.credit?.paralelo ?? "", DISPOSITIVO: "iPhone", LATITUDE: "0.00", LONGITUDE: "0.00", UA: "", MOBILE: "Apple", PHONE: UIDevice.modelName, TABLET: "", USERAGENT: "", OS: "iOS", IP_PUBLICA: "") { (dataGet, error) in
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.stopLoadingAnimating()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }
            
            self.stopLoadingAnimating()
            guard let dataGet = dataGet else{ return }
            if let success = dataGet.success, success{
                self.fetchRefreshDataGET(Paralelo: Constant.CreditoConsumo)
            }
            self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar")
        }
    }
    
    private func loadTeApoyo(){
        Manager.shared.requestSaveRequest(ASOID: DMProduct.shared.credit?.asoid ?? "", ASONCTA: DMProduct.shared.credit?.asoncta ?? "", TIPOCANAL: Constant.canal, ASOAPENOMDNI: DMProduct.shared.credit?.asoapenomdni ?? "", CUENTA_VALIDADA: "", OFERTA_TIPOCREID: DMProduct.shared.credit?.tipoCredito ?? "0", OFERTA_MONMIN: DMProduct.shared.credit?.montoMinimoOtorgar ?? "0", OFERTA_MONMAX: DMProduct.shared.credit?.montoMaximoOtorgar ?? "0", OFERTA_CUOMIN: DMProduct.shared.credit?.plazoMinimoOtorgar ?? "0", OFERTA_CUOMAX: DMProduct.shared.credit?.plazoMaximoOtorgar ?? "0", OFERTA_MONTO: DMProduct.shared.credit?.montoOtorgar ?? "0", OFERTA_PLAZO: DMProduct.shared.credit?.plazoOtorgar ?? "0", OFERTA_CUOTA: DMProduct.shared.credit?.cuotaOtorgar ?? "0", OFERTA_PORINT: DMProduct.shared.credit?.interesCredito ?? "0", OFERTA_PORFLT: DMProduct.shared.credit?.flatCredito ?? "0", OFERTA_PORDES: DMProduct.shared.credit?.desgravamen ?? "0", OFERTA_CAPPAGO: DMProduct.shared.credit?.capacidadPagoMontoDisponible ?? "0", FECEMI: "", FECCAP: "", NOMPADREMADRE: "", CORREO: emailSend, CELULAR: phoneSend, OFERTA_NOTAABONO: DMProduct.shared.credit?.notaAbono ?? "", OFERTA_DIAGRACIA: DMProduct.shared.credit?.diasDeGracia ?? "0", OFERTA_DIAPROCESO: DMProduct.shared.credit?.diasEnProceso ?? "0", OFERTA_DIAPROCESOGRACIA: DMProduct.shared.credit?.diasProcesoGracia ?? "0", OFERTA_MONINTPRO: DMProduct.shared.credit?.montoInteresProceso ?? "0", OFERTA_TEA: DMProduct.shared.credit?.tea ?? "", OFERTA_TCEA: DMProduct.shared.credit?.tcea ?? "", OFERTA_CODPAD: DMProduct.shared.credit?.codpad ?? "0", OFERTA_HABER: DMProduct.shared.credit?.haber ?? "0", OFERTA_DESCUENTO: DMProduct.shared.credit?.descuento ?? "0", OFERTA_LIQUIDO: DMProduct.shared.credit?.liquido ?? "0", FIRMA: "", PARALELO: DMProduct.shared.credit?.paralelo ?? "", DISPOSITIVO: "iPhone", LATITUDE: "0.00", LONGITUDE: "0.00", UA: "", MOBILE: "Apple", PHONE: UIDevice.modelName, TABLET: "", USERAGENT: "", OS: "iOS", IP_PUBLICA: "") { (dataGet, error) in
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.stopLoadingAnimating()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }
            
            self.stopLoadingAnimating()
            guard let dataGet = dataGet else{ return }
            if let success = dataGet.success, success{
                self.fetchRefreshDataGET(Paralelo: Constant.CreditoTeApoyo)
            }
            self.alertasNativas(title: Constant.textEmpty, message: dataGet.mensaje ?? "", acceptButton: "Aceptar")
        }
    }
    
    private func fetchRefreshDataGET(Paralelo: String){
        self.view.isUserInteractionEnabled = false
        Manager.shared.requestGetProducts(ASODNI: DNI, RECALCULAR: Constant.Recalcular, MONTO_OTORGAR: "0", NROCUOTAS: "0", TEM: "", MONTO_OTORGAR_X: "", NROCUOTAS_X: "", PARALELO: Paralelo)  {(dataGet, error) in
                        
            self.view.isUserInteractionEnabled = true
            
            if (error.first != nil) {
                self.refreshControl.endRefreshing()
                print("error: \(String(describing: error.first?.mensaje))")
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }
            
            if self.indexRequestTypeCredit == 0 || self.indexRequestTypeCredit == nil{
                if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                    DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                    DMProduct.shared.credit = dataGet
                }else{
                    DMProduct.shared.credit = dataGet
                }
                
            }else{
                DMProduct.shared.credit = dataGet
            }
            
            
            DispatchQueue.main.async {
                self.initView()
                self.delegate?.refreshPage(controller: self)
                self.refreshControl.endRefreshing()
            }
        }
    }
}
