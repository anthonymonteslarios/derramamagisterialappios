//
//  DMScannerImageViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 16/11/20.
//

import AVFoundation
import UIKit
import Vision
import ImageDetect

protocol DMScannerDelegate{
    func backToController()
    func backToManual()
}


final class DMScannerImageViewController: UIViewController {
    
    @IBOutlet weak var counterLabel: UILabel!
    private let captureSession = AVCaptureSession()
    private lazy var previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
    
    private var previewLayerCamera:CALayer!
    private var captureDevice:AVCaptureDevice!
    
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private var faceLayers: [CAShapeLayer] = []
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var delegate : DMScannerDelegate?
    
    var isCaptured = false
    var seconds = 3
    var timer = Timer()
    var isTimerRunning = false
    var isFaceDetected = false

    var count = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCamera()
        self.counterLabel.text = Constant.textEmpty
        captureSession.startRunning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.previewLayer.frame = self.containerView.frame
    }
    
    private func setupCamera() {
        
        let deviceSession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInWideAngleCamera], mediaType: .video, position: .front)
        if let device = deviceSession.devices.first {
            if let deviceInput = try? AVCaptureDeviceInput(device: device) {
                if captureSession.canAddInput(deviceInput) {
                    captureSession.addInput(deviceInput)
                    
                    setupPreview()
                }
            }
        }
    }
    @IBAction func closeButtonAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func removeView(){
        delegate?.backToController()
    }
    
    
    private func setupPreview() {
        self.previewLayer.videoGravity = .resizeAspectFill
        //self.view.layer.addSublayer(self.previewLayer)
        
        self.containerView.layer.addSublayer(self.previewLayer)
        self.previewLayer.frame = self.containerView.frame
        
        self.videoDataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]

        self.videoDataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "camera queue"))
        self.captureSession.addOutput(self.videoDataOutput)
        
        let videoConnection = self.videoDataOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
    }
}

extension DMScannerImageViewController: ResultScannerDelegate{
    func restartRegisterDocument() {
        delegate?.backToManual()
    }
    
    func restartAction() {
        self.counterLabel.text = Constant.textEmpty
        self.seconds = 3
        self.count = 0
        self.counterLabel.text = ""
        isTimerRunning = false
        isCaptured = false
        captureSession.startRunning()
    }
    
    func closePresent(){
        dismiss(animated: false) {
            self.delegate?.backToController()
        }
        
    }
    
}


extension DMScannerImageViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
          return
        }


        let faceDetectionRequest = VNDetectFaceLandmarksRequest(completionHandler: { (request: VNRequest, error: Error?) in
        
            DispatchQueue.main.async {
                self.faceLayers.forEach({ drawing in drawing.removeFromSuperlayer() })

                if let observations = request.results as? [VNFaceObservation] {
                    self.handleFaceDetectionObservations(observations: observations)
                }
                
                if self.isCaptured{
                    self.isCaptured = false
                    print("SE IMPRIMEEEEE ESTTO")
                    if let image = self.getImageFromSamplerBuffer(buffer: sampleBuffer){
                        image.detector.crop(type: .face) { [weak self] result in
                            switch result {
                                case .success(let croppedImages):
                                    let vc = UIStoryboard.init(name: "Biometria", bundle: Bundle.main).instantiateViewController(withIdentifier: "ResultScannerFaceViewController") as? ResultScannerFaceViewController
                                    vc?.delegate = self
                                    vc?.faceImage = croppedImages.first
                                    vc?.isFaceDetected = self!.isFaceDetected
                                    vc?.modalPresentationStyle = .fullScreen
                                    self!.captureSession.stopRunning()
                                    self!.present(vc!, animated: true, completion: nil)
                                    print("Found")
                                case .notFound:
                                    print("Not Found")
                                case .failure(let error):
                                    print(error.localizedDescription)
                                }
                        }
                        
                        
                        
                        
                 
                    }
                }
                
            }                        
        })

        
        let imageRequestHandler = VNImageRequestHandler(cvPixelBuffer: imageBuffer, orientation: .leftMirrored, options: [:])

        do {
            try imageRequestHandler.perform([faceDetectionRequest])
            
            if let results = faceDetectionRequest.results as? [VNFaceObservation] {
                
                if results.count == 1 {
                    self.isFaceDetected = true
                }else{
                    self.isFaceDetected = false
                    count = 0
                    self.seconds = 3
                    self.counterLabel.text = ""
                    self.isCaptured = false
                    self.isTimerRunning = false
                    self.timer.invalidate()
                }
                
                if isFaceDetected{
                    let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
                    let image : UIImage = Utils.convertCIImage(cmage: ciimage)
                    
                    if let faceImage = CIImage(image: image) {
                        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
                        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
                        let faces = faceDetector?.features(in: faceImage, options: [CIDetectorSmile:true, CIDetectorEyeBlink: true])
                               
                        if !faces!.isEmpty {
                             for face in faces as! [CIFaceFeature] {
                                 let leftEyeClosed = face.leftEyeClosed
                                 let rightEyeClosed = face.rightEyeClosed
                                 let blinking = leftEyeClosed && rightEyeClosed
                                
                                if blinking{
                                    count += 1
                                    if count == 1{
                                        DispatchQueue.main.async {

                                        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                                                print("Timer fired!")
                                                self.isTimerRunning = true
                                                self.isCaptured = false
                                                self.counterLabel.text = String(self.seconds)
                                                if self.seconds > 0 {
                                                    print("\(self.seconds) seconds to the end of the world")
                                                    self.seconds -= 1
                                                }else{
                                                    self.counterLabel.text = ""
                                                    self.isCaptured = true
                                                    timer.invalidate()
                                                    
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    
                                }
                             }
                            
                            }
                        }
                    }
                }
        } catch {
          print(error.localizedDescription)
        }
    }
    
    func getImageFromSamplerBuffer (buffer: CMSampleBuffer) -> UIImage?{
        if let pixelBuffer = CMSampleBufferGetImageBuffer(buffer){
            let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
            let context = CIContext()
            
            let imageRect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer), height: CVPixelBufferGetHeight(pixelBuffer))
            
            if let image = context.createCGImage(ciImage, from: imageRect){
                return UIImage(cgImage: image, scale: UIScreen.main.scale, orientation: .up)
            }
        }
        return nil
    }
    
    
    public func highlightFaces(for source: UIImage, complete: @escaping (UIImage) -> Void) {
        let resultImage = source
        let detectFaceRequest = VNDetectFaceLandmarksRequest { (request, error) in
            if error == nil {
                if let results = request.results as? [VNFaceObservation] {
                    print("Found \(results.count) faces")
                    
                    for faceObservation in results {
                        guard let landmarks = faceObservation.landmarks else {
                            continue
                        }
                        let boundingRect = faceObservation.boundingBox
                        var landmarkRegions: [VNFaceLandmarkRegion2D] = []
                        if let faceContour = landmarks.faceContour {
                            landmarkRegions.append(faceContour)
                        }
                        if let leftEye = landmarks.leftEye {
                            landmarkRegions.append(leftEye)
                        }
                        if let rightEye = landmarks.rightEye {
                            landmarkRegions.append(rightEye)
                        }
                        
                    }
                }
            } else {
                print(error!.localizedDescription)
            }
            complete(resultImage)
        }
        
        let vnImage = VNImageRequestHandler(cgImage: source.cgImage!, options: [:])
        try? vnImage.perform([detectFaceRequest])
    }
    
    
    private func handleFaceDetectionObservations(observations: [VNFaceObservation]) {
        for observation in observations {
            let faceRectConverted = self.previewLayer.layerRectConverted(fromMetadataOutputRect: observation.boundingBox)
            let faceRectanglePath = CGPath(rect: faceRectConverted, transform: nil)
            
            let faceLayer = CAShapeLayer()
            faceLayer.path = faceRectanglePath
            faceLayer.fillColor = UIColor.clear.cgColor
            faceLayer.strokeColor = UIColor.yellow.cgColor
            
            if self.isTimerRunning{
                self.faceLayers.append(faceLayer)
                
                self.containerView.layer.addSublayer(faceLayer)
            }else{
                self.containerView.layer.removeAllAnimations()
                
                if !self.faceLayers.isEmpty{
                    self.faceLayers.forEach({ drawing in drawing.removeFromSuperlayer() })
                }
            }
            
            
            if let landmarks = observation.landmarks {
                if let leftEye = landmarks.leftEye {
                    self.handleLandmark(leftEye, faceBoundingBox: faceRectConverted)
                }
                if let rightEye = landmarks.rightEye {
                    self.handleLandmark(rightEye, faceBoundingBox: faceRectConverted)
                }
                if self.isTimerRunning{
                    if let outerLips = landmarks.outerLips {
                        self.handleLandmark(outerLips, faceBoundingBox: faceRectConverted)
                    }
                    if let innerLips = landmarks.innerLips {
                        self.handleLandmark(innerLips, faceBoundingBox: faceRectConverted)
                    }
                    if let nose = landmarks.nose {
                        self.handleLandmark(nose, faceBoundingBox: faceRectConverted)
                    }
                    
                }
            }
        }
    }

    func timeString(time:TimeInterval) -> String {
        
        let seconds = Int(time) % 60
        
        return String(format:"%02i", seconds)
    }
    
    
    
    private func handleLandmark(_ eye: VNFaceLandmarkRegion2D, faceBoundingBox: CGRect) {
        let landmarkPath = CGMutablePath()
        let landmarkPathPoints = eye.normalizedPoints
            .map({ eyePoint in
                CGPoint(
                    x: eyePoint.y * faceBoundingBox.height + faceBoundingBox.origin.x,
                    y: eyePoint.x * faceBoundingBox.width + faceBoundingBox.origin.y)
            })


        
        landmarkPath.addLines(between: landmarkPathPoints)
        landmarkPath.closeSubpath()
        let landmarkLayer = CAShapeLayer()
        landmarkLayer.path = landmarkPath
        landmarkLayer.fillColor = UIColor.clear.cgColor
        landmarkLayer.strokeColor = UIColor.green.cgColor

        self.faceLayers.append(landmarkLayer)
        //self.view.layer.addSublayer(landmarkLayer)
        self.containerView.layer.addSublayer(landmarkLayer)
    
 

        
        
        
        
    }
}

