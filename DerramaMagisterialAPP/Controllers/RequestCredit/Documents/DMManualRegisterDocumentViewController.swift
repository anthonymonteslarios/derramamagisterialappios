//
//  DMManualRegisterDocumentViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 19/11/20.
//

import UIKit

enum SelectButton {
    case DNI1
    case DNI2
    case HojaResumen
    case CartaDescuento
    case ContratoHoja1
    case ContratoHoja2
    case ContratoHoja3
    case Pagare
}

protocol  ManualDelegate: NSObject {
    func goToMyRequest()
}

class DMManualRegisterDocumentViewController: DMBaseViewController{
        
    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var signatureElectronicButton: UIButton!
    private var dmDialogTip:DMdialogTipSendFormatView!
    private var dmDialogEmail : DMDialogSuccessDocument!
    var dateEmisionPicker :UIDatePicker!
    var dateCaducidadPicker :UIDatePicker!
    var delegate : ManualDelegate?
    
    
    @IBOutlet weak var oficinaButton: UIButton!
    var isOficina : Bool = false
    @IBOutlet weak var virtualButton: UIButton!
    var isVirtual : Bool = false
    
    var ArrayNames : ResponseRandomNameArray = []
    
    
    @IBOutlet weak var titleTipLabel: UILabel!
    @IBOutlet weak var oficinaView: UIView!
    
    
    @IBOutlet var scrollContainerView: UIScrollView!
    @IBOutlet var containerDocumentsView: UIView!
        
    var DNI1Image : UIImage? = UIImage()
    var DNI2Image : UIImage? = UIImage()
    var HojaResumenImage : UIImage? = UIImage()
    var CartaDescuentoImage : UIImage? = UIImage()
    var ContratoCredito1Image : UIImage? = UIImage()
    var ContratoCredito2Image : UIImage? = UIImage()
    var ContratoCredito3Image : UIImage? =  UIImage()
    var PagareImage : UIImage? = UIImage()
    
    
    //Mark: ImageView
    @IBOutlet weak var Dni1imageView: UIImageView!
    @IBOutlet weak var Dni2Imageview: UIImageView!
    
    @IBOutlet weak var HojaResumentImageView: UIImageView!
    
    @IBOutlet weak var cartaAutorizacionImageView: UIImageView!
    @IBOutlet weak var contratoImageView: UIImageView!
    @IBOutlet weak var contrato2ImageView: UIImageView!
    @IBOutlet weak var contrato3ImageView: UIImageView!
    
    @IBOutlet weak var pagareImageView: UIImageView!
    
    //Mark: Buttons
    
    @IBOutlet weak var DNI1Button: UIButton!
    @IBOutlet weak var DNI2Button: UIButton!
    @IBOutlet weak var HojaResumenButton: UIButton!
    
    @IBOutlet weak var CartaDescuentoButton: UIButton!
    @IBOutlet weak var ContratoCredito1Button: UIButton!
    @IBOutlet weak var ContratoCredito2Button: UIButton!
    @IBOutlet weak var ContratoCredito3Button: UIButton!
    @IBOutlet weak var PagareButton: UIButton!
    
    var selectButtonImage : SelectButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSytleToElements()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        if DMProduct.shared.credit?.recogerdoc == "O" {
            oficinaView.isHidden = true
            oficinaButton.isEnabled = false
            virtualButton.isEnabled = false
            virtualButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            oficinaButton.setImage(UIImage(named: "radio_button_checked"), for: .normal)
            containerDocumentsView.isHidden = true
            signatureElectronicButton.isHidden = true
            signatureElectronicButton.isEnabled = false
        }else if DMProduct.shared.credit?.recogerdoc == "V"{
            oficinaView.isHidden = true
            containerDocumentsView.isHidden = true
            oficinaButton.isEnabled = false
            virtualButton.isEnabled = false
            oficinaButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            virtualButton.setImage(UIImage(named: "radio_button_checked"), for: .normal)
            signatureElectronicButton.isHidden = true
            signatureElectronicButton.isEnabled = false
        }else if DMProduct.shared.credit?.recogerdoc == "F"{
            oficinaButton.isEnabled = false
            virtualButton.isEnabled = false
            oficinaButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            virtualButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
        }
    }
    
    func addSytleToElements(){
        documentView.layer.cornerRadius = 5.0
        documentView.layer.borderWidth = 1.0
        documentView.layer.borderColor = UIColor.black.cgColor
        signatureElectronicButton.layer.cornerRadius = 5.0
        oficinaView.isHidden = true
        scrollContainerView.isScrollEnabled = true
        containerDocumentsView.isHidden = true
    }
    
    @IBAction func signatureElectronicAction(_ sender: Any) {
  

        if !isVirtual && !isOficina{
            alertasNativas(title: "", message: "Debes seleccionar un modo de presentar los documentos del crédito", acceptButton: Constant.acceptButton)
        }else{
            
            if isOficina{
                self.loadingAnimation()
                Manager.shared.requestUpdLoadDocument(CODSOL: (DMProduct.shared.credit?.codsol != Constant.textEmpty) ? DMProduct.shared.credit?.codsol ?? "" : (DMProduct.shared.creditTeApoyo?.codsol)! , RECOGER_DOCUMENTO: self.isOficina ? "O" : "V", ASODNI: DMProduct.shared.credit?.asodni != "" ?  DMProduct.shared.credit?.asodni ?? "" :  DMProduct.shared.creditTeApoyo?.asodni ?? "", file_dni: self.DNI1Image!, file_dni2: self.DNI2Image!, file_contrato_credito: self.ContratoCredito1Image!, file_contrato_credito2: self.ContratoCredito2Image!, file_contrato_credito3: self.ContratoCredito3Image!,  file_carta_autorizacion: self.CartaDescuentoImage!, file_hoja_resumen: self.HojaResumenImage!, file_pagare: self.PagareImage!) { (response) in
                    print(response)
                    self.stopLoadingAnimating()
                                        
                    if response.success == true{
                        let alertGoTo = UIAlertController(title: Constant.textEmpty, message: response.mensaje, preferredStyle: .alert)
                          let OKAction = UIAlertAction(title: Constant.acceptButton, style: .default) { (action:UIAlertAction!) in
                                DMProduct.shared.credit?.recogerdoc = "O"
                                self.signatureElectronicButton.isEnabled = false
                                self.signatureElectronicButton.isHidden = true
                                self.delegate?.goToMyRequest()
                                NotificationCenter.default.post(name: Notification.Name("ShowToRequest"), object: nil)
                                print("Robert shapiro")
                          }
                        alertGoTo.addAction(OKAction)
                        self.present(alertGoTo, animated: true, completion: nil)
                        
                    }else{
                        print("error: \(String(describing: response.mensaje))")
                        self.delegate?.goToMyRequest()
                        self.alertasNativas(title: Constant.textEmpty, message: response.mensaje, acceptButton: Constant.acceptButton)
                    }
                    
                }
                
            }else if isVirtual{
            
                let alertController = UIAlertController(title: "¡MUY IMPORTANTE!", message: Constant.messageAlertCredit, preferredStyle: .alert)
                  let OKAction = UIAlertAction(title: "SI", style: .default) { (action:UIAlertAction!) in
                    if self.isVirtual{
                        self.loadingAnimation()
                        
 
                        if !(Utils.imageIsNullOrNot(imageName: self.DNI1Image!)) && !(Utils.imageIsNullOrNot(imageName: self.DNI2Image!)){
                            print("Is Not DNI Document YES")
                            self.stopLoadingAnimating()
                            self.alertasNativas(title: Constant.textEmpty, message: "Debe ingresar al menos un documento DNI", acceptButton: Constant.acceptButton)
                            return
                        }
                        
                        if !(Utils.imageIsNullOrNot(imageName: self.ContratoCredito1Image!)) && !(Utils.imageIsNullOrNot(imageName: self.ContratoCredito2Image!)) && !(Utils.imageIsNullOrNot(imageName: self.ContratoCredito3Image!)) {
                            
                            self.stopLoadingAnimating()
                            self.alertasNativas(title: Constant.textEmpty, message: "Debe ingresar al menos un documento contrato de credito", acceptButton: Constant.acceptButton)
                            print("Is Not Contrato Document YES")
                            return
                        }
                        
                        if !(Utils.imageIsNullOrNot(imageName: self.CartaDescuentoImage!)) {
                            self.stopLoadingAnimating()
                            self.alertasNativas(title: Constant.textEmpty, message: "Debe ingresar al menos una carta documento carta descuento", acceptButton: Constant.acceptButton)
                            print("Is Not Carta Document YES")
                            return
                        }
                        
                        if !(Utils.imageIsNullOrNot(imageName: self.HojaResumenImage!)) {
                            print("Is Not Hoja Resumen YES")
                            self.stopLoadingAnimating()
                            self.alertasNativas(title: Constant.textEmpty, message: "Debe ingresar al menos una hoja resumen", acceptButton: Constant.acceptButton)
                            return
                        }
                        
                        if !(Utils.imageIsNullOrNot(imageName: self.PagareImage!))  {
                            print("Is Not Pagare YES")
                            self.stopLoadingAnimating()
                            self.alertasNativas(title: Constant.textEmpty, message: "Debe ingresar al menos un docuemnto pagare", acceptButton: Constant.acceptButton)
                            return
                        }
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        Manager.shared.requestUpdLoadDocument(CODSOL: (DMProduct.shared.credit?.codsol != Constant.textEmpty) ? DMProduct.shared.credit?.codsol ?? "" : (DMProduct.shared.creditTeApoyo?.codsol)! , RECOGER_DOCUMENTO: self.isOficina ? "O" : "V", ASODNI: DMProduct.shared.credit?.asodni != "" ?  DMProduct.shared.credit?.asodni ?? "" :  DMProduct.shared.creditTeApoyo?.asodni ?? "", file_dni: self.DNI1Image!, file_dni2: self.DNI2Image!, file_contrato_credito: self.ContratoCredito1Image!, file_contrato_credito2: self.ContratoCredito2Image!, file_contrato_credito3: self.ContratoCredito3Image!,  file_carta_autorizacion: self.CartaDescuentoImage!, file_hoja_resumen: self.HojaResumenImage!, file_pagare: self.PagareImage!) { (response) in
                            print(response)
                            self.stopLoadingAnimating()
                            if response.success == true{
                                let alertGoTo = UIAlertController(title: Constant.textEmpty, message: response.mensaje, preferredStyle: .alert)
                                  let OKAction = UIAlertAction(title: Constant.acceptButton, style: .default) { (action:UIAlertAction!) in
                                        DMProduct.shared.credit?.recogerdoc = "V"
                                        self.delegate?.goToMyRequest()
                                        NotificationCenter.default.post(name: Notification.Name("ShowToRequest"), object: nil)
                                        print("Robert shapiro")
                                  }
                                alertGoTo.addAction(OKAction)
                                self.present(alertGoTo, animated: true, completion: nil)
                            }else{
                                print("error: \(String(describing: response.mensaje))")
                                self.alertasNativas(title: Constant.textEmpty, message: response.mensaje, acceptButton: Constant.acceptButton)
                            }
                        }
                     
                    }
                }
                alertController.addAction(OKAction)
                  
                // Create Cancel button
                let cancelAction = UIAlertAction(title: "NO", style: .cancel) { (action:UIAlertAction!) in
                      print("Cancel button tapped");
                }
                alertController.addAction(cancelAction)
            
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func oficinaAction(_ sender: Any) {
        
        isOficina = !isOficina
        
        if isOficina{
            oficinaView.isHidden = false
            isVirtual = false
            virtualButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            oficinaButton.setImage(UIImage(named: "radio_button_checked"), for: .normal)
            titleTipLabel.text = "Al recibir la confirmación de la aprobación de su crédito, usted deberá apersonarse a cualquier de nuestras oficinas a nivel nacional."
            scrollContainerView.isScrollEnabled = false
            containerDocumentsView.isHidden = true
        }
        
    }
    @IBAction func virtualAction(_ sender: Any) {
        isVirtual = !isVirtual
        
        if isVirtual{
            isOficina = false
            oficinaView.isHidden = true
            oficinaButton.setImage(UIImage(named: "radio_button_unchecked"), for: .normal)
            virtualButton.setImage(UIImage(named: "radio_button_checked"), for: .normal)
            titleTipLabel.text = "Si ya imprimió y firmó los documentos, captúrelos desde la sección CAPTURA DE DOCUMENTOS"
            scrollContainerView.isScrollEnabled = true
            containerDocumentsView.isHidden = false
            
            dmDialogTip = DMdialogTipSendFormatView.instanceViewFromXIB()
            
            var frame = dmDialogTip.frame
            frame.origin = CGPoint.zero
            frame.size = UIScreen.main.bounds.size
            dmDialogTip.frame = frame
            dmDialogTip.delegate = self
            
            navigationController?.view.addSubview(dmDialogTip)

        }

    }
    @IBAction func openDniFrontAction(_ sender: Any) {
        selectButtonImage = SelectButton.DNI1
        self.showAlert()
        
    }
    
    @IBAction func openDniBackAction(_ sender: Any) {
        selectButtonImage = SelectButton.DNI2
        self.showAlert()

    }
    @IBAction func openHojaResumentAction(_ sender: Any) {
        selectButtonImage = SelectButton.HojaResumen
        self.showAlert()
    }
    @IBAction func cartaAutorizadaAction(_ sender: Any) {
        selectButtonImage = SelectButton.CartaDescuento
        self.showAlert()
    }
    @IBAction func contratoCredito1Action(_ sender: Any) {
        selectButtonImage = SelectButton.ContratoHoja1
        self.showAlert()
    }
    @IBAction func contratoCredito2Action(_ sender: Any) {
        selectButtonImage = SelectButton.ContratoHoja2
        self.showAlert()
    }
    @IBAction func contratoCredito3Action(_ sender: Any) {
        selectButtonImage = SelectButton.ContratoHoja3
        self.showAlert()
    }
    @IBAction func pagareAction(_ sender: Any) {
        selectButtonImage = SelectButton.Pagare
        self.showAlert()
    }
}

extension DMManualRegisterDocumentViewController: DMdialogTipDelegate{
    func DocumentacionSend(email: String) {
        loadingAnimation()
                
        Manager.shared.requestDocumentos(asodni: DMProduct.shared.credit?.asoid ?? "", correo: email, CREDID: DMProduct.shared.credit?.credid ?? "", AUTORIZACIONONP: DMProduct.shared.credit?.autorizacionDescuentoOnp ?? "") { (response, error) in
            self.stopLoadingAnimating()

            if response?.first?.success! == true{
                //DMDialogSuccessDocument
                self.dmDialogEmail = DMDialogSuccessDocument.instanceViewFromXIB()
                
                var frame = self.dmDialogEmail.frame
                frame.origin = CGPoint.zero
                frame.size = UIScreen.main.bounds.size
                self.dmDialogEmail.frame = frame
                                
                self.navigationController?.view.addSubview(self.dmDialogEmail)
            }else{
                self.alertasNativas(title: "", message: (response?.first?.mensaje!)!, acceptButton: Constant.acceptButton)
            }
        }
    }
}


extension DMManualRegisterDocumentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private func showAlert() {

        let alert = UIAlertController(title: "Selección de imagen", message: "¿De dónde quieres elegir esta imagen?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camara", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
            self.isPhoto = true 
        }))
        alert.addAction(UIAlertAction(title: "Album de Fotos", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancelar", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    //get image from source type
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }

    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
          
            if self?.selectButtonImage == SelectButton.DNI1{
                self?.DNI1Image = image.resized(withPercentage: 0.3)
                self?.Dni1imageView.image = self?.DNI1Image
            }else if self?.selectButtonImage == SelectButton.DNI2{
                self?.DNI2Image = image.resized(withPercentage: 0.3)
                self?.Dni2Imageview.image = self?.DNI2Image
            }else if self?.selectButtonImage == SelectButton.HojaResumen{
                self?.HojaResumenImage = image.resized(withPercentage: 0.3)
                self?.HojaResumentImageView.image = self?.HojaResumenImage
            }else if self?.selectButtonImage == SelectButton.CartaDescuento{
                self?.CartaDescuentoImage = image.resized(withPercentage: 0.3)
                self?.cartaAutorizacionImageView.image = self?.CartaDescuentoImage
            }else if self?.selectButtonImage == SelectButton.ContratoHoja1{
                self?.ContratoCredito1Image = image.resized(withPercentage: 0.3)
                self?.contratoImageView.image = self?.ContratoCredito1Image
            }else if self?.selectButtonImage == SelectButton.ContratoHoja2{
                self?.ContratoCredito2Image = image.resized(withPercentage: 0.3)
                self?.contrato2ImageView.image = self?.ContratoCredito2Image
            }else if self?.selectButtonImage == SelectButton.ContratoHoja3{
                self?.ContratoCredito3Image = image.resized(withPercentage: 0.3)
                self?.contrato3ImageView.image = self?.ContratoCredito3Image
            }else if self?.selectButtonImage == SelectButton.Pagare{
                self?.PagareImage = image.resized(withPercentage: 0.3)
                self?.pagareImageView.image = self?.PagareImage
            }
                        
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}
