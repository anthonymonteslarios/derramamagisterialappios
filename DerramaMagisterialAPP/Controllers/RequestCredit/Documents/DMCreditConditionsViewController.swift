//
//  DMCreditConditionsViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 16/11/20.
//

import UIKit
import Foundation
import AVFoundation

protocol DMCreditConditionDelegate {
    func backToManual()
}

class DMCreditConditionsViewController: UIViewController {
        
    @IBOutlet weak var ConditionView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var decriptionOneLabel: UILabel!
    @IBOutlet weak var descriptionTwoLabel: UILabel!
    @IBOutlet weak var descriptionThridLabel: UILabel!
    @IBOutlet weak var checkBoxButton: CheckBox!
    @IBOutlet weak var checkedDescriptionLabel: UILabel!
    @IBOutlet weak var NextButton: UIButton!
    
    var delegate: DMCreditConditionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Condiciones del Crédito"
        addStyleToElements()
        initview()
    }
    
    func addStyleToElements(){
        ConditionView.layer.cornerRadius = 5.0
        
        titleLabel.textColor = Colors.productCellTextColor()
        titleLabel.personalizeLabelStyle(fontSize: 20, isBold: true)
        
        decriptionOneLabel.textColor = Colors.textMenuColor()
        
        decriptionOneLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        descriptionTwoLabel.textColor = Colors.textMenuColor()

        descriptionTwoLabel.personalizeLabelStyle(fontSize: 14, isBold: false)

        
        descriptionThridLabel.textColor =  Colors.textMenuColor()

        descriptionTwoLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        descriptionThridLabel.personalizeLabelStyle(fontSize: 14, isBold: false)
        
        checkedDescriptionLabel.personalizeLabelStyle(fontSize: 14, isBold: false)

        NextButton.personalizeButtonRoundBasicStyle(with: "CONTINUAR")
    }

    func initview(){
        
        titleLabel.text = "CONDICIONES DEL CRÉDITO Nº \(DMProduct.shared.credit?.credid ?? "")"
        
        decriptionOneLabel.text = "Docente \(DMProduct.shared.credit?.asoapenomdni ?? "") se ha aprobado su crédito por \(formatAmount(DMProduct.shared.credit?.aceptadoMonoto ?? "")) con una cuota de \(formatAmount(DMProduct.shared.credit?.aceptadoMoncuo ?? "")) el cual será descontado a través de su Planilla de Haberes en \(DMProduct.shared.credit?.aceptadoNrocuo ?? "") cuotas mensuales.\n\nAsi mismo para la aprobación del crédito se han generado los documentos contractuales siguientes:"
        
        descriptionTwoLabel.text = "-Contrato y Adenda-\n\n-Pagaré\n\n-Autorización de descuento.\n\n-Hoja de resumen."
        
        descriptionThridLabel.text = "Los mismo que serán enviados a su correo electrónico: \(DMProduct.shared.credit?.correo ?? ""),después de ser aprobado y desembolsado"
        
        checkedDescriptionLabel.text = "Mediante el siguiente proceso de Reconomiento Facial suscrito el presente Contrato de Crédito nº \(DMProduct.shared.credit?.credid ?? "") y los demás documentos legales que sustentan esta operación, declarando conocer sus condiciones y aceptando plenamente su contenido. Además, señalo conocer que esta firma electronica tiene igual validez que la firma manuscrita."
    }
    
    private func formatAmount(_ amount: String) -> String {
        let credit = amount
        guard let amount = Double(credit) else {
            return ""
        }
        return Constant.coinSymbolPEN+" "+Utils.formatIntegerAmount(amount: amount)
    }
    
    @IBAction func checkboxButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func nextScannerAction(_ sender: Any) {
        if checkBoxButton.isChecked{
            
            let mediaType = AVMediaType.video
            let authStatus = AVCaptureDevice.authorizationStatus(for: mediaType)
            switch authStatus {
            case .authorized:
                DispatchQueue.main.async {
                    let vc = UIStoryboard.init(name: "Biometria", bundle: Bundle.main).instantiateViewController(withIdentifier: "DMScannerImageViewController") as? DMScannerImageViewController
                    vc?.delegate = self
                    vc?.modalPresentationStyle = .fullScreen
                    self.present(vc!, animated: false, completion: nil)
                }
                break
            case .restricted,.denied:                
                let alertController = UIAlertController(title: "", message: "La aplicación necesita acceder a la cámara  para realizar  el reconocimiento facial y validar su identidad con el RENIEC.", preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "Ir a configuración", style: .default) { (action:UIAlertAction!) in
                    print("Accept button tapped");
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    if (UIApplication.shared.canOpenURL(settingsUrl)) {
                        UIApplication.shared.open(settingsUrl, options: [:]) { (completion) in                            
                        }
                    }
                }
                alertController.addAction(OKAction)
                let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in
                    print("Cancel button tapped");
                }
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
            break
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                    DispatchQueue.main.async {
                        if response{
                            let vc = UIStoryboard.init(name: "Biometria", bundle: Bundle.main).instantiateViewController(withIdentifier: "DMScannerImageViewController") as? DMScannerImageViewController
                            vc?.delegate = self
                            vc?.modalPresentationStyle = .fullScreen
                            self.present(vc!, animated: false, completion: nil)
                        }else{
                            let alertController = UIAlertController(title: "", message: "La aplicación necesita acceder a la cámara  para realizar  el reconocimiento facial y validar su identidad con el RENIEC.", preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "Ir a configuración", style: .default) { (action:UIAlertAction!) in
                                print("Accept button tapped");
                                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                    return
                                }
                                if (UIApplication.shared.canOpenURL(settingsUrl)) {
                                    UIApplication.shared.open(settingsUrl, options: [:]) { (completion) in
                                        
                                    }
                                }

                            }
                            alertController.addAction(OKAction)
                            let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel) { (action:UIAlertAction!) in
                                print("Cancel button tapped");
                            }
                            alertController.addAction(cancelAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                        
                    }
                }
                break
            default:
                print("Error")
            }
        }else{
            alertasNativas(title: Constant.textEmpty, message: "Para aceptar las Condiciones del Crédito, marque la casilla.", acceptButton: "ACEPTAR")
        }
        
    }
    
}

extension DMCreditConditionsViewController : DMScannerDelegate{
    func backToManual() {
        navigationController?.popViewController(animated: false)
        delegate?.backToManual()
    }
    
    func backToController() {
        navigationController?.popViewController(animated: false)
    }
    func backToRefreshController(){
        
    }

    
    
}
