//
//  ResultScannerFaceViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 19/11/20.
//

import UIKit
import NVActivityIndicatorView


protocol ResultScannerDelegate {
    func restartAction()
    func closePresent()
    func restartRegisterDocument()
}

class ResultScannerFaceViewController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet weak var faceImageView: UIImageView!
    @IBOutlet weak var informationTipView: UIView!
    @IBOutlet weak var restartScanButton: UIButton!
    @IBOutlet weak var closeScanButton: UIButton!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var informationTipLabel: UILabel!
    @IBOutlet weak var topConstantConstraint: NSLayoutConstraint!
    @IBOutlet weak var resultView: UIView!
    
    
    var faceImage : UIImage!
    var delegate : ResultScannerDelegate?
    var isFaceDetected : Bool = false
    var loading : loadingView?
    
    func loadingAnimation(){
        loading = loadingView.instanceFromNib()
        loading?.frame = view.frame
        view.addSubview(loading!)
        loading!.loadingView.startAnimating()
    }
    
    func stopLoadingAnimating(){
        loading!.loadingView.stopAnimating()
        loading?.removeFromSuperview()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addStyleToElements()
        resultView.isHidden = true
        topConstantConstraint.constant = 20
        self.messageLabel.text = Constant.textEmpty
        //startAnimating()
        loadingAnimation()
        print("RESULTADO!!!!!!!!!")
        self.messageLabel.textColor = Colors.primaryColor()
        self.messageLabel.text = "procesando fotografía..."
        self.resultView.isHidden = false
        let imageCompress = faceImage
        imageCompress?.jpegData(compressionQuality: 0.7)
        print("SE IMPRIMEEEEE ESTTO    --_----------------->")
        
        if !isFaceDetected{
            self.messageLabel.text = "Imagen no valida"
            self.messageLabel.textColor = .green
            return
        }
        Utils.convertImageToBase64(image: imageCompress!) { (base64) in
                                 
            if !base64.isEmpty{
                faceImageView.image = Utils.convertBase64ToImage(imageString: base64)
                Manager.shared.requestSendDocumentFacial(asodni: UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue) ?? "", capturaFacial:base64
                                                         , aplicacion: "APPMAESTRO", usureg: "DOCENTE", cortar: "false", firma: "", asonomdni: (DMUser.shared.user?.first?.asoapenomdni)!, asoid: (DMUser.shared.user?.first?.asoid)!,  credid:  (DMProduct.shared.credit?.credid)  ?? "" , codSol: (DMProduct.shared.credit?.codsol)! != "" ? (DMProduct.shared.credit?.codsol)! : (DMProduct.shared.creditTeApoyo?.codsol)!, asoTipId: (DMUser.shared.user?.first?.asotipid) ?? "", regPenId: (DMUser.shared.user?.first?.regpenid) ?? "") { (facial) in

                    //self.stopAnimating()
                    self.stopLoadingAnimating()
                    if facial.success{
                        self.messageLabel.text = facial.mensaje
                        self.messageLabel.textColor = UIColor(red: 0.00, green: 0.55, blue: 0.01, alpha: 1.00)
                        self.resultView.isHidden = true
                        self.topConstantConstraint.constant = 20
                        self.alertasNativas(title: Constant.textEmpty, message: "La operación se ha grabado correctamente", acceptButton: "Aceptar", handlerAccept:  {
                                                                                        
                            self.dismiss(animated: false) {
                                self.delegate?.closePresent()
                                NotificationCenter.default.post(name: Notification.Name("ShowToRequest"), object: nil)
                            }
                        })
                                        
                    }else{
                        self.resultView.isHidden = false
                        self.topConstantConstraint.constant = 120
                        //DMUSER
                        let count = UserDefaults.standard.integer(forKey: UserDefault.countIntentos.rawValue)
                        UserDefaults.standard.set(count + 1, forKey: UserDefault.countIntentos.rawValue)
                                                  
                        let nroIntentoMaximos = Int((DMProduct.shared.credit?.nrointentosmaximos)!)
                        let numeroRestantes = nroIntentoMaximos! - (count + 1)
                        
                        self.messageLabel.textColor = UIColor(red: 0.86, green: 0.24, blue: 0.00, alpha: 1.00)
                        
                        if numeroRestantes == 1{
                            self.messageLabel.text = facial.mensaje + "\nTE QUEDA \(numeroRestantes) INTENTO"
                        }else{
                            self.messageLabel.text = facial.mensaje + "\nTE QUEDAN \(numeroRestantes) INTENTOS"
                        }
                        
                        if (count + 1) >= nroIntentoMaximos!{
                            self.messageLabel.text = ""
                            self.alertasNativas(title: Constant.textEmpty, message: "Maestro, no hemos podido confirmar su identidad mediante el reconocimiento facil de RENIEC, por favor seleccione otra modalidad para presentar los documentos del crédito", acceptButton: "Aceptar", handlerAccept:  {
                                                                                            
                                self.dismiss(animated: false) {
                                    self.delegate?.closePresent()

                                    self.delegate?.restartRegisterDocument()
                                }
                            })
                        }
                        
                    }
                
                }
            }
        }
    
       
    }
    
    func addStyleToElements(){
        informationTipView.layer.cornerRadius = 5.0
    }
    
    
    @IBAction func restartScanButtonAction(_ sender: Any) {
        delegate?.restartAction()
        dismiss(animated: true, completion: nil)
    }
    @IBAction func closeScanButtonAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.closePresent()
        }
    }
    

}

