//
//  DMDocumentsViewController.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 16/11/20.
//

import UIKit


protocol DMDocumentsDelegate: class {
    func validateManualController()
}


class DMDocumentsViewController: DMBaseViewController {

    @IBOutlet weak var containerElectronicView: UIView!
    @IBOutlet weak var electronicSignatureButton: UIButton!
    @IBOutlet weak var ElectronicSignatureView: UIView!
    
    var dmDialogIdentity: DMdialogIdentityCheckView!
    var dateEmisionPicker :UIDatePicker!
    var dateCaducidadPicker :UIDatePicker!
    var delegate : DMDocumentsDelegate?
    
    var isManualRegister = false
    var ArrayNames : ResponseRandomNameArray = []
    var nroIntentoMaximos : Int!
    var nroIntentos : Int!
    
    var uploadImage : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addStyleToElements()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        dmDialogIdentity = nil
        //isManualRegister = UserDefaults.standard.bool(forKey: UserDefault.validateDocument.rawValue)
        nroIntentos = UserDefaults.standard.integer(forKey: UserDefault.countIntentos.rawValue)
                
        /*if nroIntentos != Int((DMProduct.shared.credit?.nrointento ?? "0")){
            nroIntentos = Int((DMProduct.shared.credit?.nrointento ?? "0"))
        }*/
        
        nroIntentoMaximos = Int((DMProduct.shared.credit?.nrointentosmaximos)!)
        //nroIntentoMaximos = 100

        if nroIntentos >= nroIntentoMaximos{
            isManualRegister = true
        }
        
        
        if isManualRegister{
            if DMProduct.shared.credit?.validen != nil {
                delegate?.validateManualController()
            }else{
                openDialogSelector()
            }
            containerElectronicView.isHidden = true
            electronicSignatureButton.setTitle("CONTROL IDENTIDAD", for: .normal)
        }else{
            containerElectronicView.isHidden = false
            electronicSignatureButton.setTitle("FIRMA ELECTRÓNICA", for: .normal)
        }
    }
    
    func addStyleToElements(){
        electronicSignatureButton.layer.cornerRadius = 5
        ElectronicSignatureView.layer.cornerRadius = 5
    }
    
    func openDialogSelector(){
        
        if dmDialogIdentity == nil{
            
            dmDialogIdentity = DMdialogIdentityCheckView.instanceViewFromXIB()
                        
            var frame = dmDialogIdentity.frame
            frame.origin = CGPoint.zero
            frame.size = UIScreen.main.bounds.size
            dmDialogIdentity.frame = frame
            dmDialogIdentity.delegate = self
            
            dmDialogIdentity.nombrePadreMadreTextfield.delegate = self
            
            
            Manager.shared.requestGetRandomNames(Dni: UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue) ?? "", completion: {(names, error) in
                if (error.first != nil) {
                    print("error: \(String(describing: error.first?.mensaje))")
                    
                }
                
                guard let names = names else {
                    return
                }
                
                self.ArrayNames = names
            })
            
            navigationController?.view.addSubview(dmDialogIdentity)
            
            let pickerView = UIPickerView()
            pickerView.delegate = self
            dmDialogIdentity.nombrePadreMadreTextfield.inputView = pickerView
            
            let toolBarPicker = UIToolbar()
            toolBarPicker.sizeToFit()
            let button = UIBarButtonItem(title: "Aceptar", style: .plain, target: self, action: #selector(self.action))
            toolBarPicker.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), button], animated: true)
            toolBarPicker.isUserInteractionEnabled = true
            dmDialogIdentity.nombrePadreMadreTextfield.inputAccessoryView = toolBarPicker
            
            dateEmisionPicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
            dateEmisionPicker.addTarget(self, action: #selector(self.dateEmisionChanged), for: .allEvents)
            if #available(iOS 13.4, *) {
                dateEmisionPicker.preferredDatePickerStyle = .wheels
            }
            dateEmisionPicker.datePickerMode = .date
            dmDialogIdentity.fechaEmisionTextfield.inputView = dateEmisionPicker
            let doneEmisionButton = UIBarButtonItem.init(title: "Aceptar", style: .done, target: self, action: #selector(self.datePickerEmisionDone))
            let toolBarEmision = UIToolbar()
            toolBarEmision.sizeToFit()
            
            toolBarEmision.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneEmisionButton], animated: true)
            dmDialogIdentity.fechaEmisionTextfield.inputAccessoryView = toolBarEmision
            dateCaducidadPicker = UIDatePicker.init(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: 200))
            dateCaducidadPicker.addTarget(self, action: #selector(self.dateCaducidadChanged), for: .allEvents)
            if #available(iOS 13.4, *) {
                dateCaducidadPicker.preferredDatePickerStyle = .wheels
            }
            dateCaducidadPicker.datePickerMode = .date
            dmDialogIdentity.fechaCaducidadTextfield.inputView = dateCaducidadPicker
            
            let toolBarCaducidad = UIToolbar()
            toolBarCaducidad.sizeToFit()
            
            let doneCaducidadButton = UIBarButtonItem.init(title: "Aceptar", style: .done, target: self, action: #selector(self.datePickerCaducidadDone))
            toolBarCaducidad.setItems([UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil), doneCaducidadButton], animated: true)
            dmDialogIdentity.fechaCaducidadTextfield.inputAccessoryView = toolBarCaducidad
        }

    }
    
    
    
    @IBAction func ElectronicSignatureAction(_ sender: Any) {
                
        if isManualRegister {
            openDialogSelector()        
        }else{
            let vc = UIStoryboard.init(name: "Biometria", bundle: Bundle.main).instantiateViewController(withIdentifier: "DMCreditConditionsViewController") as? DMCreditConditionsViewController

            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    @objc func action() {
        dmDialogIdentity.endEditing(true)
    }
    
    @objc func datePickerEmisionDone() {
        dateEmisionChanged()
        dmDialogIdentity.fechaEmisionTextfield.endEditing(true)

    }
    
    @objc func datePickerCaducidadDone() {
        dateCaducidadChanged()
        dmDialogIdentity.fechaCaducidadTextfield.endEditing(true)
    }

    @objc func dateEmisionChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dmDialogIdentity.fechaEmisionTextfield.text = dateFormatter.string(from: dateEmisionPicker.date)
    }
    
    @objc func dateCaducidadChanged() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dmDialogIdentity.fechaCaducidadTextfield.text = dateFormatter.string(from: dateCaducidadPicker.date)
    }
    
}

extension DMDocumentsViewController: DMCreditConditionDelegate{

    func backToManual() {
        isManualRegister = true
        containerElectronicView.isHidden = true
        electronicSignatureButton.setTitle("CONTROL IDENTIDAD", for: .normal)
    }
    
    
}

extension DMDocumentsViewController: DMdialogIdentityCheckDelegate{
    func acceptActionDelegate(fechaEmision: String, fechaCaducidad: String, nombrePadres: String, caduca: String) {
        
        loadingAnimation()
        
        Manager.shared.requestControlIdentidad(asodni: UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue)!, fechaEmision: fechaEmision, fechaCaducidad: fechaCaducidad, padreMadre: nombrePadres, dniNoCaduca: caduca) { [self] (response, error) in
               
            self.stopLoadingAnimating()
            if response?.success! == true{
                let signature = response?.signature
                print("Siganture \(signature)")
                DMProduct.shared.credit?.validen = "S"
                
                self.isManualRegister = true
                /*let vc = UIStoryboard.init(name: "RequestCredit", bundle: Bundle.main).instantiateViewController(withIdentifier: "DMManualRegisterDocumentViewController") as? DMManualRegisterDocumentViewController
                self.navigationController?.pushViewController(vc!, animated: true)*/
                self.dmDialogIdentity.dismissView()
                self.dmDialogIdentity = nil
                self.delegate?.validateManualController()

                
            }else{
                self.dmDialogIdentity = nil
                self.alertasNativas(title: "", message: (response?.mensaje!)!, acceptButton: Constant.acceptButton)
            }
            
        }
    }
    
}


extension DMDocumentsViewController: UITextFieldDelegate{
    
}

extension DMDocumentsViewController: UIPickerViewDelegate, UIPickerViewDataSource{
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ArrayNames.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        dmDialogIdentity.nombrePadreMadreTextfield.text = ArrayNames[row].NOMBRE
        return ArrayNames[row].NOMBRE
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        dmDialogIdentity.nombrePadreMadreTextfield.text = ArrayNames[row].NOMBRE
    }
    
}
