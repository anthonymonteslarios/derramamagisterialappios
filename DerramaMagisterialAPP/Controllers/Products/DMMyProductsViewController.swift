//
//  HomeViewController.swift
//  DemoNavigation
//
//  Created by Walter Alonso Rodriguez Castaneda on 11/12/20.
//

import Foundation

class DMMyProductsViewController: DMBaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    //MarK: Call User Model
    var User: ResponseLogin?
    var Photo: ResponsePhoto?
    
    private var DNI : String = UserDefaults.standard.string(forKey: UserDefault.DNIUser.rawValue) ?? ""
    
    private var productsArray : [ResponseProductsAll] = []
    private var firstTime : Bool  = true
    private var emptyProducts = 0
    private var availableProducts : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        productsArray = []
        tableView.tableHeaderView = nil
        tableView.reloadData()
        loadingAnimation()
        fetchDataGET(Paralelo: Constant.CreditoConsumo)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        adjustHeightTableView()
    }
    
    // MARK: - Own Methods
    private func initView() {
        configureTableView()
        getUserData()
    }
    
    private func getUserData(){
        if let data = UserDefaults.standard.value(forKey:UserDefault.user.rawValue) as? Data {
            let userData = try? PropertyListDecoder().decode(ResponseLogin.self, from: data)
            User = userData
        }
    }
    
    private func fetchDataGET(Paralelo: String){
        Manager.shared.requestGetProducts(ASODNI: DNI, RECALCULAR: Constant.FirstRecalcular, MONTO_OTORGAR: "0", NROCUOTAS: "0", TEM: "", MONTO_OTORGAR_X: "", NROCUOTAS_X: "", PARALELO: Paralelo)  {(dataGet, error) in
                        
            if (error.first != nil) {
                print("error: \(String(describing: error.first?.mensaje))")
                self.stopLoadingAnimating()
                self.alertasNativas(title: Constant.textEmpty, message: (error.first?.mensaje)!, acceptButton: "Aceptar")
                return
            }
            
            guard let dataGet = dataGet else{ return }
            self.productsArray.append(dataGet)
            self.tableView.reloadData()
            
            if Paralelo == Constant.CreditoConsumo{
                (DMProduct.shared.credit = dataGet)
                self.loadHeader(empty: self.availableProducts)
            }else{
                (DMProduct.shared.creditTeApoyo = dataGet)
                self.stopAnimating()
                return
            }
            
            let nroIntentos = Int(((DMProduct.shared.credit?.nrointento == nil) ? "0" : DMProduct.shared.credit?.nrointento)!)!
            
            UserDefaults.standard.set(nroIntentos, forKey: UserDefault.countIntentos.rawValue)

            
            if (DMProduct.shared.credit?.paralelo)! == Constant.CreditoConsumo && DMProduct.shared.credit?.estadoSolicitud == nil && DMProduct.shared.credit?.validaTeapoyo == Constant.CreditoTeApoyo{
                self.fetchDataGET(Paralelo: Constant.CreditoTeApoyo)
            }else{
                self.stopLoadingAnimating()
            }
            
            
            if DMProduct.shared.credit?.codsol != nil{
                self.GoToRequest(index: 0)
            }

        }
    }
    
    private func loadHeader(empty: Bool){
        let headerTableView = DMProductsHeader.instanceViewFromXIB()
        let messageHeader = !empty ? "pronto tendremos, \nnuevos productos \npara tí" : "tenemos estos productos para tí,\ncon tasas especiales"
        headerTableView.setupView(title: "Hola \(User?.first?.asonomdni ?? ""),\n\(messageHeader)")
        tableView.tableHeaderView = headerTableView
    }
    
    private func configureTableView() {
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.rowHeight = 150
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.register(DMProductsTableViewCell.getNib(), forCellReuseIdentifier: DMProductsTableViewCell.cellId)
        tableView.backgroundColor = .clear
    }
    
    private func adjustHeightTableView() {
        guard let headerView = tableView.tableHeaderView else { return }
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        tableView.tableHeaderView = headerView
    }


}

//MARK: - UITableViewDataSource
extension DMMyProductsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if productsArray.first?.mensajeReultadoEvaluacion != nil{
            availableProducts = false
            return emptyProducts
        }
        return productsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DMProductsTableViewCell.cellId, for: indexPath) as? DMProductsTableViewCell else { return DMProductsTableViewCell() }
        let product = productsArray[indexPath.row]
        cell.iWantItButton.tag = indexPath.row
        cell.loadCell(loadData: product)
        cell.delegate = self

        return cell
    }
    
}

extension DMMyProductsViewController: DMProductsDelegate{
    func GoToRequest(index: Int) {
        var paralelo = ""
        
        if index == 0{
            if DMProduct.shared.credit?.estadoSolicitud == nil &&  DMProduct.shared.credit?.paralelo == Constant.CreditoTeApoyo ||
                DMProduct.shared.credit?.estadoSolicitud != nil && DMProduct.shared.credit?.tipoparalelo != nil{
                paralelo = Constant.CreditoTeApoyo
            }else{
                paralelo = Constant.CreditoConsumo
            }
            
        }else{
            paralelo = Constant.CreditoTeApoyo
        }
        
        self.startAnimating()
        self.view.isUserInteractionEnabled = false
        Manager.shared.requestGetProducts(ASODNI: DNI, RECALCULAR: Constant.Recalcular, MONTO_OTORGAR: "0", NROCUOTAS: "0", TEM: "", MONTO_OTORGAR_X: "", NROCUOTAS_X: "", PARALELO: paralelo)  {(dataGet, error) in
                        
            self.view.isUserInteractionEnabled = true
            self.stopAnimating()
            
            if (error.first != nil) {
                return
            }
            
            if index == 0{
                DMProduct.shared.credit = dataGet
            }else{
                DMProduct.shared.credit = dataGet
            }
            
            UserDefaults.standard.set(index, forKey: UserDefault.requestTypeCredit.rawValue)
            let controllerRequestCredit = UIViewController.getController(storyBoard: "RequestCredit", indentifier: "RequestCredit")
            self.revealViewController().frontViewController = controllerRequestCredit
            controllerRequestCredit.revealViewController()?.revealView()
            controllerRequestCredit.revealViewController()?.revealView()
            
        }
    
    }
    
}
