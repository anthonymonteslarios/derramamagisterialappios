//
//  AppDelegate.swift
//  DerramaMagisterialAPP
//
//  Created by Anthony Montes on 11/11/20.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var timer: Timer?
    lazy var sessionTime: Int = {
        var value = 300
        return value
    }()
    lazy var inactiveTime = { return 0 }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // timer
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
        
        NotificationCenter.default.addObserver(self,
                                   selector: #selector(applicationDidTimeout(notification:)),
                                   name: .appTimeout,
                                   object: nil
            )
        
        UINavigationBar.appearance().barTintColor = Colors.primaryColor()
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barStyle = .black        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Aceptar"
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
        
        FirebaseApp.configure()

        
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    
        if UserDefaults.standard.bool(forKey: UserDefault.SessionActive.rawValue){
            self.timer?.invalidate()
            self.timer = nil
            application.beginBackgroundTask(expirationHandler: nil)
            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.tick), userInfo: nil, repeats: true)
                RunLoop.current.add(self.timer!, forMode: .common)
                
        }
    }
    
    
    @objc func applicationDidTimeout(notification: NSNotification) {

        print("application did timeout, perform actions")
        UserDefaults.standard.set(false, forKey: UserDefault.SessionActive.rawValue)
        print("SE CERRO ->")
  
        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)

        let vc = UIApplication.shared.keyWindow?.rootViewController as! SplashViewController
        print("Viewcontroller \(vc)")
        vc.sessionTime = true
                
        
        vc.goToController(animated:false)
        
    }

    // MARK: - Timer Methods
    @objc func tick() {
        
        /*if UserDefaults.standard.bool(forKey: UserDefault.SessionActive.rawValue){
            inactiveTime += 1
            if inactiveTime >= sessionTime {
                self.inactiveTime = 0
            
                UserDefaults.standard.set(false, forKey: UserDefault.SessionActive.rawValue)
                print("SE CERRO ->")

                UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
                let vc = UIApplication.shared.keyWindow?.rootViewController as! SplashViewController
                print("Viewcontroller \(vc)")
                vc.goToController(animated:false)
            }
            
        }*/
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

